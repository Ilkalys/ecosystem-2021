using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SSEv_UpdateCheckpoint : MonoBehaviour
{
    public int levelValue;

    [Tooltip("The scene details from where to search for the level value")]
    public LevelsScenesDistribution sceneDetails;

    public bool overrideLevelValue;

    public bool setLevelPrimarySceneAsActive = true;

    public void Start()
    {
        searchLevelinLevelScenesDistribution(this);
    }

    public void UpdateCheckpoint()
    {
        PlayerPrefs.SetInt("LastLevelLoaded", levelValue);
        if (setLevelPrimarySceneAsActive)
        {
            SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneDetails.levelsDetails[levelValue].GetPrimaryScene));
        }
    }
    public static void searchLevelinLevelScenesDistribution(SSEv_UpdateCheckpoint targetObject)
    {
        if (targetObject.sceneDetails != null && !targetObject.overrideLevelValue)
            targetObject.levelValue = targetObject.sceneDetails.SearchLevelWithPrimarySceneName(targetObject.gameObject.scene.name);
    }
}

#if UNITY_EDITOR

[CanEditMultipleObjects]
[CustomEditor(typeof(SSEv_UpdateCheckpoint))]
public class SSEv_UpdateCheckpointEditor : Editor
{
    SerializedProperty levelValueProperty;
    SerializedProperty sceneDetailsProperty;
    SerializedProperty overrideLevelValueProperty;
    SerializedProperty setLevelPrimarySceneAsActiveProperty;

    private void OnEnable()
    {
        levelValueProperty = serializedObject.FindProperty("levelValue");
        sceneDetailsProperty = serializedObject.FindProperty("sceneDetails");
        overrideLevelValueProperty = serializedObject.FindProperty("overrideLevelValue");
        setLevelPrimarySceneAsActiveProperty = serializedObject.FindProperty("setLevelPrimarySceneAsActive");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        if (!overrideLevelValueProperty.boolValue || overrideLevelValueProperty.hasMultipleDifferentValues)
        {
            EditorGUILayout.LabelField("Detected Level Value", levelValueProperty.hasMultipleDifferentValues ? "Mixed Value" : levelValueProperty.intValue.ToString());
        }
        else
        {
            EditorGUILayout.PropertyField(levelValueProperty);
            EditorUtils.clampIntValue(levelValueProperty, 0, null);
        }
        bool usefullToChangeSceneDetails = !overrideLevelValueProperty.boolValue || overrideLevelValueProperty.hasMultipleDifferentValues || 
                                  setLevelPrimarySceneAsActiveProperty.boolValue || setLevelPrimarySceneAsActiveProperty.hasMultipleDifferentValues;
        EditorGUI.BeginDisabledGroup(!usefullToChangeSceneDetails);
        EditorGUILayout.PropertyField(sceneDetailsProperty);
        EditorGUI.EndDisabledGroup();
        EditorGUILayout.PropertyField(overrideLevelValueProperty);
        EditorGUILayout.PropertyField(setLevelPrimarySceneAsActiveProperty);

        foreach(SSEv_UpdateCheckpoint targetObject in serializedObject.targetObjects)
        {
            SSEv_UpdateCheckpoint.searchLevelinLevelScenesDistribution(targetObject);
        }

        serializedObject.ApplyModifiedProperties();
    }

    
}

#endif