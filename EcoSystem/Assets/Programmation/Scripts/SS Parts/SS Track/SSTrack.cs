using UnityEngine;
using System;
using Cinemachine.Utility;

namespace Cinemachine
{
    [AddComponentMenu("SideScroller/SSTrack")]
    [SaveDuringPlay]
    [DisallowMultipleComponent]
    public class SSTrack : CinemachinePath
    {
        public bool m_gizmosStayVisible;

        public float FindClosestPointWithAxes( Vector3 p, int startSegment, int searchRadius, int stepsPerSegment, Vector3 axes )
        {
            p = Vector3.Scale( p, axes );

            float start = MinPos;
            float end = MaxPos;
            if ( searchRadius >= 0 )
            {
                int r = Mathf.FloorToInt(Mathf.Min(searchRadius, (end - start) / 2f));
                start = startSegment - r;
                end = startSegment + r + 1;
                if ( !Looped )
                {
                    start = Mathf.Max( start, MinPos );
                    end = Mathf.Min( end, MaxPos );
                }
            }
            stepsPerSegment = Mathf.RoundToInt( Mathf.Clamp( stepsPerSegment, 1f, 100f ) );
            float stepSize = 1f / stepsPerSegment;
            float bestPos = startSegment;
            float bestDistance = float.MaxValue;
            int iterations = (stepsPerSegment == 1) ? 1 : 3;
            for ( int i = 0; i < iterations; ++i )
            {
                Vector3 v0 = Vector3.Scale(EvaluatePosition(start), axes);

                for ( float f = start + stepSize; f <= end; f += stepSize )
                {
                    Vector3 v = Vector3.Scale(EvaluatePosition(f), axes);
                    float t = p.ClosestPointOnSegment(v0, v);
                    float d = Vector3.SqrMagnitude(p - Vector3.Lerp(v0, v, t));
                    if ( d < bestDistance )
                    {
                        bestDistance = d;
                        bestPos = f - ( 1 - t ) * stepSize;
                    }
                    v0 = v;
                }
                start = bestPos - stepSize;
                end = bestPos + stepSize;
                stepSize /= stepsPerSegment;
            }
            return bestPos;
        }
    }
}
