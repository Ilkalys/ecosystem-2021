using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PushableObjectMovement : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\


    private const   float           m_minSpeed        = 0.8f;
    public          UnityEvent      m_OnStart;
    public          UnityEvent      m_OnMovement;
    public          UnityEvent      m_OnStop;
    private         bool            m_isMoving;

    public          bool            m_isPushed;
    private const   float           m_breakDistance   = 0.05f;
    private const   float           m_smoothStep      = 0.1f;
    private         float           m_dist            = 0;


    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Start()
    {
        m_OnStart.Invoke();
    }

    //--------------------------------------------------------------------------------------\\
    void FixedUpdate()
    {
        if ( m_isPushed )
        {
            float diffDist = Vector3.Distance(SS_Character_Controller.instance.transform.position, this.transform.position);
            if ( Mathf.Abs( m_dist - diffDist ) > m_breakDistance )
            {
                m_isPushed = false;
            }
            else if ( diffDist != m_dist )
            {
                Vector3 forward = Vector3.Cross(  SS_Character_Controller.instance.transform.right,  SS_Character_Controller.instance.m_normalGround );
                transform.position = Vector3.MoveTowards( transform.position, transform.position + ( m_dist - diffDist ) * forward, m_smoothStep );
            }

            if ( Mathf.Abs( SS_Character_Controller.instance.m_currentSpeed ) > m_minSpeed && !m_isMoving )
            {
                m_isMoving = true;
                m_OnMovement.Invoke();
            }
            else if ( Mathf.Abs( SS_Character_Controller.instance.m_currentSpeed ) < m_minSpeed && m_isMoving )
            {
                m_isMoving = false;
                m_OnStop.Invoke();
            }

        }
        else
        {
            if ( m_isMoving )
            {
                m_isMoving = false;
                m_OnStop.Invoke();
            }
        }

    }

    //--------------------------------------------------------------------------------------\\
    public void BeginPush()
    {
        if( m_dist == 0)
            m_dist = Vector3.Distance( SS_Character_Controller.instance.transform.position, this.transform.position );
        m_isPushed = true;
    }

    //--------------------------------------------------------------------------------------\\
    public void EndPush()
    {
        m_dist = 0;
        m_isPushed = false;
    }
}
