using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CC_State_Deactivatable : CC_State
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public bool     canUse              = true;
    public float    desactivateDuration = 0.5f;

}
