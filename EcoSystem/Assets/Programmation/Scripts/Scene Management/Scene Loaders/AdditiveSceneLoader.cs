using CustomsAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// A component used to load scenes additively when the player is getting close and unload them when they are far enough
/// </summary>
public class AdditiveSceneLoader : MonoBehaviour
{
    public enum CheckMethod
    {
        Distance,
        Trigger
    }

    [Tooltip("The name of the scene to load. It has to be added to build settings for it to work.")]
    public string sceneToLoad;
    bool isLoaded = false;
    bool isLoading = false;

    [Tooltip("Should the scene be obligatorily loaded or unloaded at startup. If left to false (recommanded) " +
        "the shouldLoad value will take the state of the scene at startup, otherwise it will force the scene " +
        "load or unload if needed before checking shouldLoad value as normal")]
    public bool overrideShouldLoadStartupValue = false;
    [SerializeField]
    [Tooltip("Default value at startup. This will get updated automaticaly by the script afterward")]
    private bool shouldLoad = false;

    [Tooltip("The GameObject that this component should track to know when to load or unload the scene. " +
        "This is generaly the player. If its value is null the shouldLoad value will not be updated.")]
    public GameObject checkObject;

    [Tooltip("The way the component check if the scene should be loaded or unloaded : using simple distances " +
        "or trigger collider")]
    public CheckMethod checkMethod;

    [Tooltip("The trigger collider in which the scene should be loaded (checks with OnTriggerEnter(), " +
        "the pointed gameObject must also contain an UnityEventCatcher component)")]
    public Collider loadCollider;
    [Tooltip("The trigger collider outside of which the scene should be unloaded (checks with OnTriggerExit(), " +
        "the pointed gameObject must also contain an UnityEventCatcher component)")]
    public Collider unloadCollider;
    bool colliderEventsLinked = false;
    [Tooltip("Should this component check if this scene should be loaded or unloaded when it is enabled? " +
        "Auto updates shouldLoad variable each time OnEnable() is called.\n" +
        "OnTriggerEnter might still be called and load the scene even if this is not toggled on. " +
        "This is using bonding boxes which tends to be larger than the trigger collider they are representing.")] 
    public bool checkAtEnable = false;

    [Tooltip("The distance under which the scene should be loaded")]
    public float loadDistance;
    [Tooltip("The distance after which the scene should be unloaded")]
    public float unloadDistance;

    private float? sqrDistance;


    // Awake is called before OnEnable 
    void Awake()
    {
        for (int i = 0; i < SceneManager.sceneCount; ++i)
        {
            Scene scene = SceneManager.GetSceneAt(i);
            if (scene.name == sceneToLoad)
            {
                isLoaded = true;
                if (!scene.isLoaded) //This scene has not finished loading yet
                {
                    isLoading = true;
                    SceneManager.sceneLoaded += waitForSceneLoaded;
                }
            }
        }

        if (!overrideShouldLoadStartupValue)
            shouldLoad = isLoaded;
        else
        {
            if (shouldLoad)
                LoadScene();
            else
                UnloadScene();
        }
    }

    private void OnEnable()
    {
        if (checkMethod == CheckMethod.Trigger)
        {
            // manual test of onTrigger to unload the scene if asked and necessary
            if (checkAtEnable)
            {
                manualCheckTrigger();
            }
            linkColliderEvents();
        }
    }

    /// <summary>
    /// Used this method to check wether the checkObject collider bounding box intersect 
    /// with the bounding boxes of the load and unload trigger colliders and update shoiuldLoad accordingly.
    /// This could be usefull in the case of a missed trigger exit if the check object 
    /// has been deactivated while in the collider for example.
    /// Carefull bounding boxes are typically larger than the collider they are linked to!
    /// </summary>
    public void manualCheckTrigger()
    {
        if (checkMethod == CheckMethod.Trigger && checkObject != null)
        {
            if (loadCollider.bounds.Intersects(checkObject.GetComponent<Collider>().bounds))
                shouldLoad = true;
            if (!unloadCollider.bounds.Intersects(checkObject.GetComponent<Collider>().bounds))
                shouldLoad = false;
        }
    }

    private void OnDisable()
    {
        if (checkMethod == CheckMethod.Trigger)
        {
            unlinkColliderEvents();
        }
    }

    private void waitForSceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        if (arg0.name == sceneToLoad)
        {
            isLoading = false;
            SceneManager.sceneLoaded -= waitForSceneLoaded;
        }
    }


    // Update is called once per frame
    void Update()
    {
        if (checkMethod == CheckMethod.Distance)
        {
            checkWithDistance();
        }

        if (shouldLoad)
            LoadScene();
        else
            UnloadScene();
    }

    void LoadScene()
    {
        if (!isLoaded && !isLoading)
        {
            isLoading = true;
            SceneManager.LoadSceneAsync(sceneToLoad, LoadSceneMode.Additive).completed += (AsyncOperation _) =>
            {
                isLoaded = true;
                isLoading = false;
                //Debug.Log("loaded " + sceneToLoad);
            };
        }
    }


    void UnloadScene()
    {
        if (isLoaded && !isLoading)
        {
            isLoading = true;
            SceneManager.UnloadSceneAsync(sceneToLoad).completed += (AsyncOperation _) =>
            {
                isLoaded = false;
                isLoading = false;
                //Debug.Log("unloaded " + sceneToLoad);
            };
        }
    }

    void checkWithDistance()
    {
        // Verify data formation //TODO add this directly in a custom inspector
        if (loadDistance > unloadDistance)
            Debug.LogWarning("unloadDistance of " + gameObject.name + " is smaller than loadDistance!");

        // Search shouldLoad Value
        sqrDistance = (checkObject != null) ? (float?)(this.transform.position - checkObject.transform.position).sqrMagnitude : null;

        if (sqrDistance.HasValue)
        {
            shouldLoad = sqrDistance.Value <= loadDistance * loadDistance ? true : (sqrDistance.Value > unloadDistance * unloadDistance ? false : shouldLoad);
        }
    }

    void linkColliderEvents() // TODO change this to unity event and auto bind/debind with the editor/custom inspector
    {
        if (!colliderEventsLinked)
        {
            loadCollider.GetComponent<UnityEventCatcher>().OnTriggerEnterEvent += OnTriggerEnterEvent;
            unloadCollider.GetComponent<UnityEventCatcher>().OnTriggerExitEvent += OnTriggerExitEvent;

            colliderEventsLinked = true;
        }
    }
    void unlinkColliderEvents() // TODO change this to unity event and auto bind/debind with the editor/custom inspector
    {
        if (colliderEventsLinked)
        {
            loadCollider.GetComponent<UnityEventCatcher>().OnTriggerEnterEvent -= OnTriggerEnterEvent;
            unloadCollider.GetComponent<UnityEventCatcher>().OnTriggerExitEvent -= OnTriggerExitEvent;

            colliderEventsLinked = false;
        }
    }

    private void OnTriggerEnterEvent(Collider other)
    {
        if (other.gameObject == checkObject)
            shouldLoad = true;
    }
    private void OnTriggerExitEvent(Collider other)
    {
        if (other.gameObject == checkObject)
            shouldLoad = false;
    }

    private void OnDrawGizmosSelected()
    {
        if (checkMethod == CheckMethod.Distance)
        {
            Color prevColor = Gizmos.color;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, loadDistance);
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, unloadDistance);
            Gizmos.color = prevColor;
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(AdditiveSceneLoader))]
public class AdditiveSceneLoaderEditor : Editor
{
    SerializedProperty sceneToLoadProperty;
    SerializedProperty overrideShouldLoadStartupProperty;
    SerializedProperty shouldLoadProperty;
    SerializedProperty checkObjectProperty;
    SerializedProperty checkMethodProperty;

    SerializedProperty loadDistanceProperty;
    SerializedProperty unloadDistanceProperty;

    SerializedProperty loadColliderProperty;
    SerializedProperty unloadColliderProperty;
    SerializedProperty checkAtEnableProperty;


    private void OnEnable()
    {
        sceneToLoadProperty = serializedObject.FindProperty("sceneToLoad");
        overrideShouldLoadStartupProperty = serializedObject.FindProperty("overrideShouldLoadStartupValue");
        shouldLoadProperty = serializedObject.FindProperty("shouldLoad");
        checkObjectProperty = serializedObject.FindProperty("checkObject");
        checkMethodProperty = serializedObject.FindProperty("checkMethod");

        loadDistanceProperty = serializedObject.FindProperty("loadDistance");
        unloadDistanceProperty = serializedObject.FindProperty("unloadDistance");

        loadColliderProperty = serializedObject.FindProperty("loadCollider");
        unloadColliderProperty = serializedObject.FindProperty("unloadCollider");
        checkAtEnableProperty = serializedObject.FindProperty("checkAtEnable");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(sceneToLoadProperty);
        if (!EditorUtils.SceneExists(sceneToLoadProperty.stringValue))
        {
            EditorGUILayout.HelpBox("This scene cannot be found in the build settings please add it or verify this name!", MessageType.Error);
        }

        EditorGUILayout.PropertyField(overrideShouldLoadStartupProperty); bool enabled = true;
        foreach (AdditiveSceneLoader sceneLoader in serializedObject.targetObjects)
        {
            enabled &= sceneLoader.enabled;
        }
        bool allowShouldLoadModif = (enabled && Application.isPlaying) || (!overrideShouldLoadStartupProperty.hasMultipleDifferentValues && !overrideShouldLoadStartupProperty.boolValue);
        
        EditorGUI.BeginDisabledGroup(allowShouldLoadModif);
        EditorGUILayout.PropertyField(shouldLoadProperty);
        EditorGUI.EndDisabledGroup();

        EditorGUILayout.PropertyField(checkObjectProperty);
        EditorGUILayout.PropertyField(checkMethodProperty);

        if (checkMethodProperty.enumValueIndex == (int)AdditiveSceneLoader.CheckMethod.Trigger)
        {
            EditorGUILayout.PropertyField(loadColliderProperty);
            EditorGUILayout.PropertyField(unloadColliderProperty);
            EditorGUILayout.PropertyField(checkAtEnableProperty);
        }
        else if (checkMethodProperty.enumValueIndex == (int)AdditiveSceneLoader.CheckMethod.Distance)
        {
            EditorGUILayout.PropertyField(loadDistanceProperty);
            EditorGUILayout.PropertyField(unloadDistanceProperty);
        }

        serializedObject.ApplyModifiedProperties();
    }

}
#endif