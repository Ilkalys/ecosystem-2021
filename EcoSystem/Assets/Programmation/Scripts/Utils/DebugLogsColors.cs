using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DebugHelpers
{
    public class Colors
    {
        public const string Red = "<color=red>";
        public const string Blue = "<color=blue>";
        public const string Green = "<color=green>";

        public const string End = "</color>";
    }
}
