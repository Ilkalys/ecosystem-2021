using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SS_Character_Animation : MonoBehaviour
{    
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public Animator m_animController;
    private SS_Character_Controller m_characterController => SS_Character_Controller.instance;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void FixedUpdate()
    {
        m_animController.SetFloat( "MutationLevel", Mathf.Abs( m_characterController.m_brainState.m_currentMutationLevel) );

        m_animController.SetBool( "IsWaiting", m_characterController.m_isWaiting );

        m_animController.SetFloat( "PreviousSpeed", Mathf.Abs( m_characterController.m_previousSpeed ) );
        m_animController.SetFloat( "NormalizeSpeed", Mathf.Abs( m_characterController.m_currentSpeed ) / m_characterController.m_currentMaxSpeed );
        m_animController.SetFloat( "Speed", m_characterController.m_isPushing ? ( m_characterController.m_isLookingRight ? 1 : -1 ) * m_characterController.m_currentSpeed : Mathf.Abs( m_characterController.m_currentSpeed ) );
        m_animController.SetBool( "isPushing", m_characterController.m_isPushing );

        m_animController.SetBool( "isGrounded", m_characterController.m_isGrounded );
        m_animController.SetBool( "Jump", m_characterController.m_isJumping );
        m_animController.SetBool( "HardLanding", m_characterController.m_isHardLanding );

        int layerObjectInFront = m_characterController.m_Object_InFront? m_characterController.m_Object_InFront.layer : -1;
        m_animController.SetBool("High_Obstacle", layerObjectInFront == LayerMask.NameToLayer("HighObject") && m_characterController.currentState.GetType() == typeof( CC_State_Waiting ) && !m_characterController.m_isHardLanding );
        m_animController.SetBool("Medium_Obstacle", layerObjectInFront == LayerMask.NameToLayer( "MediumObject" ) && m_characterController.currentState.GetType() == typeof( CC_State_Waiting ) && !m_characterController.m_isHardLanding );
        m_animController.SetBool( "JumpOverObject", layerObjectInFront == LayerMask.NameToLayer( "JumpOverObject" ) && m_characterController.currentState.GetType() == typeof( CC_State_Waiting ) && !m_characterController.m_isHardLanding );

        m_animController.SetBool( "Climb", m_characterController.currentState.GetType() == typeof( CC_State_Waiting ) );
        m_animController.SetFloat("VerticalMovement", m_characterController.m_currentVerticalMovementInput);
        m_animController.SetBool( "Climbable_Wall", m_characterController.currentState.GetType() == typeof( CC_State_ClimbingWall ) );
        m_animController.SetBool( "Edge", m_characterController.m_isOnEdge );
    }

    //--------------------------------------------------------------------------------------\\
    public void Death()
    {
        m_animController.SetBool( "OnDeath", m_characterController.m_isDead );
    }

    //--------------------------------------------------------------------------------------\\
    public void Cough()
    {
        m_animController.SetInteger( "nbCough", m_characterController.m_nbCough );
        m_animController.SetLayerWeight( m_animController.GetLayerIndex( "CoughLayer" ), m_characterController.m_coughIntensity );
        m_animController.SetTrigger( "Cough" );
    }
}
