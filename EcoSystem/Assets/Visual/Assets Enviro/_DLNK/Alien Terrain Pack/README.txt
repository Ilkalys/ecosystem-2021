[Alien Terrain Pack] by Daelonik Artworks

Package info here: https://www.dlnkworks.com/asset-store/environments/alien-terrain-pack/

Support: https://www.dlnkworks.com/contact/

Thanks for your purchase!

>> GAIA Biome setup:
1) Import Gaia 2 or Pro
2) Unpack GaiaBiome rar file
3) When creating terrain with Gaia select Alien Terrain in the Biome Settings
4) Select the spawners you want to include in your terrain with the [+] button
5) Push Create tools
6) Play as you want with the Biome stamper tools