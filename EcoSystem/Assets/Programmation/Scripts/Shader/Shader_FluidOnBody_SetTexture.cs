using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shader_FluidOnBody_SetTexture : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public RenderTexture    m_cam, m_from;
    public float            actualisationRate;
    public MeshRenderer     mat;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    private void Start()
    {
        StartCoroutine("ActualiseTexture");
    }

    //--------------------------------------------------------------------------------------\\
    IEnumerator ActualiseTexture()
    {
        Graphics.Blit(m_cam, m_from);
        yield return new WaitForSeconds(actualisationRate);
        StartCoroutine("ActualiseTexture");
    }
}
