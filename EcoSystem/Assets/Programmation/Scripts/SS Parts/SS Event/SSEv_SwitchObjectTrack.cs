using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SSEv_SwitchObjectTrack : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Tooltip("The object who will change is Track")]
    public SSElementOnTrack     m_objectToSwitch;

    // The new Track
    [Tooltip("The new Track, for a better switch the old and the new track should probably be overlaped")]
    public Cinemachine.SSTrack  m_newTrack;

    [Tooltip("The segment of the new track where we will search the closest Point on this track, a segment is a section of path between 2 waypoints. The shorter is this segment, the better will be the position")]
    public int                  m_segmentOfNewTrack;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void Switch()
    {
        if (m_objectToSwitch != null)
        {
            if(m_objectToSwitch.m_pathParams.path != m_newTrack)
            {
                m_objectToSwitch.m_pathParams.path = m_newTrack;
                m_objectToSwitch.m_pathParams.positionOnThePath = m_newTrack.FromPathNativeUnits (m_newTrack.FindClosestPoint(m_objectToSwitch.transform.position, m_segmentOfNewTrack, 1, 10), m_objectToSwitch.m_pathParams.positionUnits);
            }
        }
    }
}
