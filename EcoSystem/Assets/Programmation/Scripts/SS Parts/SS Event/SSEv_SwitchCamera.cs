using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SSEv_SwitchCamera : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public Cinemachine.CinemachineVirtualCameraBase newCamera;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void SwitchCamera()
    {
        Cinemachine.CinemachineBrain brain = Cinemachine.CinemachineCore.Instance.GetActiveBrain(0);
        Cinemachine.ICinemachineCamera oldCamera = brain.ActiveVirtualCamera;

        oldCamera.Priority = 0;
        newCamera.Priority = 100;
    }
}
