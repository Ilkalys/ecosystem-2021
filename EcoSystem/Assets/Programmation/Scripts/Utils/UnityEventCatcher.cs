using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine;

/// <summary>
/// Special class to catch any Unity events outside of a Game Object.
/// In Unity, events such as OnTriggerEnter can only be caught in a script attached to the Game Object from which the event is triggered.
/// 
/// This MonoBehaviour can be used to redirect specific events to another component which would subscribe to the contained public events.
/// 
/// If needed other similarly working event could be redirected with this class.
/// </summary>
public class UnityEventCatcher : MonoBehaviour
{
    public delegate void OnTriggerHandler(Collider other);
    public event OnTriggerHandler OnTriggerEnterEvent;
    public event OnTriggerHandler OnTriggerStayEvent;
    public event OnTriggerHandler OnTriggerExitEvent;

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(gameObject.name + " received the event OnTriggerEnter");
        OnTriggerEnterEvent?.Invoke(other);
    }

    private void OnTriggerStay(Collider other)
    {
        //Debug.Log(gameObject.name + " received the event OnTriggerStay");
        OnTriggerStayEvent?.Invoke(other);
    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log(gameObject.name + " received the event OnTriggerExit");
        OnTriggerExitEvent?.Invoke(other);
    }
}
