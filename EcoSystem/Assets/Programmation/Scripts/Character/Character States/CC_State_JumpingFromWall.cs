using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterController/State/JumpingFromWall")]
public class CC_State_JumpingFromWall : CC_State
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\
   
    [Tooltip("The angle of the jump")]
    public Vector2 m_JumpAngle;
    [Tooltip("The force of the jump")]
    public float m_force;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        instanceCC.m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_currentSpeed = m_JumpAngle.normalized.x * m_force * (instanceCC.m_isLookingRight? -1 : 1);
        instanceCC.m_current2DMovement.y = m_JumpAngle.normalized.y * m_force ;
        instanceCC.m_currentMovement = new Vector3(instanceCC.transform.forward.x, 0, instanceCC.transform.forward.z) * Mathf.Abs(instanceCC.m_currentSpeed) + Vector3.up * instanceCC.m_current2DMovement.y;
        instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;
        instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Jumping);
        return;
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        return false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnExitState() {}

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState() {}
}
