using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeRTPCSetters : MonoBehaviour
{
    public string mainVolumeRTPCName;
    public string musicVolumeRTPCName;
    public string soundVolumeRTPCName;
    public string voicesVolumeRTPCName;
    public string rumblesVolumeRTPCName;


    public void SetMainVolume(float value) =>
        AkSoundEngine.SetRTPCValue(mainVolumeRTPCName, value);
    public void SetMusicVolume(float value) =>
        AkSoundEngine.SetRTPCValue(musicVolumeRTPCName, value);
    public void SetSoundVolume(float value) =>
        AkSoundEngine.SetRTPCValue(soundVolumeRTPCName, value);
    public void SetVoicesVolume(float value) =>
        AkSoundEngine.SetRTPCValue(voicesVolumeRTPCName, value);

    public void SetRumblesVolume(float value) =>
        AkSoundEngine.SetRTPCValue(rumblesVolumeRTPCName, value);
}
