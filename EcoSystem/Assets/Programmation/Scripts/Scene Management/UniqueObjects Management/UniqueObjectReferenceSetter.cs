using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

//Sets a given  a reference to a gameObject
// TODO being able to setup the reference to set/update (similar to unity event method list selector)
// and were to find it (type list + getfield() for example) directly in the inspector would be awesome! 
public class UniqueObjectReferenceSetter : MonoBehaviour
{
    public enum RegisteredType
    {
        NotFound,
        CinemachineVirtualCamera,

    }

    public Component detectedComponentToSet = null;

    RefUpdater<GameObject> subscribedToChanges = null;

    private void Start()
    {
        searchComponent();

        updateValue();

        //Add other possible component comportement if needed!
    }
    public void searchComponent()
    {
        foreach (Component component in GetComponents<Component>())
        {
            if (component.GetType() == typeof(CinemachineVirtualCamera) ||
                component.GetType() == typeof(AdditiveSceneLoader))
            {
                detectedComponentToSet = component;
            }
            //Add other possible component search if needed!


            if (detectedComponentToSet != null)
                break;
        }
    }

    public void updateValue(GameObject newValue = null)
    {
        if (detectedComponentToSet.GetType() == typeof(CinemachineVirtualCamera))
        {
            CinemachineVirtualCamera cineVCam = detectedComponentToSet as CinemachineVirtualCamera;
            RefUpdater<GameObject> playerGO = UniqueObject.GetByLabel("Player");
            SetCinemachineVirtualCamRefs(cineVCam, playerGO.Reference);
            if (subscribedToChanges == null)
            {
                playerGO.OnReferenceChanged += updateValue;
                subscribedToChanges = playerGO;
            }
        }
        else if (detectedComponentToSet.GetType() == typeof(AdditiveSceneLoader))
        {
            AdditiveSceneLoader sceneLoader = detectedComponentToSet as AdditiveSceneLoader;
            RefUpdater<GameObject> playerGO = UniqueObject.GetByLabel("Player");
            SetAdditiveSceneLoaderRefs(sceneLoader, playerGO.Reference);
            if (subscribedToChanges == null)
            {
                playerGO.OnReferenceChanged += updateValue;
                subscribedToChanges = playerGO;
            }
        }
    }

    public void SetCinemachineVirtualCamRefs(CinemachineVirtualCamera cineVCam, GameObject value)
    {
        if (value != null)
        {
            if (gameObject.scene != SceneManager.GetActiveScene())
            {
                cineVCam.Priority = 0;
            }

            cineVCam.Follow = value.transform;
            cineVCam.LookAt = value.transform;
        }
    }

    public void SetAdditiveSceneLoaderRefs(AdditiveSceneLoader sceneLoader, GameObject value)
    {
        sceneLoader.checkObject = value;
        sceneLoader.manualCheckTrigger();
    }

    private void OnDestroy()
    {
        if (subscribedToChanges != null)
            subscribedToChanges.OnReferenceChanged -= updateValue;
    }
}

#if UNITY_EDITOR
[CanEditMultipleObjects]
[CustomEditor(typeof(UniqueObjectReferenceSetter))]
public class ReferenceSetterEditor : Editor
{
    SerializedProperty detectedComponentToSetproperty;

    private void OnEnable()
    {
        detectedComponentToSetproperty = serializedObject.FindProperty("detectedComponentToSet");
        foreach (UniqueObjectReferenceSetter refSet in serializedObject.targetObjects)
        {
            refSet.searchComponent();
        }
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Search Component to set"))
        {
            foreach (UniqueObjectReferenceSetter refSet in serializedObject.targetObjects)
            {
                refSet.searchComponent();
            }
        }

        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.PropertyField(detectedComponentToSetproperty);
        EditorGUI.EndDisabledGroup();

        serializedObject.ApplyModifiedProperties();
    }

}
#endif



