using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "CharacterController/State/Pushing" )]
public class CC_State_Pushing : CC_State
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Tooltip("The maximum speed the character can have while pushing")]
    public          float m_maxWalkSpeed    = 5f;
    [Tooltip("The acceleration of the character")]
    public          float m_acceleration    = 5f;
    [Tooltip("The acceleration when the character change direction")]
    public          float m_turnOverAcc     = 10f;
    [Tooltip("The friction when the character is on the Ground")]
    public          float m_groundDrag      = 0.001f;

    public          float m_decceleration   = 5f;

    [Tooltip("The gravity on the ground")]
    public          float m_groundedGravity = -.05f;

    private         float m_currentSpeed;
    private         float m_currentMaxSpeed;

    [HideInInspector]
    public          Vector3 m_targetPos;
    private const   float   m_minSpeedToTarget = 5f;
    private         bool    m_inTargetPos;
    private         float   m_previousSpeed;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        if ( CheckChangeState() )
            return;

        HandleGravity(  );
        HandleSpeed(  );

        if ( !m_inTargetPos )
        {
            float step = Mathf.Max(m_minSpeedToTarget, m_previousSpeed) * Time.deltaTime;
            instanceCC.transform.position = Vector3.MoveTowards( instanceCC.transform.position, m_targetPos, step );
            if ( Vector3.Distance( instanceCC.transform.position, m_targetPos ) < 0.01f )
            {
                m_inTargetPos = true;
            }
        }
        else
        {
            if ( !instanceCC.m_ObjectPushable.GetComponent<PushableObjectMovement>().m_isPushed )
            {
                instanceCC.m_ObjectPushable.GetComponent<PushableObjectMovement>().BeginPush();
            }
            Vector3 forward = Vector3.Cross( instanceCC.transform.right, instanceCC.m_normalGround );

            instanceCC.m_currentMovement = forward * ( instanceCC.m_currentSpeed * ( ( instanceCC.m_isLookingRight ) ? 1 : -1 ) ) + Vector3.up * instanceCC.m_current2DMovement.y;
            instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;
        }
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        if ( !instanceCC.m_isInteractPressed || ( !instanceCC.m_ObjectPushable ) )
        {
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Walking );
            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnExitState( )
    {
        if ( instanceCC.m_ObjectPushable )
        {
            instanceCC.m_ObjectPushable.GetComponent<PushableObjectMovement>().EndPush();
        }
        m_targetPos = Vector3.zero;
        instanceCC.m_currentSpeed = 0;
        instanceCC.m_previousSpeed = 0;
        instanceCC.m_isPushing = false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState( )
    {
        m_targetPos = instanceCC.m_ObjectPushable.GetComponent<OnTrigger_InfosToPlayer>().GetTargetPos();
        m_inTargetPos = m_targetPos == Vector3.zero;
        m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_isPushing = true;

        instanceCC.m_currentSpeed = 0;
        instanceCC.m_currentMaxSpeed = m_maxWalkSpeed;
        m_currentMaxSpeed = m_maxWalkSpeed;
    }

    //--------------------------------------------------------------------------------------\\
    void HandleSpeed(  )
    {
        m_currentMaxSpeed = m_maxWalkSpeed * Mathf.Abs( instanceCC.m_currentMovementInput );
        if ( Mathf.Abs( m_currentSpeed ) <= m_currentMaxSpeed)
        {
            float acc = ( ( Mathf.Sign(m_currentSpeed) == Mathf.Sign(instanceCC.m_currentMovementInput) ) ) ? m_acceleration : m_turnOverAcc;
            float addedSpeed = (acc * Time.fixedDeltaTime * instanceCC.m_currentMovementInput);
            m_currentSpeed = Mathf.Clamp(m_currentSpeed + addedSpeed, -m_currentMaxSpeed, m_currentMaxSpeed );
        }
        else
        {
            m_currentSpeed *= Mathf.Pow(m_groundDrag, Time.fixedDeltaTime);
            if (m_currentSpeed < 0.01f && m_currentSpeed > -0.01f)
                m_currentSpeed = 0f;
        }

        instanceCC.m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_currentSpeed = m_currentSpeed;
    }

    //--------------------------------------------------------------------------------------\\
    void HandleGravity( )
    {
        instanceCC.m_current2DMovement.y = m_groundedGravity;
    }
}
