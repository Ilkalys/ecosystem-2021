using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shader_SetFillQuantity_OverAnim : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  AnimationCurve  m_speedOverTime;
    private Animation       m_animation;

    public  bool            m_setFillQuantity;
    private bool            m_alreadyAnimated;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Awake()
    {
        m_animation = this.GetComponent<Animation>();
        m_setFillQuantity = false;
        m_alreadyAnimated = false;
    }

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        if (m_setFillQuantity)
        {
            float currentTime = m_animation["ShaderFill"].normalizedTime;
            if (!m_animation.isPlaying)
            {
                m_setFillQuantity = false;
                m_alreadyAnimated = true;
            }
            else
            {
                m_animation["ShaderFill"].speed = m_speedOverTime.Evaluate(currentTime);
            }
        }
    }

    //--------------------------------------------------------------------------------------\\
    public void Fill()
    {
        if (!m_alreadyAnimated)
        {
            m_animation.Play("ShaderFill");
            m_setFillQuantity = true;
        }
    }
}
