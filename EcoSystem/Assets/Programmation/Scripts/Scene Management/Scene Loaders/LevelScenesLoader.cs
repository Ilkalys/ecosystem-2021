using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEditor;
using UnityEngine.EventSystems;

/// <summary>
/// A component to loads additively the scenes necessary for specifics levels 
/// </summary>
public class LevelScenesLoader : MonoBehaviour
{
    [Tooltip("The list of levels and their linked scenes to load")]
    public LevelsScenesDistribution scenesDistribution;
    
    [Tooltip("An image of type Filled to be updated with the completion percentage as scenes are loading")]
    public Image loadingBar = null;

    [Tooltip("Link to a TMP_Dropdown to populate with the levelDetails list and to use for LoadDropdownSelectedLevel method")]
    public TMP_Dropdown levelSelectDropdown;

    [Tooltip("Link to a \"Continue\" button that will be deactivated at startup if no save for \"LastLevelLoaded\" is found and activated otherwise")]
    public Button continueButton;


    bool UseLoadingBar
    {
        get
        {
            if (loadingBar != null)
            {
                if (loadingBar.type == Image.Type.Filled)
                    return true;
                else
                    Debug.LogError("Could not use given image as loading bar : it needs to be set as a Filled image type !");
            }
            return false;
        }
    }

    public bool isLoading { get; private set; } = false;


    public void Start()
    {
        Time.timeScale = 1;
        if ( levelSelectDropdown != null)
            PopulateDropdownWithLevel(levelSelectDropdown);

        if (continueButton != null)
            continueButton.gameObject.SetActive(PlayerPrefs.GetInt("LastLevelLoaded", -1) != -1);
    }

    public void PopulateDropdownWithLevel(TMP_Dropdown dropdown)
    {
        dropdown.ClearOptions();
        for (int i = 0; i < scenesDistribution.levelsDetails.Count; i++)
        {
            dropdown.options.Add(new TMP_Dropdown.OptionData("Level " + i));
        }
        dropdown.RefreshShownValue();
    }

    public void LoadLastLevel()
    {
        int lastLevelLoaded = PlayerPrefs.GetInt("LastLevelLoaded", -1);
        if (lastLevelLoaded == -1)
        {
            PlayerPrefs.SetInt("LastLevelLoaded", 0);
            LoadLevel(0);
        }
        else
        {
            LoadLevel(lastLevelLoaded);
        }
    }
    public void LoadDropdownSelectedLevel()
    {
        if (levelSelectDropdown is null)
        {
            Debug.LogError("You need to link a dropdown in Level Select Dropdown property before calling this method!");
        }
        else
        {
            LoadLevel(levelSelectDropdown.value);
        }
    }

    public void LoadLevel(int levelIndex)
    {
        if (!isLoading)
        {
            isLoading = true;
            StartCoroutine(LoadLevel(scenesDistribution.levelsDetails[levelIndex], scenesDistribution.globalScene));
        }
    }

    IEnumerator LoadLevel(LevelScenesDetails levelToLoad, string globalSceneName)
    {
        List<AsyncOperation> loadingScenes = new List<AsyncOperation>();

        if (SceneManager.sceneCount > 1)
            Debug.LogWarning("It seems that more than one scene is already loaded but this " +
                "code only unload the currently active one for now. Don't hesitate to add the " +
                "necessary instruction to unload all of them in this coroutine!\n" +
                "Currently (before the additive load) there is " + SceneManager.sceneCount + " scenes loaded");

        // Launch Scene loads
        loadingScenes.Add(SceneManager.LoadSceneAsync(levelToLoad.GetPrimaryScene, LoadSceneMode.Additive));
        loadingScenes[0].allowSceneActivation = false;
        for (int i = 0; i < levelToLoad.scenesToLoad.Count; i++)
        {
            if (i == levelToLoad.indexOfPrimaryScene)
                continue;

            loadingScenes.Add(SceneManager.LoadSceneAsync(levelToLoad.scenesToLoad[i], LoadSceneMode.Additive));
            loadingScenes[loadingScenes.Count-1].allowSceneActivation = false;
        }
        if (globalSceneName != null && globalSceneName != "")
        {
            loadingScenes.Add(SceneManager.LoadSceneAsync(globalSceneName, LoadSceneMode.Additive));
            loadingScenes[loadingScenes.Count - 1].allowSceneActivation = false;
        }

        // display scene load progress
        float totalProgress = 0;
        for (int i = 0; i < loadingScenes.Count; ++i)
        {
            while (loadingScenes[i].progress < 0.9f)
            {
                totalProgress = (i + loadingScenes[i].progress) / loadingScenes.Count;
                if (UseLoadingBar)
                {
                    loadingBar.fillAmount = totalProgress;
                }
                yield return null; // wait for next frame
            }
        }

        // Deactivate current scene element that might interfere with the new ones'
        EventSystem eventSystem = EventSystem.current;
        if (eventSystem != null)
            eventSystem.gameObject.SetActive(false);
        Scene sceneToUnload = SceneManager.GetActiveScene();

        // activate loaded scenes (we cannot unload the current one before at least one other is ready to become the active scene)
        loadingScenes[0].completed += (AsyncOperation _) => SceneManager.SetActiveScene(SceneManager.GetSceneByName(levelToLoad.GetPrimaryScene)); //Carefull CMvirtualsCamera priority is redefined if not in the active scene at load time (so thanks to this line, think before modifing it!)
        for (int i = 0; i < loadingScenes.Count; ++i)
        {
            loadingScenes[i].allowSceneActivation = true;
            if (i == 0)
            {
                yield return new WaitUntil(() => loadingScenes[0].isDone);
            }
        }

        // Wait for all scene loaded 
        for (int i = 0; i < loadingScenes.Count; ++i)
        {
            while (!loadingScenes[i].isDone)
            {
                yield return null;
            }
        }

        // Unload previous scene (and thus this gameobject and its coroutines)
        SceneManager.UnloadSceneAsync(sceneToUnload);
    }

    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Application.Quit called !");
    }    
}


#if UNITY_EDITOR

[CanEditMultipleObjects]
[CustomEditor(typeof(LevelScenesLoader))]
public class LevelSelectAndLoadEditor : Editor
{
    SerializedProperty scenesDistributionProperty;
    SerializedProperty loadingBarProperty;
    SerializedProperty continueButtonProperty;
    SerializedProperty levelSelectDropdownProperty;
    bool isFoldout;

    private void OnEnable()
    {
        scenesDistributionProperty = serializedObject.FindProperty("scenesDistribution");
        loadingBarProperty = serializedObject.FindProperty("loadingBar");
        continueButtonProperty = serializedObject.FindProperty("continueButton");
        levelSelectDropdownProperty = serializedObject.FindProperty("levelSelectDropdown");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(scenesDistributionProperty);

        isFoldout = EditorGUILayout.BeginFoldoutHeaderGroup(isFoldout, "Optional : UI links");
        if (isFoldout)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(loadingBarProperty);
            EditorGUILayout.PropertyField(continueButtonProperty);
            EditorGUILayout.PropertyField(levelSelectDropdownProperty);
            EditorGUI.indentLevel--;
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        serializedObject.ApplyModifiedProperties();
    }
}

#endif