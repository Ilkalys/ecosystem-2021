using CustomsAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

/// <summary>
/// Timer component that exposes the Timer class as a MonoBehaviour
/// </summary>
public class TimerComponent : MonoBehaviour
{
    public Timer timer = new Timer(1f);

    public bool autoStart;

    public void Start()
    {
        // This first line would not be needed if this was a whole new script but since some values
        // (_timeLeft especially) might not have been serialized it is safer to have it!
        timer.Reset();  

        if (autoStart)
            TimerStart();
    }
    public void Update() =>
        timer.Update(Time.deltaTime);

    public void TimerStart() =>
        timer.Start();
    public void TimerStop() =>
        timer.Stop();
    public void TimerPause() =>
        timer.Pause();
    public void TimerResume() =>
        timer.Resume();
    public void TimerReset(float newWaitTime = -1) =>
        timer.Reset(newWaitTime);
    public void TimerRestart(float newWaitTime = -1) =>
        timer.Restart(newWaitTime);

}