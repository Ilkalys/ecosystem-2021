using CustomsAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
/// <summary>
/// A class that combine the fonctionnality and the accessibility of UnityEvents and Csharp events 
/// and that can be easily cast to one of them if needed
/// </summary>
[Serializable]
public class USharpEvent : UnityEvent
{
    public delegate void CSharpEventHandler();
    
    private event CSharpEventHandler CSharpEvent;
    [SerializeField, ShowOnly(true)] 
    private int CSharpCalls = 0;

    public new void Invoke()
    {
        CSharpEvent?.Invoke();
        base.Invoke();
    }

    public void AddListener(CSharpEventHandler call)
    {
        CSharpEvent += call;
        CSharpCalls++;
    }

    public void RemoveListener(CSharpEventHandler call)
    {
        CSharpEvent -= call;
        CSharpCalls--;
    }

    public static explicit operator CSharpEventHandler(USharpEvent uSharpEvent)
    {
        return uSharpEvent.CSharpEvent;
    }
}

// TODO a custom inspector that is able to display all currently subscribed events from unity and from the code.
// if code information is too complex to get perhaps this is a solution that could be optionnably enabled for debugging at least 
// -> https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/attributes/caller-information

// but : https://docs.microsoft.com/en-us/dotnet/api/system.delegate.getinvocationlist?view=net-6.0 should be enough

/*
#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(USharpEvent))]
public class USharpEventProperty : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        UnityEventPropertyDrawer.OnGUI(position, property, label);
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true) *2;
    }
}

#endif

//*/