using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using DebugHelpers;
using System.Reflection;
using UnityEngine.SceneManagement;

namespace CustomsAttributes
{
    /// <summary>
    /// TODO finish implementation, replace in code and test!
    /// /// A small class to be evaluated as a bool whose value is directly derived from one or two SerializedProperties values.
    /// if malformed this will always evaluate to false.
    /// </summary>
#if UNITY_EDITOR
    public class FieldBasedTest
    {
        public enum DataType
        {
            Boolean,
            Numerical
        }
        public enum TestType
        {
            Equals,
            GreaterThan,
            LowerThan,
            GreaterOrEqualTo,
            LowerOrEqualTo,
            Different
        }

        SerializedObject serializedObject;

        //First value to evaluate
        string serializedProperty1Path;

        //What is the type of the serialized property 
        DataType dataType;

        //Which comparison test should be done. null value will evaluate directly the first property as a bool.
        TestType? testType;

        //Second value to compare to (only if testType != null)
        string serializedProperty2Path;
        double? numValue;
        bool? boolValue;

        //TODO change constructors to basic values only (and calls them it in attributes constructors instead of attribute directly) (need basic parser via string ideally)
        public FieldBasedTest(string serializedProperty1Path, TestType testType, string serializedProperty2Path, DataType dataType, SerializedObject serializedObject = null)
        {
            this.dataType = dataType;
            this.serializedProperty1Path = serializedProperty1Path;
            this.testType = testType;
            this.serializedProperty2Path = serializedProperty2Path;
            this.numValue = null;
            this.boolValue = null;
            this.serializedObject = serializedObject;
        }
        public FieldBasedTest(string serializedProperty1Path, TestType testType, double numValue, SerializedObject serializedObject = null)
        {
            this.dataType = DataType.Numerical;
            this.serializedProperty1Path = serializedProperty1Path;
            this.testType = testType;
            this.serializedProperty2Path = null;
            this.numValue = numValue;
            this.boolValue = null;
            this.serializedObject = serializedObject;
        }
        public FieldBasedTest(string serializedProperty1Path, TestType testType, bool boolValue, SerializedObject serializedObject = null)
        {
            this.dataType = DataType.Boolean;
            this.serializedProperty1Path = serializedProperty1Path;
            this.testType = testType;
            this.serializedProperty2Path = null;
            this.numValue = null;
            this.boolValue = boolValue;
            this.serializedObject = serializedObject;
        }
        public FieldBasedTest(string serializedBoolPropertyPath, SerializedObject serializedObject = null)
        {
            this.dataType = DataType.Boolean;
            this.serializedProperty1Path = serializedBoolPropertyPath;
            this.testType = null;
            this.serializedProperty2Path = null;
            this.numValue = null;
            this.boolValue = null;
            this.serializedObject = serializedObject;
        }

        public void setSerializedObject(SerializedObject serializedObject)
        {
            this.serializedObject = serializedObject;
        }

        bool tryGetNumValue(SerializedProperty numProperty, out double result)
        {
            result = 0;
            switch (numProperty.propertyType)
            {
                case SerializedPropertyType.Float:
                    result = numProperty.doubleValue;
                    return true;

                case SerializedPropertyType.Integer:
                    result = (double)numProperty.intValue;
                    return true;

                case SerializedPropertyType.Boolean:
                    Debug.LogError("SerializedProperty is of type Boolean but a Numerical value was expected");
                    return false;

                default:
                    Debug.LogError("SerializedProperty is of a type not handeld by FieldBasedTest");
                    return false;
            }
        }
        bool tryGetBoolValue(SerializedProperty boolProperty, out bool result)
        {
            result = false;
            switch (boolProperty.propertyType)
            {
                case SerializedPropertyType.Boolean:
                    result = boolProperty.boolValue;
                    return true;

                case SerializedPropertyType.Integer:
                case SerializedPropertyType.Float:
                    Debug.LogError("SerializedProperty is of type Numerical but a Boolean value was expected");
                    return false;

                default:
                    Debug.LogError("SerializedProperty is of a type not handeld by FieldBasedTest");
                    return false;
            }
        }

        /// <summary>
        /// Search for the properties inside the serialized object and evaluate them as precised at construction.
        /// </summary>
        /// <param name="serializedObject"></param>
        /// <returns>The result of the evaluation, or false if any error is encountered</returns>
        public bool evaluate()
        {
            if (serializedObject == null)
            {
                Debug.LogError("You need to set up the serialized object before evaluating a FieldBasedTest, returning false as default.");
                return false;
            }

            SerializedProperty property = serializedObject.FindProperty(serializedProperty1Path);
            if (property == null)
            {
                Debug.LogError("Property " + serializedProperty1Path + " not found! Returning false as default.");
                return false;
            }
            SerializedProperty property2 = null;
            if (serializedProperty2Path != null)
            {
                property2 = serializedObject.FindProperty(serializedProperty2Path); 
                if (property2 == null)
                {
                    Debug.LogError("Property " + serializedProperty2Path + " not found! Returning false as default.");
                    return false;
                }
            }

            if (dataType == DataType.Boolean)
            {
                bool result = false;

                if (!tryGetBoolValue(property, out result))
                {
                    Debug.LogError("evaluate() returning false as default.");
                    return false;
                }

                if (testType == null)
                {
                        return result;
                }

                if (boolValue == null)
                {
                    if (tryGetBoolValue(property2, out bool boolValue))
                        this.boolValue = boolValue;
                    else
                    {
                        Debug.LogError("evaluate() returning false as default.");
                        return false;
                    }
                }
                switch (testType)
                {
                    case TestType.Equals:
                        return result == (bool)boolValue;

                    case TestType.Different:
                        return result != (bool)boolValue;

                    default:
                        Debug.LogError("DataType is supposed to be Boolean but the TestType is not \"Equals\" or \"Different\", returning false as default.");
                        return false;
                }
            }
            else if(dataType == DataType.Numerical)
            {
                double result = 0;

                if (!tryGetNumValue(property, out result))
                {
                    Debug.LogError("evaluate() returning false as default.");
                    return false;
                }

                if (numValue == null)
                {
                    if (tryGetNumValue(property2, out double numValue))
                        this.numValue = numValue;
                    else
                    {
                        Debug.LogError("evaluate() returning false as default.");
                        return false;
                    }
                }
                switch (testType)
                {
                    case TestType.Equals:
                        return result == numValue;

                    case TestType.Different:
                        return result != numValue;

                    case TestType.LowerThan:
                        return result < numValue;

                    case TestType.LowerOrEqualTo:
                        return result <= numValue;

                    case TestType.GreaterThan:
                        return result > numValue;

                    case TestType.GreaterOrEqualTo:
                        return result >= numValue;

                    default:
                        Debug.LogError("Unrecognized testType value, returning false as default.");
                        return false;
                }
            }

            Debug.LogError("Unknown error during evaluation, returning false as default.");
            return false;
        }

        public static implicit operator bool(FieldBasedTest t) => t.evaluate();
    }

#endif
    /// <summary>
    /// the basic version of the previous class...
    /// </summary>
    public class Evaluable
    {
        bool? boolValue;
        string boolPropertyPath;
        bool IsFixed => boolValue != null;

        public Evaluable(bool boolValue)
        {
            this.boolValue = boolValue;
            this.boolPropertyPath = null;
        }

        public Evaluable(string boolPropertyPath)
        {
            this.boolPropertyPath = boolPropertyPath;
            this.boolValue = null;
        }

#if UNITY_EDITOR
        public bool? evaluateAttribute(SerializedObject serializedObject)
        {
            if (IsFixed)
            {
                return boolValue.Value;
            }
            else
            { 
                if (serializedObject == null)
                {
                    Debug.LogError("Invalid serialized object given! returning Mixed value");
                    return null;
                }

                tryObjectEditorUpdate(serializedObject);

                SerializedProperty boolProperty = serializedObject.FindProperty(boolPropertyPath);
                if (boolProperty == null)
                {
                    Debug.LogError("Property " + boolPropertyPath + " not found! Default value is Display = true");
                    return null;
                }
                else
                {
                    if (boolProperty.hasMultipleDifferentValues)
                        return null;
                    else
                        return boolProperty.boolValue;
                }
            }
        }

        public void tryObjectEditorUpdate(SerializedObject serializedObject)
        {
            //if the kind of object defines an EditorUpdate Method we call it on each underlying object!
            MethodInfo updateEditorMethod = serializedObject.targetObjects[0].GetType().GetMethod("EditorUpdate");
            if (updateEditorMethod != null)
            {
                foreach (UnityEngine.Object targetObject in serializedObject.targetObjects)
                {
                    updateEditorMethod.Invoke(targetObject, null);
                }
            }
        }
#endif
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ShowOnly : PropertyAttribute
    {
        public Evaluable evaluable;
        public ShowOnly(bool isShowOnly = true)
        {
            evaluable = new Evaluable(isShowOnly);
        }

        public ShowOnly(string boolPropertyPath)
        {
            evaluable = new Evaluable(boolPropertyPath);
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class BeginShowOnlyGroup : PropertyAttribute
    {
        public Evaluable evaluable;
        public BeginShowOnlyGroup(bool isShowOnly = true)
        {
            evaluable = new Evaluable(isShowOnly);
        }

        public BeginShowOnlyGroup(string boolPropertyPath)
        {
            evaluable = new Evaluable(boolPropertyPath);
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class EndShowOnlyGroup : PropertyAttribute
    {
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class ConditionnalDisplay : PropertyAttribute
    {
        public Evaluable evaluable;
        public ConditionnalDisplay(bool shouldDipslay = true)
        {
            evaluable = new Evaluable(shouldDipslay);
        }

        public ConditionnalDisplay(string boolPropertyPath)
        {
            evaluable = new Evaluable(boolPropertyPath);
        }
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
    public class ButtonCall : PropertyAttribute
    {
        public string buttonName;
        public Type type;
        public string methodName;
        public object[] parameters;
        public float height;

        public ButtonCall(string buttonName, Type type, string methodName, params object[] parameters)
        {
            this.buttonName = buttonName;
            this.type = type;
            this.methodName = methodName;
            this.parameters = parameters;

#if UNITY_EDITOR
            this.height = EditorGUIUtility.singleLineHeight;
#else            
            this.height = 20;
#endif
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ShowOnly))]
    public class ShowOnlyPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            bool showOnly = (attribute as ShowOnly).evaluable.evaluateAttribute(property.serializedObject) ?? true;

            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginDisabledGroup(showOnly);
            EditorGUI.PropertyField(position, property, label, true);
            EditorGUI.EndDisabledGroup();
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);              
        }
    }

    [CustomPropertyDrawer(typeof(BeginShowOnlyGroup))]
    public class BeginShowOnlyGroupPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            bool showOnly = (attribute as ShowOnly).evaluable.evaluateAttribute(property.serializedObject) ?? true;

            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginDisabledGroup(showOnly);
            EditorGUI.PropertyField(position, property, label, true);
            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label, true);
        }
    }
    
    [CustomPropertyDrawer(typeof(EndShowOnlyGroup))]
    public class EndShowOnlyGroupDecoratorDrawer : DecoratorDrawer
    {
        public override void OnGUI(Rect position)
        {
            EditorGUI.EndDisabledGroup();
        }

        public override float GetHeight()
        {
            return 0;
        }
    }

    [CustomPropertyDrawer(typeof(ConditionnalDisplay))]
    public class ConditionnalDisplayPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            bool? shouldDisplay = (attribute as ConditionnalDisplay).evaluable.evaluateAttribute(property.serializedObject);
            bool showOnly = shouldDisplay == null;

            if (shouldDisplay ?? true)
            {
                EditorGUI.BeginProperty(position, label, property);
                EditorGUI.BeginDisabledGroup(showOnly);
                EditorGUI.PropertyField(position, property, label, true);
                EditorGUI.EndDisabledGroup();
                EditorGUI.EndProperty();
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if ((attribute as ConditionnalDisplay).evaluable.evaluateAttribute(property.serializedObject) ?? true)
                return EditorGUI.GetPropertyHeight(property, label, true);
            else
                return -EditorGUIUtility.standardVerticalSpacing;
        }
    }

    //
    [CustomPropertyDrawer(typeof(ButtonCall))]
    public class ButtonCallPropertyDrawer : DecoratorDrawer
    {
        public void MakeAttributeButton(Rect buttonPos)
        {
            ButtonCall buttonCall = attribute as ButtonCall;

            if (GUI.Button(buttonPos, buttonCall.buttonName))
            {
                List<Type> paramType = new List<Type>();
                foreach (object o in buttonCall.parameters)
                {
                    paramType.Add(o.GetType());
                }

                MethodInfo methodInfo = buttonCall.type.GetMethod(buttonCall.methodName, paramType.ToArray());
                if (methodInfo != null && methodInfo.IsStatic)
                {
                    methodInfo.Invoke(null, buttonCall.parameters);
                }
                else
                    Debug.LogError("Could not find specified static method");
            }
        }

        public override void OnGUI(Rect position)
        {
            MakeAttributeButton(position);
        }

        public override float GetHeight()
        {
            return (attribute as ButtonCall).height;
        }
    }
#endif


}
