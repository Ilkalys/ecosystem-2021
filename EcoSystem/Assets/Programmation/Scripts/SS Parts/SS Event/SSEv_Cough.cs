using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SSEv_Cough : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                        ATTRIBUTES                                                   \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public int          m_numberOfCough;
    public float        m_coughIntensity;

    [Tooltip("The position that the player will have at the beginning of the cough animation, None = stay in place")]
    public Transform    targetPos;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void Cough()
    {
        SS_Character_Controller.instance.Cough( m_numberOfCough, m_coughIntensity, targetPos ? targetPos.position : Vector3.zero);
    }
}
