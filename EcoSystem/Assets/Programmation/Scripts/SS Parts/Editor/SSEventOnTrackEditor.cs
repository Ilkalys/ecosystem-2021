using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SSEventOnTrack))]
public class SSEventOnTrackEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SSEventOnTrack eventOnTrack = (SSEventOnTrack)target;

        GUILayout.Label("Preset of the position of the Event");
        GUILayout.BeginHorizontal();
        if (eventOnTrack.m_pathParams.path)
        {
            if (GUILayout.Button("Begin + 1"))
            {
                int result = (int)eventOnTrack.m_pathParams.path.MaxUnit(Cinemachine.SSTrack.PositionUnits.PathUnits) > 1 ? 1 : 0;

                SetPosValue(eventOnTrack, result);

                eventOnTrack.m_pathParams.positionOnThePath = eventOnTrack.m_pathParams.path.FromPathNativeUnits(result, eventOnTrack.m_pathParams.positionUnits);
            }
            else if (GUILayout.Button("Middle"))
            {
                float result = eventOnTrack.m_pathParams.path.MaxUnit(Cinemachine.SSTrack.PositionUnits.PathUnits) / 2;

                SetPosValue(eventOnTrack, result);

                eventOnTrack.m_pathParams.positionOnThePath = eventOnTrack.m_pathParams.path.FromPathNativeUnits(result, eventOnTrack.m_pathParams.positionUnits);
            }
            else if (GUILayout.Button("End - 1"))
            {
                int result = (int)eventOnTrack.m_pathParams.path.MaxUnit(Cinemachine.SSTrack.PositionUnits.PathUnits);
                result = result > 1 ? result - 1 : 1;

                SetPosValue(eventOnTrack, result);

                eventOnTrack.m_pathParams.positionOnThePath = eventOnTrack.m_pathParams.path.FromPathNativeUnits(result, eventOnTrack.m_pathParams.positionUnits);
            }
        }
        else
        {
            GUILayout.Label("Can't preset without a Path");
        }
        GUILayout.EndHorizontal();
    }

    private void SetPosValue(SSEventOnTrack eventOnTrack, float value)
    {
        SerializedObject SO = new UnityEditor.SerializedObject(eventOnTrack);
        SerializedProperty posOnPath = SO.FindProperty("m_pathParams.positionOnThePath");

        posOnPath.floatValue = value;
        SO.ApplyModifiedProperties();
    }
}