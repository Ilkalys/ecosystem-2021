using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SS_MixCameraWithDist : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                        INTERNAL STRUCT                                                   \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [System.Serializable]
    public class AreaCamera
    {
        public Transform beginPoint, endPoint;

    }

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  AreaCamera[]                        m_cameraPoints;
    public  AnimationCurve[]                    mixOverDist;
    private Cinemachine.CinemachineMixingCamera m_mixingCamera;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void Start()
    {
        m_mixingCamera = this.GetComponent<Cinemachine.CinemachineMixingCamera>();
    }

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        if ( m_mixingCamera )
        {
            if ( m_mixingCamera.Priority > 0 )
            {
                if ( m_mixingCamera.ChildCameras.Length == m_cameraPoints.Length )
                {
                    int indexPointMinDist = FindClosestPoint(1,Vector3.Distance( SS_Character_Controller.instance.transform.position, m_cameraPoints[0].beginPoint.position ) );
                    if ( indexPointMinDist == 0 )
                    {
                        WeightOnTwoCameraOrLess( 0, 100, 1 );
                    }
                    else if ( indexPointMinDist == m_cameraPoints.Length * 2 - 1 )
                    {
                        WeightOnTwoCameraOrLess( m_mixingCamera.ChildCameras.Length - 1, 100, 0);
                    }
                    else
                    {

                        // CLOSEST = BEGIN POINT
                        if ( indexPointMinDist % 2 == 0 )
                        {
                            Vector3 normal = (m_cameraPoints[(indexPointMinDist-1)/2].endPoint.position - m_cameraPoints[indexPointMinDist/2].beginPoint.position).normalized ;
                            Vector3 projectPos = Vector3.Project(SS_Character_Controller.instance.transform.position-m_cameraPoints[indexPointMinDist/2].beginPoint.position, normal) + m_cameraPoints[indexPointMinDist/2].beginPoint.position;
                          
                            float distBetweenArea = Vector3.Distance( m_cameraPoints[indexPointMinDist/2].beginPoint.position, m_cameraPoints[(indexPointMinDist-1)/2].endPoint.position    );
                            float distToPreviousArea = Vector3.Distance( projectPos, m_cameraPoints[(indexPointMinDist-1)/2].endPoint.position );

                            if ( distBetweenArea < distToPreviousArea )
                            {
                                WeightOnTwoCameraOrLess( indexPointMinDist / 2, 100, 0 );
                            }
                            else
                            {
                                float distToBeginArea = Vector3.Distance( projectPos, m_cameraPoints[indexPointMinDist/2].beginPoint.position );
                                float totalDist = distToBeginArea + distToPreviousArea;
                                WeightOnTwoCameraOrLess( indexPointMinDist / 2 - 1, distToBeginArea * 100 / totalDist, indexPointMinDist / 2);
                            }
                        }
                        // CLOSEST = END POINT
                        else
                        {
                            Vector3 normal = (m_cameraPoints[(indexPointMinDist+1)/2].beginPoint.position - m_cameraPoints[indexPointMinDist/2].endPoint.position).normalized ;
                            Vector3 projectPos = Vector3.Project(SS_Character_Controller.instance.transform.position - m_cameraPoints[indexPointMinDist/2].endPoint.position, normal) + m_cameraPoints[indexPointMinDist/2].endPoint.position;

                            float distBetweenArea = Vector3.Distance( m_cameraPoints[indexPointMinDist/2].endPoint.position, m_cameraPoints[(indexPointMinDist+1)/2].beginPoint.position    );
                            float distToNextArea = Vector3.Distance( projectPos, m_cameraPoints[(indexPointMinDist+1)/2].beginPoint.position );

                            if ( distBetweenArea < distToNextArea )
                            {
                                WeightOnTwoCameraOrLess( indexPointMinDist / 2, 100, 0 );
                            }
                            else
                            {
                                float distToEndArea = Vector3.Distance( projectPos, m_cameraPoints[indexPointMinDist/2].endPoint.position );
                                float totalDist = distToEndArea + distToNextArea;
                                WeightOnTwoCameraOrLess( indexPointMinDist / 2, distToNextArea * 100 / totalDist, indexPointMinDist / 2 + 1 );
                            }
                        }
                    }
                }
                else
                {
                    Debug.LogError( "SS_MixCameraWithDist : not enough cameraPoints : " + m_cameraPoints.Length + "/" + m_mixingCamera.ChildCameras.Length );
                }
            }
        }
        else
        {
            Debug.LogError( "SS_MixCameraWithDist : no MixingCamera attached to this gameobject : " + this.gameObject );
            return;
        }
    }

    //--------------------------------------------------------------------------------------\\
    public void WeightOnTwoCameraOrLess( int indexCamOne, float pourventCamOne, int indexCamTwo )
    {
        bool withanim = false;
        float animCurveValue = -1;
        if ( indexCamOne < mixOverDist.Length )
        {
            AnimationCurve animCurve = mixOverDist[indexCamOne];
            if ( animCurve != null )
            {
                withanim = true;
                animCurveValue = 100 - animCurve.Evaluate( (100-pourventCamOne) / 100 ) * 100;
            }

        }
        for ( int i = 0; i < m_mixingCamera.ChildCameras.Length; i++ )
        {
            if ( i == indexCamOne )
            {
                m_mixingCamera.SetWeight( i,  withanim  ? animCurveValue : pourventCamOne );
            }
            else if ( i == indexCamTwo )
            {
                m_mixingCamera.SetWeight( i, 100 - ( withanim ? animCurveValue : pourventCamOne) );
            }
            else
            {
                m_mixingCamera.SetWeight( i, 0 );
            }
        }
    }

    //--------------------------------------------------------------------------------------\\
    public int FindClosestPoint( int currentIndex, float currentMinDist )
    {

        float currentDist;
        if ( currentIndex % 2 == 0 )
        {
            currentDist = Vector3.Distance( SS_Character_Controller.instance.transform.position, m_cameraPoints[currentIndex / 2].beginPoint.position );
        }
        else
        {
            currentDist = Vector3.Distance( SS_Character_Controller.instance.transform.position, m_cameraPoints[currentIndex / 2].endPoint.position );
        }

        if ( currentDist < currentMinDist )
        {
            if ( currentIndex + 1 >= m_cameraPoints.Length * 2 )
            {
                return currentIndex;
            }
            else
            {
                return FindClosestPoint( currentIndex + 1, currentDist );
            }
        }
        else
        {
            return currentIndex - 1;
        }
    }
}
