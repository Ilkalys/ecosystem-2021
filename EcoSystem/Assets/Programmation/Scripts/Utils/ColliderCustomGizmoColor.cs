using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderCustomGizmoColor : MonoBehaviour
{
    private void Update()
    {
        //just here to make the editor display the enabled toggle
    }

    public Color customGizmoColor;
    public bool gizmoStayVisible = false;
    private void OnDrawGizmosSelected()
    {
        if (!gizmoStayVisible)
            DrawCustomColorGizmo();
    }

    private void OnDrawGizmos()
    {
        if (gizmoStayVisible)
            DrawCustomColorGizmo();
    }

    void DrawCustomColorGizmo()
    {
        if (!enabled)
            return;

        Color oldColor = Gizmos.color;
        Matrix4x4 oldMatrix = Gizmos.matrix; // Perhaps saving and restoring these isn't necessary?
        Gizmos.color = customGizmoColor;
        Gizmos.matrix = transform.localToWorldMatrix;
        foreach (Collider collider in GetComponents<Collider>())
        {
            if (!collider.enabled)
                continue;

            if (collider.GetType() == typeof(SphereCollider))
            {
                SphereCollider sphereCol = collider as SphereCollider;
                Gizmos.DrawWireSphere(sphereCol.center, sphereCol.radius);
                Gizmos.DrawWireSphere(sphereCol.center, sphereCol.radius+0.0001f);
            }
            else if (collider.GetType() == typeof(BoxCollider))
            {
                BoxCollider boxCol = collider as BoxCollider;
                Gizmos.DrawWireCube(boxCol.center, boxCol.size);
            }
            else if (collider.GetType() == typeof(MeshCollider))
            {
                MeshCollider meshCol = collider as MeshCollider;
                Gizmos.DrawWireMesh(meshCol.sharedMesh);
            }
        }
        Gizmos.color = oldColor;
        Gizmos.matrix = oldMatrix;
    }
}
