using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

/// <summary>
/// A scriptable object to hold the logic information of how to treat the different kind of Unique Object the game will have.
/// The logic itself is in the UniqueObject Class.
/// </summary>
[CreateAssetMenu(menuName = "Scene Management/Unique object label list")]
public class UniqueObjectsLabelList : ScriptableObject
{
    public enum DontDestroyOnLoadComportement
    {
        DontDestroyOnLoad,
        SwitchToActiveScene,
        SwitchToNamedScene
    }

    public List<UniqueObjectLogic> uniqueLogic = new List<UniqueObjectLogic>()
    {
        new UniqueObjectLogic("Camera"),
        new UniqueObjectLogic("Player"),
        new UniqueObjectLogic("ShadowedDirectionnalLight")
    };

    public DontDestroyOnLoadComportement dontDestroyOnLoadComportement = DontDestroyOnLoadComportement.DontDestroyOnLoad;
    public string sceneName = ""; //TODO hide if dontDestroyOnLoadComportement != switchToNamedScene
    public bool followActiveScene = true; //TODO hide if dontDestroyOnLoadComportement != switchToActiveScene
}

[Serializable]
public class UniqueObjectLogic
{
    public enum SingletonLogic
    {
        keepFirstDestroyNewer, 
        destroyFirstKeepNewer,
        keepFirstDeactivateNewer,
        deactivateFirstKeepNewer 
    }

    public enum Privilege //the idea of this parameter is to override the singleton logic if one of the compared object is privileged
    {
        NoPrivilege,
        PrivilegeActiveSceneObject,
        privilegeMarkedObject,
        //privilegeHigherPriorityObject //Need to add a parametrable priority value to the UniqueObject Class
    }

    public enum DontDestroyOnLoad
    {
        True,
        False,
        ObjectSpecific
    }

    public string label;
    public SingletonLogic singletonLogic  = SingletonLogic.keepFirstDestroyNewer;
    //public Privilege privilegeLogic = Privilege.NoPrivilege; // TODO implement logic in UniqueObject
    public DontDestroyOnLoad dontDestroyOnLoad = DontDestroyOnLoad.True;


    public UniqueObjectLogic(string labelName)
    {
        this.label = labelName;
    }
}


#if UNITY_EDITOR

[CustomEditor(typeof(UniqueObjectsLabelList))]
public class UniqueObjectLabelListEditor : Editor
{
    SerializedProperty uniqueLogicProperty;
    SerializedProperty dontDestroyOnLoadComportementProperty;
    SerializedProperty sceneNameProperty;
    SerializedProperty followActiveSceneProperty;

    private void OnEnable()
    {
        uniqueLogicProperty = serializedObject.FindProperty("uniqueLogic");
        dontDestroyOnLoadComportementProperty = serializedObject.FindProperty("dontDestroyOnLoadComportement");
        sceneNameProperty = serializedObject.FindProperty("sceneName");
        followActiveSceneProperty = serializedObject.FindProperty("followActiveScene");
    }

    public override void OnInspectorGUI()
    {
        if (Application.isPlaying)
            EditorGUILayout.HelpBox("To change these values please exit Play mode.", MessageType.Info);

        EditorGUI.BeginDisabledGroup(Application.isPlaying);
        serializedObject.Update();

        EditorGUILayout.PropertyField(uniqueLogicProperty);
        EditorGUILayout.PropertyField(dontDestroyOnLoadComportementProperty);

        if (dontDestroyOnLoadComportementProperty.enumValueIndex == ((int)UniqueObjectsLabelList.DontDestroyOnLoadComportement.SwitchToNamedScene))
            EditorGUILayout.PropertyField(sceneNameProperty);

        if (dontDestroyOnLoadComportementProperty.enumValueIndex == ((int)UniqueObjectsLabelList.DontDestroyOnLoadComportement.SwitchToActiveScene))
            EditorGUILayout.PropertyField(followActiveSceneProperty);

        serializedObject.ApplyModifiedProperties();
        EditorGUI.EndDisabledGroup();
    }
}

[CustomPropertyDrawer(typeof(UniqueObjectLogic))]
public class UniqueObjectLogicProperty : PropertyDrawer
{
    //TODO hide privilegeLogic parameter when destroying SingletonLogic is chosen
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label.text += " (ID = " + new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()) + ")";
        EditorGUI.BeginProperty(position, label, property);

        EditorGUI.PropertyField(position, property, label, true);

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }
}


#endif