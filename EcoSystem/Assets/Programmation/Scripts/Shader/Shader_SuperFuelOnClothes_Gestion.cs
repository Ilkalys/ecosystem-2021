using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Shader_SuperFuelOnClothes_Gestion: MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public SS_Character_SoundGestion    soundGestion;

    public  LayerMask                   m_fluidMask;
    public  float                       m_raycastLength;

    public  float                       m_maxLocalYPos;
    public  GameObject                  Character;
    public  MeshRenderer                levelOfFluidOnBody;
    private float                       m_yInitPos;
    private float                       m_maxDist;
    private float                       m_yCurrentPos;

    //---------------VFX------------------------------------\\
    public  VisualEffect                m_vfxDrop;
    private Vector3                     initLocalPosVFX;
    public  float                       dropduration;
    private float                       counterDrop;

    //---------------VFX-FOOT------------------------------------\\
    public VisualEffect                 m_vfxFootL,  m_vfxFootR;
    public Transform                    FootL, FootR;
    public LayerMask                    m_groundMask;
    public float                        raycastFootLength;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Start()
    {
        initLocalPosVFX = m_vfxDrop.transform.localPosition;
        m_yInitPos = this.transform.localPosition.y;
        m_maxDist = m_maxLocalYPos - m_yInitPos;
        m_yCurrentPos = m_yInitPos;
    }

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        RaycastHit hit;
        if ( Character )
        {
            if ( Physics.Raycast( Character.transform.position + Vector3.up * m_maxLocalYPos * Character.transform.localScale.y, Vector3.down, out hit, m_raycastLength, m_fluidMask ) )
            {
                this.transform.position = new Vector3( this.transform.position.x, hit.point.y, this.transform.position.z );
                if ( m_vfxDrop )
                {
                    counterDrop = dropduration;
                    m_vfxDrop.transform.position = new Vector3( m_vfxDrop.transform.position.x, hit.point.y, m_vfxDrop.transform.position.z );
                    m_vfxDrop.SetFloat( "SpawnRate", counterDrop );
                }
                if(soundGestion)
                    soundGestion.onWater = true;
            }
            else
            {
                if ( soundGestion )
                    soundGestion.onWater = false;

                this.transform.localPosition = new Vector3( this.transform.localPosition.x, m_yInitPos, this.transform.localPosition.z );
            }
            if ( this.transform.localPosition.y != m_yCurrentPos )
            {
                m_yCurrentPos = this.transform.localPosition.y;
                float currentDist = m_yCurrentPos - m_yInitPos;
                float pourcent = (currentDist) * 100 / m_maxDist;
                if ( levelOfFluidOnBody )
                    levelOfFluidOnBody.material.SetFloat( "_CurrentLevel", pourcent );
            }
        }
        if ( m_vfxDrop && m_vfxFootR && m_vfxFootL )
        {
            if ( counterDrop > 0 )
            {
                counterDrop -= Time.deltaTime;
                if ( Physics.Raycast( m_vfxDrop.transform.position, Vector3.down, out hit, 1.5f, m_groundMask ) )
                {
                    m_vfxDrop.SetVector3( "GroundPos", hit.point );
                }
                if ( Physics.Raycast( FootR.position, Vector3.down, out hit, raycastFootLength, m_groundMask ) )
                {
                    m_vfxFootR.SetVector3( "GroundPos", hit.point );
                }
                else
                {
                    m_vfxFootR.SetVector3( "GroundPos", Vector3.zero );
                }

                if ( Physics.Raycast( FootL.position, Vector3.down, out hit, raycastFootLength, m_groundMask ) )
                {
                    m_vfxFootL.SetVector3( "GroundPos", hit.point );
                }
                else
                {
                    m_vfxFootL.SetVector3( "GroundPos", Vector3.zero );
                }
            }
            else
            {
                m_vfxDrop.transform.localPosition = initLocalPosVFX;
                m_vfxDrop.SetFloat( "SpawnRate", 0 );
                m_vfxFootL.SetVector3( "GroundPos", Vector3.zero );
                m_vfxFootR.SetVector3( "GroundPos", Vector3.zero );
            }
        }
    }

    //--------------------------------------------------------------------------------------\\
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawLine( Character.transform.position + Vector3.up * m_maxLocalYPos * Character.transform.localScale.y, Character.transform.position + Vector3.up * m_maxLocalYPos * Character.transform.localScale.y + Vector3.down * m_raycastLength );
    }
}
