using CustomsAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class SS_Character_Controller : MonoBehaviour
{

    #region "ATTRIBUTES"
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\
    public static SS_Character_Controller instance;
    public CC_State currentState;

    public CC_BrainState m_brainState;
    public SkinnedMeshRenderer m_christineMesh, m_armatureMesh, m_helmetMesh;

    [HideInInspector]
    public  float           m_maxInitSpeed          = 5f;

    [Header("               GroundCheck Parameters ")]
    [Space]
    [Tooltip("Layers that the player have to touch to by considered 'On the Ground'")]
    public LayerMask        m_groundLayer;
    [Tooltip("Length of the raycast for checking the ground")]
    public  float           m_maxCheckGroundDist;
    public  Vector3         m_groundCheckBoxOffest;
    public  GameObject      m_DebugGroundcheckBox;
    [Header("               Trigger Detection Parameters ")]

    public Collider         m_character_EdgeDetection;
    public Collider         m_character_ClimbDetection;

    [HideInInspector]
    public  InputMaster     m_playerActions;

    [Space , Space]
    public UnityEvent m_BeginDeath;
    public UnityEvent m_EndDeath;
    public float durationBeforeEndDeath;

    public UnityEvent m_OnCough;
    public UnityEvent m_OnEndCough;

    [Space]
    public bool showDebug = false;

    [Space]
    [ConditionnalDisplay("showDebug")]
    public string m_groundTag;
    [Space]
    [ConditionnalDisplay("showDebug")]
    public float            m_currentVerticalMovementInput;
    [ConditionnalDisplay("showDebug")]
    public  float           m_currentMovementInput;

    [Space]
    [ConditionnalDisplay("showDebug")]
    public Vector3          m_normalGround;

    [Space]
    [ConditionnalDisplay("showDebug")]
    public float           m_currentSpeed          = 0f;
    [ConditionnalDisplay("showDebug")]
    public float           m_previousSpeed         = 0f;
    [ConditionnalDisplay("showDebug")]
    public float           m_currentMaxSpeed;
    [ConditionnalDisplay("showDebug")]
    public Vector2         m_current2DMovement;
    [ConditionnalDisplay("showDebug")]
    public Vector3         m_currentMovement;

    [Space]
    [ConditionnalDisplay("showDebug")]
    public bool             m_isMovementPressed;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isJumpPressed;
    [ConditionnalDisplay("showDebug")]
    public float            m_timeJumpStillPressed;
    [ConditionnalDisplay("showDebug")]
    public bool            m_alreadyPressJump = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isRunPressed;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isInteractLeftPressed;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isInteractRightPressed;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isInteractPressed;
    [ConditionnalDisplay("showDebug")]
    public float            m_timeInteractStillPressed;

    [Space]
    [ConditionnalDisplay("showDebug")]
    public GameObject       m_wallClimbable_InFront ;
    [ConditionnalDisplay("showDebug")]
    public GameObject       m_Object_InFront ;
    [ConditionnalDisplay("showDebug")]
    public GameObject       m_edge_InFront;
    [ConditionnalDisplay("showDebug")]
    public Vector3          m_animationPos;
    [Space]
    [ConditionnalDisplay("showDebug")]
    public GameObject      m_ObjectPushable;
  
    [Space]
    [ConditionnalDisplay("showDebug")]
    public bool             m_isGrounded;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isDead = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isWaiting = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isFalling;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isHardLanding = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isPushing = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isOnWall = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isOnEdge = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isJumping = false;
    [ConditionnalDisplay("showDebug")]
    public bool             m_isLookingRight = true;

    [ConditionnalDisplay("showDebug")]
    public int m_nbCough = 0;
    [ConditionnalDisplay("showDebug")]
    public float m_coughIntensity = 1;

    [Space]
    [ConditionnalDisplay("showDebug")]
    public Rigidbody        m_rigidBody;

    [ConditionnalDisplay("showDebug")]
    public Collider         m_collider;

    #endregion
    #region "METHODS"
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    private void OnEnable()
    {
        if ( m_playerActions != null )
            m_playerActions.Enable();
    }

    //--------------------------------------------------------------------------------------\\
    private void OnDisable()
    {
        if ( m_playerActions != null )
            m_playerActions.Disable();
    }

    //--------------------------------------------------------------------------------------\\
    private void Awake()
    {
        if ( !instance )
        {
            instance = this;
        }
        else
        {
            GameObject.Destroy( this.gameObject );
        }

        currentState.OnEnterState( );
        m_rigidBody = this.GetComponent<Rigidbody>();
        m_collider = this.GetComponent<Collider>();
        SwitchBrain();
        m_playerActions = new InputMaster();

        m_playerActions.Player.Jump.started += context => onJumpInput( context );
        m_playerActions.Player.Jump.canceled += context => onJumpInput( context );

        m_playerActions.Player.Movement.performed += context => onMovementInput( context );
        m_playerActions.Player.Movement.canceled += context => onMovementInput( context );

        m_playerActions.Player.VerticalMovement.performed += context => onVerticalMovementInput( context );
        m_playerActions.Player.VerticalMovement.canceled += context => onVerticalMovementInput( context );

        m_playerActions.Player.Run.started += context => onRunInput( context );
        m_playerActions.Player.Run.canceled += context => onRunInput( context );

        m_playerActions.Player.InteractLeft.started += context => onInteractInput( context, true );
        m_playerActions.Player.InteractLeft.canceled += context => onInteractInput( context, true );

        m_playerActions.Player.InteractRight.started += context => onInteractInput( context, false );
        m_playerActions.Player.InteractRight.canceled += context => onInteractInput( context, false );

        AkSoundEngine.SetRTPCValue( "Helmet",  ( m_helmetMesh.gameObject.activeInHierarchy ? 0f : 1f ));

        m_nbCough = 0;
        m_coughIntensity = 0;
    }

    //--------------------------------------------------------------------------------------\\
    void onMovementInput( InputAction.CallbackContext context )
    {
        m_currentMovementInput = context.ReadValue<float>();
        m_current2DMovement.x = Mathf.Sign( m_currentMovementInput );
        m_isMovementPressed = m_currentMovementInput != 0;
    }

    //--------------------------------------------------------------------------------------\\
    void onVerticalMovementInput( InputAction.CallbackContext context )
    {
        m_currentVerticalMovementInput = context.ReadValue<float>();
    }

    //--------------------------------------------------------------------------------------\\
    void onJumpInput( InputAction.CallbackContext context )
    {
        if ( context.ReadValueAsButton() )
            m_isJumpPressed = true;
        else
        {
           StartCoroutine( "SetJumpPressedFalse");
        }

    }

    //--------------------------------------------------------------------------------------\\
    void onRunInput( InputAction.CallbackContext context )
    {
        m_isRunPressed = context.ReadValueAsButton();
    }

    //--------------------------------------------------------------------------------------\\
    void onInteractInput( InputAction.CallbackContext context, bool isLeft )
    {
        if ( context.ReadValueAsButton() )
        {
            if ( isLeft )
            {
                m_isInteractLeftPressed = true;
            }
            else
            {
                m_isInteractRightPressed = true;
            }
            m_isInteractPressed = m_isInteractLeftPressed && m_isInteractRightPressed;
        }
        else
            StartCoroutine( "SetInteractPressedFalse", isLeft );
    }


    //--------------------------------------------------------------------------------------\\
    void FixedUpdate()
    {
        SSCC_HandleGroundCheck();

        if ( !m_isDead )
        {
            m_isFalling = m_current2DMovement.y <= 0.0f || !m_isJumpPressed;

            if ( !m_isJumping && ( m_isGrounded || m_isOnWall ) && m_isJumpPressed && m_brainState.brain_CC_State_Jumping.canUse && !m_alreadyPressJump)
            {
                m_alreadyPressJump = true;
                m_isJumping = true;
            }
            else if ( m_isJumping && ( ( m_isGrounded && m_isFalling ) || m_isOnWall || m_isOnEdge ) )
            {
                m_isJumping = false;
            }

            currentState.UpdateState();

            if ( !m_isOnWall && !m_isPushing )
                HandleRotation();
        }
        else
        {
            if ( m_isGrounded )
            {
                m_rigidBody.velocity *= 0.9f;
            }
        }

        AkSoundEngine.SetRTPCValue( "Speed", m_isWaiting? 5 : Mathf.Abs( SS_Character_Controller.instance.m_currentSpeed ) );
    }

    //--------------------------------------------------------------------------------------\\
    public void ChangeStateWithUpdate( CC_State newState )
    {
        currentState.OnExitState( );
        currentState = newState;
        print( currentState );
        currentState.OnEnterState( );
        currentState.UpdateState( );
    }

    //--------------------------------------------------------------------------------------\\
    public void ChangeState( CC_State newState )
    {
        currentState.OnExitState( );
        currentState = newState;
        print( currentState );
        currentState.OnEnterState( );
    }

    //--------------------------------------------------------------------------------------\\
    private void SSCC_HandleGroundCheck()
    {
        RaycastHit hitInfo;
        m_isGrounded = Physics.BoxCast( this.gameObject.transform.position + Vector3.up * 0.5f, m_groundCheckBoxOffest, Vector3.down, out hitInfo, this.gameObject.transform.rotation, m_maxCheckGroundDist, m_groundLayer );
        m_normalGround = m_isGrounded ? hitInfo.normal : Vector3.one;
        m_groundTag = m_isGrounded ? hitInfo.collider.gameObject.tag : null;
        if ( m_DebugGroundcheckBox )
        {
            m_DebugGroundcheckBox.transform.localScale = m_groundCheckBoxOffest;
            m_DebugGroundcheckBox.transform.rotation = this.transform.rotation;
            m_DebugGroundcheckBox.transform.position = this.gameObject.transform.position + Vector3.down * m_maxCheckGroundDist;
        }
    }

    //--------------------------------------------------------------------------------------\\
    IEnumerator DesactivateState( CC_State_Deactivatable deactivable )
    {
        deactivable.canUse = false;
        yield return new WaitForSeconds( deactivable.desactivateDuration );
        deactivable.canUse = true;
    }


    //--------------------------------------------------------------------------------------\\
    IEnumerator SetJumpPressedFalse( )
    {
        yield return new WaitForSeconds( m_timeJumpStillPressed );
        m_isJumpPressed = false;
        m_alreadyPressJump = false;
    }

    //--------------------------------------------------------------------------------------\\
    IEnumerator SetInteractPressedFalse(bool isLeft)
    {
        yield return new WaitForSeconds( m_timeInteractStillPressed );
        if ( isLeft )
        {
            m_isInteractLeftPressed = false;
        }
        else
        {
            m_isInteractRightPressed = false;
        }
        m_isInteractPressed = false;
    }
    //--------------------------------------------------------------------------------------\\
    public void HandleRotation()
    {
        if ( m_currentSpeed != 0 && ( Mathf.Sign( m_currentSpeed ) == 1 && !m_isLookingRight ) || ( Mathf.Sign( m_currentSpeed ) == -1 && m_isLookingRight ) )
        {
            m_isLookingRight = Mathf.Sign( m_currentSpeed ) == 1 ? true : false;
            transform.rotation = Quaternion.Euler( transform.rotation.eulerAngles + Vector3.up * 180 );
        }
    }

    //--------------------------------------------------------------------------------------\\
    public bool SameDirection(GameObject other)
    {
        return other!= null && Vector3.Dot( this.transform.forward, other.transform.forward ) > 0;
    }

    //--------------------------------------------------------------------------------------\\
    public bool LookAt( GameObject other )
    {
        Vector3 dirBetweenObject = ( Vector3.right * ( other.transform.position.x - this.transform.position.x ) + Vector3.forward* ( other.transform.position.z - this.transform.position.z )).normalized;
        return other != null && Vector3.Dot( this.transform.forward, dirBetweenObject) > 0;
    }

    public void OnDeath()
    {
        m_currentSpeed = 0;
        m_isDead = true;
        m_BeginDeath.Invoke();
        StartCoroutine( "TimeBeforeDead", durationBeforeEndDeath );
    }

    public void Cough(int nbCough, float coughIntensity, Vector3 targetPos)
    {
        m_nbCough = nbCough;
        m_coughIntensity = coughIntensity;
        m_animationPos = targetPos;
        m_OnCough.Invoke();
        ChangeState( m_brainState.brain_CC_State_Waiting );
    }

    public void SwitchBrain()
    {
        m_christineMesh.material = this.m_brainState.m_christine_Materiel;
        m_armatureMesh.material = this.m_brainState.m_armature_Materiel;
        m_helmetMesh.material = this.m_brainState.m_helmet_Materiel;
        ResetState();
    }

    private void ResetState()
    {
        if( this.currentState.GetType() == typeof( CC_State_Walking) )
        {
            ChangeState( this.m_brainState.brain_CC_State_Walking);
        }
        else if ( this.currentState.GetType() == typeof( CC_State_Jumping ) )
        {
            ChangeState( this.m_brainState.brain_CC_State_Jumping);
        }
    }

    IEnumerator TimeBeforeDead(float duration)
    {
        yield return new WaitForSeconds( duration );
        m_EndDeath.Invoke();
    }
    #endregion
}
