using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[ExecuteAlways]
public class SSElementOnTrack : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                        INTERNAL STRUCT                                                   \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Serializable]
    public class PathParams
    {
        [Tooltip("The path to follow")]
        public Cinemachine.SSTrack                  path;

        [Tooltip("How to interpret the Path Position.  If set to Path Units, values are as follows: 0 represents the first waypoint on the path, 1 is the second, and so on.  Values in-between are points on the path in between the waypoints.  If set to Distance, then Path Position represents distance along the path.")]
        public Cinemachine.SSTrack.PositionUnits    positionUnits               = Cinemachine.SSTrack.PositionUnits.Distance;

        [Tooltip("The position along the path at which the character will be placed.  This can be animated directly or, if the velocity is non-zero, will be updated automatically.  The value is interpreted according to the Position Units setting.")]
        public float                                positionOnThePath;

        [Tooltip("Wich Axis of the Track the position of the Character will follow (0 = not followed)")]
        public Vector3                              followedPositionTrackAxis   = new Vector3(1, 0, 1);

        [Tooltip("Wich Axis of the Track the rotation of the Character will follow (0 = not followed)")]
        public Vector3                              followedRotationTrackAxis   = new Vector3(0, 1, 0);
    }
   
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //All the information about the Path of the Character.
    public PathParams m_pathParams;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    protected virtual void Update()
    {
        SetEventPosition(m_pathParams.positionOnThePath);
    }

    //--------------------------------------------------------------------------------------\\
    public void SetEventPosition(float distanceAlongPath)
    {
        if (m_pathParams.path != null)
        {
            m_pathParams.positionOnThePath = m_pathParams.path.StandardizeUnit(distanceAlongPath, m_pathParams.positionUnits);

            Vector3 positionOnPath = m_pathParams.path.EvaluatePositionAtUnit(m_pathParams.positionOnThePath, m_pathParams.positionUnits);
            positionOnPath.x = m_pathParams.followedPositionTrackAxis.x == 0 ? transform.position.x : positionOnPath.x;
            positionOnPath.y = m_pathParams.followedPositionTrackAxis.y == 0 ? transform.position.y : positionOnPath.y;
            positionOnPath.z = m_pathParams.followedPositionTrackAxis.z == 0 ? transform.position.z : positionOnPath.z;

            transform.position = positionOnPath;

            Vector3 orientationOnPath = m_pathParams.path.EvaluateOrientationAtUnit(m_pathParams.positionOnThePath, m_pathParams.positionUnits).eulerAngles;
            orientationOnPath.x = m_pathParams.followedRotationTrackAxis.x == 0 ? transform.rotation.eulerAngles.x : orientationOnPath.x;
            orientationOnPath.y = m_pathParams.followedRotationTrackAxis.y == 0 ? transform.rotation.eulerAngles.y : orientationOnPath.y;
            orientationOnPath.z = m_pathParams.followedRotationTrackAxis.z == 0 ? transform.rotation.eulerAngles.z : orientationOnPath.z;

            transform.rotation = Quaternion.Euler(orientationOnPath);
        }
    }
}
