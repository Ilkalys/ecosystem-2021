using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( menuName = "CharacterController/BrainState" )]
public class CC_BrainState : ScriptableObject
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public Material                     m_christine_Materiel;
    public Material                     m_armature_Materiel;
    public Material                     m_helmet_Materiel;

    public int                          m_currentMutationLevel;

    [Header("   States  ")]
    public CC_State_ClimbingWall        brain_CC_State_ClimbingWall;
    public CC_State_JumpingFromWall     brain_CC_State_JumpingFromWall;
    public CC_State_Jumping             brain_CC_State_Jumping;
    public CC_State_OnEdge              brain_CC_State_OnEdge;
    public CC_State_Pushing             brain_CC_State_Pushing;
    public CC_State_Waiting             brain_CC_State_Waiting;
    public CC_State_Walking             brain_CC_State_Walking;
}
