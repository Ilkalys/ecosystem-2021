using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

//TODO ADD
// - grid setup parameters : (UnityUnit or panelSize)
// - optional panel size modifier (animation curve (in to out of focus and out of to in focus) + copyrevert to the other)
// - auto search panel + reposition in switcher grid
// - auto search selectables in each panel
// - select panel in scene hierachy and/or gizmo in scene view
// - cut old refs to speed anim Curve
// - clean up old code

/// <summary>
/// A rework of the UI panel switcher from 51 BAFFES
/// </summary>
public class UIPanelSwitcher : MonoBehaviour
{
    public RectTransform panelHolder; // the gameobject holding all the anel, this is moved to change the panels displayed on the camera
    public Vector2 panelSize = new Vector2(1920, 1080); // the size of each panel, generally this is the UI canvas base size
    public bool workWithSpeedCurve = false; //choose between normal animation curve and speed animation curve

    //normal animation curve
    public AnimationCurve animationCurve = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0)); // the animation to play to switch panels

    //speed animation curve
    public AnimationCurve animationSpeedCurve = new AnimationCurve(new Keyframe(0, 0, 0, 0), new Keyframe(0.5f, 2f, 0, 0), new Keyframe(1, 0, 0, 0)); // the animation to play to switch panels
    public float postAnimationAdjustTime = 0; // a time dedicated to adjust the position with a linear mouvement if the animation isn't perfectly done, 0 is instantaneous (no post animation)

    //both animation Curve
    private float animationTime; //the time the animation will take (calculated with the last keyAnim)

    public int currentPanel; // the pannel currently on screen or the on targeted by the end of the animation
    private int lastPanel = 0; // the last panel which was previously the current one
    public List<PanelInfo> panels = new List<PanelInfo>();

    //bool moving = false;
    bool postAnimation = false;
    private Vector2 baseSpeedToNextPanel = Vector2.zero;
    private Vector2 moveDistance = Vector2.zero;
    float timeElapsed = 0f;

    private bool initialized = false;

    public PanelSwitcherEvent OnPanelSwitch;


    public void Start()
    {
        //check panelHolder
        animationTime = animationSpeedCurve.keys[animationSpeedCurve.keys.Length - 1].time;
        //Debug.Log(animationTime);

        panelHolder.anchoredPosition = getPanelPosOnCamera(panels[currentPanel]);

        foreach (PanelInfo panel in panels)
        {
            panel.setSelectableInteraction(false);
        }
        panels[currentPanel].setSelectableInteraction(true);
        panels[currentPanel].selectSelectable();

        initialized = true;
    }
    /*
    public void FixedUpdate()
    {
        if (moving)
        {
            //evaluate and apply mouvement based on speed and modified by the anim curve
            //Debug.Log(deplacement +" = " + baseSpeedToNextPanel + " * " + changeAnimationCurve.Evaluate(timeElapsed));

            if (timeElapsed < animationTime) //Main animation
            {
                if (!workWithSpeedCurve)
                {
                    panelHolder.anchoredPosition = Vector2.Lerp(getPanelPosOnCamera(panels[currentPanel]) - moveDistance, getPanelPosOnCamera(panels[currentPanel]), animationCurve.Evaluate(timeElapsed));
                }
                else
                {
                    Vector2 deplacement = baseSpeedToNextPanel * animationSpeedCurve.Evaluate(timeElapsed);
                    panelHolder.anchoredPosition += deplacement;
                }

                timeElapsed += Time.deltaTime;
            }
            else if (workWithSpeedCurve && timeElapsed < animationTime + postAnimationAdjustTime) //post animation linear adjust if using speedCurve
            {
                if (!postAnimation)
                {
                    searchBaseSpeed(postAnimationAdjustTime);
                    postAnimation = true;
                }
                panelHolder.anchoredPosition += baseSpeedToNextPanel;
                timeElapsed += Time.deltaTime;
            }
            else //snap in perfect place and end animation
            {
                panelHolder.anchoredPosition = getPanelPosOnCamera(panels[currentPanel]);
                moveDistance = Vector2.zero;
                moving = false;
                postAnimation = false;
                panels[currentPanel].selectSelectable();
                //Debug.Log("good!");
            }
        }
    }
    */

    public void OnEnable()
    {
        if (!initialized)
        {
            return;
        }

        panels[currentPanel].OnPanelDisplayed.Invoke(-1, currentPanel);
        panels[currentPanel].setSelectableInteraction(true);
    }

    public void OnDisable()
    {
        panels[currentPanel].OnPanelHidden.Invoke(currentPanel, -1);
        panels[currentPanel].setSelectableInteraction(false);
    }

    public void SwitchToPanel(Vector2 positionOnUI)
    {
        //if (NotAllPanelHaveDifferentUIPos) //TODO this param
        //    Debug.LogWarning("The SwitchToPanel(Vector2 positionOnUI) was called but some UIpanels share " +
        //        "the same position : the panel selected might not be the one you were waiting for. Please " +
        //        "use SwitchToPanel(int panelNumber) instead");

        int i = panels.FindIndex(p => p.positionOnUI == positionOnUI);
        if (i == -1)
            Debug.LogError("Couldn't find panel at position : " + positionOnUI);
        else
            SwitchToPanel(i);
    }
    public void SwitchToPanel(int panelNumber)
    {
        lastPanel = currentPanel;
        currentPanel = panelNumber;

        panels[lastPanel].setSelectableInteraction(false);
        panels[currentPanel].setSelectableInteraction(true);

        panels[lastPanel].OnPanelHidden.Invoke(lastPanel, currentPanel);
        panels[currentPanel].OnPanelDisplayed.Invoke(lastPanel, currentPanel);
        OnPanelSwitch.Invoke(lastPanel, currentPanel);

        if (workWithSpeedCurve)
        {
            searchBaseSpeed(animationTime);
        }
        else
        {
            searchMoveDistance();
        }

        //Debug.Log(baseSpeedToNextPanel + " = " + distanceLeft + " * " + Time.fixedDeltaTime);
        timeElapsed = 0f;

        //moving = true;
        StartCoroutine(MovePanelAnimation());
    }

    IEnumerator MovePanelAnimation()
    {
        while (timeElapsed < animationTime) //Main animation
        {
            panelHolder.anchoredPosition = Vector2.Lerp(getPanelPosOnCamera(panels[currentPanel]) - moveDistance, getPanelPosOnCamera(panels[currentPanel]), animationCurve.Evaluate(timeElapsed));
            timeElapsed += Time.unscaledDeltaTime;
            yield return new WaitForEndOfFrame();
        }

        panelHolder.anchoredPosition = getPanelPosOnCamera(panels[currentPanel]);
        moveDistance = Vector2.zero;
        postAnimation = false;
        panels[currentPanel].selectSelectable();
        //Debug.Log("good!");
    }

    void searchBaseSpeed(float time)
    {
        //calculate speed with time and distance
        searchMoveDistance();
        baseSpeedToNextPanel = moveDistance * Time.fixedDeltaTime / time;
    }

    void searchMoveDistance()
    {
        moveDistance = getPanelPosOnCamera(panels[currentPanel]) - panelHolder.anchoredPosition;
    }

    public Vector2 getPanelPosOnCamera(PanelInfo panel)
    {
        //TODO : check if this work with diverse anchor position (need to add an offset?)
        return panelSize * panel.positionOnUI * -1;
    }
}


[System.Serializable]
public class PanelInfo
{
    public enum SelectableActivation
    {
        All,
        SaveAndRestore,
        None
    }

    public Vector2Int positionOnUI = new Vector2Int(-1, 0);

    public List<Selectable> selectables = new List<Selectable>();
    public SelectableActivation activationRules = SelectableActivation.SaveAndRestore;
    [HideInInspector] // Ideally only if activationRules != saveAndRestore
    public List<bool> selectableSavedState;

    public PanelSwitcherEvent OnPanelDisplayed;
    public PanelSwitcherEvent OnPanelHidden;

    public void setSelectableInteraction(bool activate)
    {
        switch (activationRules)
        {
            case SelectableActivation.All:
                foreach (Selectable selectable in selectables)
                {
                    selectable.interactable = activate;
                }
                break;

            case SelectableActivation.SaveAndRestore:
                if (activate)
                {
                    if (selectableSavedState != null && selectableSavedState.Count == selectables.Count)
                    {
                        for (int i = 0; i < selectables.Count; i++)
                        {
                            selectables[i].interactable = selectableSavedState[i];
                        } 
                    }
                    else
                    {
                        if (Application.isPlaying)
                        {
                            Debug.LogError("selectableSavedState isn't corresponding to selectables list : " +
                                "if you are using activationRules = SelectableActivation.SaveAndRestore, " +
                                "please make sure you're populating selectableSavedState accordingly to selectable list");
                        }
                        else
                        {
                            setSelectableSavedState();
                            Debug.LogWarning("selectableSavedState wasn't corresponding to selectables list : it has been " +
                                "set to the current value");
                        }
                    }
                }
                else
                {
                    setSelectableSavedState(); 
                    for (int i = 0; i < selectables.Count; i++)
                    {
                        selectables[i].interactable = false;
                    }
                }
                break;

            case SelectableActivation.None:
                break;

            default:
                Debug.LogError("unkown or unimplemented value for activationRules!");
                break;        
        }
    }

    public void selectSelectable()
    {
        Selectable selectionable = selectables.Find((Selectable selectable) => (selectable.IsInteractable() && selectable.gameObject.activeInHierarchy));

        if (selectionable != null)
        {
            selectionable.Select();
        }
    }

    public void setSelectableSavedState()
    {
        if (selectableSavedState == null)
            selectableSavedState = new List<bool>();
        else
            selectableSavedState.Clear();

        foreach (Selectable selectable in selectables)
        {
            selectableSavedState.Add(selectable.interactable);
        }
    }
}


[System.Serializable]
public class PanelSwitcherEvent : UnityEvent<int, int> { } // T0 => from, T1 => to


#if UNITY_EDITOR
//Todo to automate entering infos...
[CustomEditor(typeof(UIPanelSwitcher))]
public class UIPanelSwitcherEditor : Editor
{
    SerializedProperty panelHolderProp;
    SerializedProperty panelSizeProp; // the size of each panel, generally this is the UI canvas base size
    SerializedProperty workWithSpeedCurveProp;

    SerializedProperty animationCurveProp; // the animation to play to switch panels

    SerializedProperty animationSpeedCurveProp; // the animation to play to switch panels
    float animationDistancePercentage;
    SerializedProperty postAnimationAdjustTimeProp; // a time dedicated to adjust the position with a linear mouvement if the animation isn't perfectly done, 0 is instantaneous (no post animation)

    SerializedProperty currentPanelProp; // the pannel currently on screen or the on targeted by the end of the animation

    SerializedProperty panelSwitchEventProp;

    SerializedProperty panelsProp;

    private void OnEnable()
    {
        panelHolderProp = serializedObject.FindProperty("panelHolder");
        panelSizeProp = serializedObject.FindProperty("panelSize");
        workWithSpeedCurveProp = serializedObject.FindProperty("workWithSpeedCurve");

        animationCurveProp = serializedObject.FindProperty("animationCurve");

        animationSpeedCurveProp = serializedObject.FindProperty("animationSpeedCurve");
        postAnimationAdjustTimeProp = serializedObject.FindProperty("postAnimationAdjustTime");

        currentPanelProp = serializedObject.FindProperty("currentPanel");
        panelSwitchEventProp = serializedObject.FindProperty("OnPanelSwitch");
        panelsProp = serializedObject.FindProperty("panels");
    }

    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();

        serializedObject.Update();

        EditorGUILayout.PropertyField(panelHolderProp);
        EditorGUILayout.PropertyField(panelSizeProp);
        //EditorGUILayout.PropertyField(workWithSpeedCurveProp);

        if (!workWithSpeedCurveProp.boolValue)
        {
            EditorGUILayout.PropertyField(animationCurveProp);
            if (animationCurveProp.animationCurveValue.keys[0].time != 0f)
            {
                //TODO : scale animation
            }
        }
        else
        {
            EditorGUILayout.PropertyField(animationSpeedCurveProp);
            serializedObject.ApplyModifiedProperties(); // it seems that this fix the wrong update of the animation curve...
            if (animationCurveProp.animationCurveValue.keys[0].time != 0f)
            {
                //TODO : scale animation
            }
            int nbKeys = animationSpeedCurveProp.animationCurveValue.keys.Length;
            animationDistancePercentage = IntegrateCurve(animationSpeedCurveProp.animationCurveValue, nbKeys * 3) / animationSpeedCurveProp.animationCurveValue.keys[nbKeys - 1].time * 100;
            EditorGUILayout.LabelField("Percentage of displacement : ", animationDistancePercentage.ToString());
            if (animationDistancePercentage < 95)
            {
                EditorGUILayout.HelpBox("Your animation is too short or too slow for the displacement, " +
                    "you can change the curve or add a post animation linear displacement with the option below to avoid immediate snapping", MessageType.Warning);
            }
            else if (animationDistancePercentage > 105)
            {
                EditorGUILayout.HelpBox("Your animation is too long or too fast for the displacement, " +
                    "you can change the curve or add a post animation linear displacement with the option below to avoid immediate snapping", MessageType.Warning);
            }

            EditorGUILayout.PropertyField(postAnimationAdjustTimeProp);
        }

        EditorGUILayout.PropertyField(currentPanelProp);

        EditorGUILayout.PropertyField(panelSwitchEventProp);

        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Panels Settings", UnityEditor.EditorStyles.boldLabel);

        EditorGUILayout.PropertyField(panelsProp, true);

        if(GUILayout.Button("Switch To Next Panel"))
        {
            UIPanelSwitcher underlyingPanel = (UIPanelSwitcher)serializedObject.targetObject;
            underlyingPanel.SwitchToPanel((underlyingPanel.currentPanel + 1) % underlyingPanel.panels.Count);
        }

        serializedObject.ApplyModifiedProperties();
    }

    // Integrate area under AnimationCurve between start and end time
    public static float IntegrateCurve(AnimationCurve curve, int steps)
    {
        return Integrate(curve.Evaluate, curve.keys[0].time, curve.keys[curve.keys.Length - 1].time, steps);
    }

    // Integrate function f(x) using the trapezoidal rule between x=x_low..x_high
    public static float Integrate(System.Func<float, float> f, float x_low, float x_high, int N_steps)
    {
        float h = (x_high - x_low) / N_steps;
        float res = (f(x_low) + f(x_high)) / 2;
        for (int i = 1; i < N_steps; i++)
        {
            res += f(x_low + i * h);
        }
        return h * res;
    }
}
#endif