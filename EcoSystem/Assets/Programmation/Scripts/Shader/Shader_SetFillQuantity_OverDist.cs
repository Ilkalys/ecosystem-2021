using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shader_SetFillQuantity_OverDist : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  Transform       m_beginPoint;
    public  Transform       m_endpoint;
    public  Transform       m_movingPoint;

    public  Vector3         m_axis                  = Vector3.one;

    public  MeshRenderer    m_meshRenderer;
    private Material        m_material;

    public  float           m_currentFillQuantity;
    private float           m_maxDist;
    private Vector3         m_currentPos;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    private void Start()
    {
        m_maxDist = (m_endpoint.position - m_beginPoint.position).magnitude;
        m_material = m_meshRenderer.material;
        m_axis = new Vector3(m_axis.x > 0 ? 1 : 0, m_axis.y > 0 ? 1 : 0, m_axis.z > 0 ? 1 : 0);
        m_material.SetFloat("_FillQuantity", m_currentFillQuantity);
    }

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        if (m_currentPos != m_movingPoint.position)
        {
            float value;
            Vector3 normal = (m_endpoint.position - m_beginPoint.position);
            Vector3 currentPos = (m_movingPoint.position - m_beginPoint.position);

            currentPos.x *= m_axis.x;
            currentPos.y *= m_axis.y;
            currentPos.z *= m_axis.z;

            if (Vector3.Angle(normal, currentPos) > 90f)
            {
                value = 0;
            }
            else
            {
                float currentDist = (currentPos).magnitude;
                value = Mathf.Clamp(currentDist / m_maxDist, 0f, 1f);
            }
            if(m_currentFillQuantity < value)
            {
                m_currentFillQuantity = value;
                m_material.SetFloat("_FillQuantity", m_currentFillQuantity);
            }
            currentPos = m_movingPoint.position;
        }
    }
}
