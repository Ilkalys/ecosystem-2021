using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using CustomsAttributes;
using UnityEngine.Serialization;
using System;

/// <summary>
/// Script to populate a unity UI dropdown with the currently available audio output
/// and setup WWise accordingly to the selected one.
/// Updates will add support for auto binding and save of last setup.
/// </summary>
public class HFAudioDeviceRouter : MonoBehaviour
{
    [Tooltip("The dropdown to populate with the audio device list")]
    public TMP_Dropdown dropdown;
    [Tooltip("The playerPrefKey associated with this component to save previously set values in between plays. Empty or 'null' value will skip saving and loading")]
    public string playerPrefsKey;
    [Tooltip("The default string to search for to autoBind output. Override by the previously set value (saved in playerPref) if there is one. Chooses the first matching result using 'contains()'.")]
    public string defaultMatchSearch;
    [Tooltip("Wwise Audio Device name")]
    public string WwiseDeviceShareSet;
    [Tooltip("Is this device arleady linked to a system default when this component is started up")]
    public bool isLinkedAtStartup = false;
    //public AudioDeviceDropdownFiller other;

    uint audioDeviceCount = 0;
    Dictionary<int, uint?> optionsToDeviceID = new Dictionary<int, uint?>();
    // currently binded output : null is not binded, 0 is system default (Value given by Wwise when binding)
    ulong? currentOutputID = null; 
    // currently binded device : null is no device, 0 is system default (Value given by Windows when listing devices)
    uint? currentDeviceID = null; 

    bool initialized = false;

    public bool sendDebugLogs = false;

    void Start()
    {
        if(dropdown == null)
            if (!gameObject.TryGetComponent<TMP_Dropdown>(out dropdown))
            {
                Debug.LogError("Couldn't find the dropdown to get linked to : please link it in the parameter " +
                    "before reactivating this component.");
                enabled = false;
                return;
            }


        if (isLinkedAtStartup)
        {
            currentOutputID = 0;
            currentDeviceID = 0;
        }

        UpdateDevices(true);

        changeSelectedDevice(dropdown.value);
    }

    void Update()
    {
        if (audioDeviceCount != AkSoundEngine.GetWindowsDeviceCount(AkAudioDeviceState.AkDeviceState_Active))
        {
            UpdateDevices(!currentOutputID.HasValue);
        }
        //Debug.Log(gameObject.name + " has currentOutputID = " + currentOutputID + "\n and currentDeviceID = " + currentDeviceID);
    }

    /// <summary>
    /// Updates the dropdown with the currently available audio outputs
    /// </summary>
    /// <param name="selectDefault"></param>
    public void UpdateDevices(bool selectDefault = false)
    {
        initialized = false;

        string toSelect;
        int updatedSelected = 0;
        bool foundToSelect = false;
        if (selectDefault)
        {
            if (playerPrefsKey != null && playerPrefsKey != "" && PlayerPrefs.HasKey(playerPrefsKey))
                toSelect = PlayerPrefs.GetString(playerPrefsKey);
            else
                toSelect = defaultMatchSearch;
        }
        else
        {
            toSelect = dropdown.options[dropdown.value].text;
        }

        audioDeviceCount = AkSoundEngine.GetWindowsDeviceCount(AkAudioDeviceState.AkDeviceState_Active);
        dropdown.ClearOptions();
        optionsToDeviceID.Clear();
        List<TMP_Dropdown.OptionData> updatedOptions = new List<TMP_Dropdown.OptionData>();

        optionsToDeviceID.Add(0, null);
        updatedOptions.Add(new TMP_Dropdown.OptionData("None"));
        for (int i = 0; i < audioDeviceCount; i++)
        {
            uint DeviceID;
            string name = AkSoundEngine.GetWindowsDeviceName(i, out DeviceID, AkAudioDeviceState.AkDeviceState_Active);
            updatedOptions.Add(new TMP_Dropdown.OptionData(name));
            optionsToDeviceID.Add(i + 1, DeviceID);
            if (name.Contains(toSelect) && !foundToSelect)
            {
                updatedSelected = i + 1;
            }
        }

        dropdown.AddOptions(updatedOptions);
        dropdown.value = updatedSelected;

        initialized = true;

        if (sendDebugLogs)
            Debug.Log("Updated the " + gameObject.name + " device list.");

        changeSelectedDevice(updatedSelected);
    }

    /// <summary>
    /// Changes the actual audio output to the one precised in the parameter optionSelected 
    /// </summary>
    /// <param name="optionSelected">The current option chosen index (the audio device ID is found with an internal dictionnary)</param>
    public void changeSelectedDevice(int optionSelected)
    {
        if (!initialized)
            return;

        uint? deviceID;
        if (!optionsToDeviceID.TryGetValue(optionSelected, out deviceID))
        {
            Debug.LogError("Invalid option selected value");
            return;
        }

        if (currentDeviceID == deviceID)
        {
            if (sendDebugLogs)
                Debug.Log("Not updating " + gameObject.name + " value as the deviceID is the same.");
            return;
        }

        AKRESULT result;
        if (deviceID == null)
        {
            if (currentOutputID != null)
            {
                result = AkSoundEngine.RemoveOutput((ulong)currentOutputID);
                if (sendDebugLogs)
                    Debug.Log("removing output for " + gameObject.name + " : " + result + "\n" +
                        "Old ID was " + currentOutputID + ".");
                currentOutputID = null;
                currentDeviceID = null;
            }
        }
        else
        {
            AkOutputSettings newOutput = new AkOutputSettings(WwiseDeviceShareSet, (uint)deviceID);
            ulong tmpID;
            if (currentOutputID == null)
            {
                result = AkSoundEngine.AddOutput(newOutput, out tmpID);
                if (sendDebugLogs)
                    Debug.Log("Adding new output for " + gameObject.name + " : " + result + "\n" +
                        "New ID is " + tmpID + ".");
            }
            else
            {
                result = AkSoundEngine.ReplaceOutput(newOutput, (ulong)currentOutputID, out tmpID);
                if (sendDebugLogs)
                    Debug.Log("Replacing output " + gameObject.name + " : " + result + "\n" +
                        "From ID " + currentOutputID + "\n           to " + tmpID + ".");
            }
            currentOutputID = tmpID;
            currentDeviceID = deviceID;
        }
    }

    public void SetActualAsDefault()
    {
        if (playerPrefsKey != null && playerPrefsKey != "")
        {
            PlayerPrefs.SetString(playerPrefsKey, dropdown.options[dropdown.value].text);
        }
        defaultMatchSearch = dropdown.options[dropdown.value].text;
    }
}

