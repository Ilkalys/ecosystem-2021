using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

[ExecuteAlways]
public class SSEventOnTrack : SSElementOnTrack
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //The event triggered when entering the Collider of the GameObject
    public UnityEvent m_EventsOnTriggerEnter;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == SS_Character_Controller.instance.gameObject)
        {
            m_EventsOnTriggerEnter.Invoke();
        }
    }
}
