using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// A very simple scene loader which will unload all current scenes and load the newer one : 
/// usefull to load back the main menu or a cinematic only scene
/// </summary>
public class SingleSceneLoader : MonoBehaviour
{
    public List<string> singleScenesList;

    public void LoadScene(int index)
    {
        SceneManager.LoadSceneAsync(singleScenesList[index], LoadSceneMode.Single);
    }
}
