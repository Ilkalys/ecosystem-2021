using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// A script to be added to a button to make it easily toggleable
/// </summary>
public class UI_Toggleable : MonoBehaviour
{
    public Color baseColor;
    public Color toggledColor;

    private Button thisButton;
    public List<Button> ToggleOffButtons;

    // Start is called before the first frame update
    void Start()
    {
        thisButton = GetComponent<Button>();
        thisButton.onClick.AddListener(ToggleOn);

        foreach (Button button in ToggleOffButtons)
        {
            button.onClick.AddListener(ToggleOff);
        }
    }

    public void ToggleOn()
    {
        thisButton.targetGraphic.CrossFadeColor(toggledColor, 0, true, true);
    }

    public void ToggleOff()
    {
        thisButton.targetGraphic.CrossFadeColor(baseColor, 0, true, true);
    }
}
