using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CC_State : ScriptableObject
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\
    protected SS_Character_Controller instanceCC => SS_Character_Controller.instance;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public      abstract void UpdateState();
    public      abstract void OnExitState();
    public      abstract void OnEnterState();
    protected abstract bool CheckChangeState();
}
