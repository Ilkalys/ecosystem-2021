using CustomsAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.UI;

/// <summary>
/// find the inputMaster, listen to UI events and enable and disable them (as well as player inputs)
/// when switching from paused to unpaused.
/// 
/// This is now very redondant with the UI_ImputInteraction script : the content of this script
/// should ideally be merged into UI_ImputInteraction but lots of scene references rely on it 
/// and we don't have enough time left to update all references in all the scenes.
/// </summary>
public class UI_InputEnabler : MonoBehaviour
{
    InputMaster inputMaster;

    [ShowOnly(true)]
    public bool paused;

    public USharpEvent OnPauseStarted;
    public USharpEvent OnPauseFinished;

    public bool directPauseExitAuthorised = false;

    private int playerInputsDisabled = 0; // a count of the number of time SetPlayerInputs(false) is called to not restore them at the first


    void Start()
    {
        StartCoroutine(startRoutine());
    }

    IEnumerator startRoutine()
    {
        // We need to find the actual input system used
        yield return new WaitUntil(() => SS_Character_Controller.instance != null);
        inputMaster = SS_Character_Controller.instance.m_playerActions;
        
        // Calls to onPauseInput are now coming from UI_ImputInteraction in parent GameObject
        //inputMaster.UI.Pause.performed += context => onPauseInput(context);

        // We deactivate the pause menu and reactivate the buttons deactivated to be hidden during the loading of this script
        gameObject.transform.parent.gameObject.SetActive(false);
        for (int i = 0; i< gameObject.transform.childCount; i++)
        {
            GameObject children = gameObject.transform.GetChild(i).gameObject;
            children.SetActive(true);
        }

        SetUIInputs(false);
    }

    public void SetUIInputs(bool enabled)
    {
        if (enabled)
        {
            inputMaster.UI.Enable();
        }
        else
        {
            inputMaster.UI.Disable();
            inputMaster.UI.Pause.Enable();
        }
    }

    public void SetPlayerInputs(bool enabled)
    {
        if (enabled && playerInputsDisabled <= 1)
        {
            inputMaster.Player.Enable();
            playerInputsDisabled = 0;
        }
        else if (enabled)
        {
            playerInputsDisabled--;
        }
        else
        {
            inputMaster.Player.Disable();
            playerInputsDisabled++;
        }
    }

    public void SetUIPauseButton(bool enabled)
    {
        if (enabled)
            inputMaster.UI.Pause.Enable();
        else
            inputMaster.UI.Pause.Disable();
    }

    public void OnPauseInput(InputAction.CallbackContext context)
    {
        if (!paused)
        {
            OnPauseStarted.Invoke();
            paused = true;
        }
        else if (directPauseExitAuthorised)
        {
            OnPauseFinished.Invoke();
            paused = false;
        }
    }

    public void FakePauseInput()
    {
        if (!paused)
        {
            OnPauseStarted.Invoke();
            paused = true;
        }
        else
        {
            OnPauseFinished.Invoke();
            paused = false;
        }
    }



    public void FakeRightMovementInput()
    {
        Keyboard keyboard = InputSystem.GetDevice<Keyboard>();
        KeyboardState stateA = new KeyboardState();
        stateA.Press(Key.D);

        InputSystem.QueueStateEvent(keyboard, stateA);
        //SetPlayerInputs(false);
    }

    public void EndFakeRightMovementInput()
    {
        //SetPlayerInputs(true);
        Keyboard keyboard = InputSystem.GetDevice<Keyboard>();
        KeyboardState stateB = new KeyboardState();
        stateB.Release(Key.D);
        InputSystem.QueueStateEvent(keyboard, stateB);
    }

    /*
    public void changeActive(GameObject PauseUI)
    {
        PauseUI.SetActive(!PauseUI.activeSelf);
    }
    */
}