Shader "FullScreen/KuwaharaPostProcess"
{
    Properties{
        _MinRadius("Min size of the Strokes",Range(0, 20)) = 3
        _MaxRadius("Max size of the Strokes", Range(0, 20)) = 3
        _IntensityByDistance("Decreasing of the effect by closing the distance", Range(0, 1000)) = 2
    }

    HLSLINCLUDE

    #pragma vertex Vert

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone vulkan metal switch

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/RenderPipeline/RenderPass/CustomPass/CustomPassCommon.hlsl"

    // The PositionInputs struct allow you to retrieve a lot of useful information for your fullScreenShader:
    // struct PositionInputs
    // {
    //     float3 positionWS;  // World space position (could be camera-relative)
    //     float2 positionNDC; // Normalized screen coordinates within the viewport    : [0, 1) (with the half-pixel offset)
    //     uint2  positionSS;  // Screen space pixel coordinates                       : [0, NumPixels)
    //     uint2  tileCoord;   // Screen tile coordinates                              : [0, NumTiles)
    //     float  deviceDepth; // Depth from the depth buffer                          : [0, 1] (typically reversed)
    //     float  linearDepth; // View space Z coordinate                              : [Near, Far]
    // };

    // To sample custom buffers, you have access to these functions:
    // But be careful, on most platforms you can't sample to the bound color buffer. It means that you
    // can't use the SampleCustomColor when the pass color buffer is set to custom (and same for camera the buffer).
    // float4 SampleCustomColor(float2 uv);
    // float4 LoadCustomColor(uint2 pixelCoords);
    // float LoadCustomDepth(uint2 pixelCoords);
    // float SampleCustomDepth(float2 uv);

    // There are also a lot of utility function you can use inside Common.hlsl and Color.hlsl,
    // you can check them out in the source code of the core SRP package.

    struct region {
        int x1, y1, x2, y2;
    };

    uint _MinRadius;
    uint _MaxRadius;
    uint _IntensityByDistance;

    float4 FullScreenPass(Varyings varyings) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(varyings);
        float depth = LoadCameraDepth(varyings.positionCS.xy);
        PositionInputs posInput = GetPositionInput(varyings.positionCS.xy, _ScreenSize.zw, depth, UNITY_MATRIX_I_VP, UNITY_MATRIX_V);
        float3 viewDirection = GetWorldSpaceNormalizeViewDir(posInput.positionWS);
        float4 color = float4(0.0, 0.0, 0.0, 0.0);

        // Load the camera color buffer at the mip 0 if we're not at the before rendering injection point
        if (_CustomPassInjectionPoint != CUSTOMPASSINJECTIONPOINT_BEFORE_RENDERING)
            color = float4(CustomPassLoadCameraColor(varyings.positionCS.xy, 0), 1);
        
        // Adjust the radius with the distance
        int radius = max(_MinRadius, lerp(0, _MaxRadius * pow((1 - depth), abs(_IntensityByDistance)), 1 - depth));
        
        // Add your custom pass code here

        float2 uv = varyings.positionCS.xy;
        float n = float((radius + 1) * (radius + 1));
        
        float3 m[4];
        float3 s[4];

        for (int k = 0; k < 4; ++k)
        {
            m[k] = float3(0, 0, 0);
            s[k] = float3(0, 0, 0);
        }

        region R[4] = {
            {-radius, -radius,       0,       0},
            {       0, -radius, radius,       0},
            {       0,        0, radius, radius},
            {-radius,        0,       0, radius}
        };

        for (k = 0; k < 4; ++k) 
        {
            for (int j = R[k].y1; j <= R[k].y2; ++j) 
            {

                for (int i = R[k].x1; i <= R[k].x2; ++i) 
                {
                    float3 c = float3(CustomPassLoadCameraColor(varyings.positionCS.xy + float2(i, j),0));
                    m[k] += c;
                    s[k] += c * c;
                }
            }
        }

        float min = 1e+2;
        float s2;
        for (k = 0; k < 4; ++k)
        {
            m[k] /= n;
            s[k] = abs(s[k] / n - m[k] * m[k]);

            s2 = s[k].r + s[k].g + s[k].b;
            if (s2 < min) {
                min = s2;
                color.rgb = m[k].rgb;
            }
        }

        // Fade value allow you to increase the strength of the effect while the camera gets closer to the custom pass volume
        float f = 1 - abs(_FadeValue * 2 - 1);
        return float4(color.rgb + f, color.a);
    }

    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "Kuwahara Filter"

            ZWrite Off
            ZTest Always
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off

            HLSLPROGRAM
                #pragma fragment FullScreenPass
            ENDHLSL
        }
    }
    Fallback Off
}
