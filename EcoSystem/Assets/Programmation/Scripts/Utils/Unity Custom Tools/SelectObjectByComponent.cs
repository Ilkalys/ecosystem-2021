#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

/// <summary>
/// A rework of the SelectByName tool made for 51 B.A.F.F.E.S ...
/// </summary>
public class SelectObjectByComponent : ScriptableWizard
{
    [Tooltip("The component name to search : if this component is found, the corresponding GameObject will be in the selection\n" +
             " - Leave it blank to not select any object.")]
    public string componentToSearch = "";
    [Tooltip("The component to avoid : if this component is found, the corresponding GameObject will be deselected.\n" +
             " - Leave it blank to not deselect any object.")]
    public string componentToAvoid = "";
    [Tooltip("Add childrens of the selected GameObjects to the list of possible results.")]
    public bool searchChildrens;
    [Space]
    [Tooltip("If activated, debug messages will be printed")]
    public bool debugOn = false;

    [MenuItem("Custom Tools/Select by Component")]
    static void CreateWizard()
    {
        SelectObjectByComponent createdWizard = DisplayWizard<SelectObjectByComponent>("Select GameObjects by Component", "Select and Close", "Select in Active Scene");
        Selection.selectionChanged += createdWizard.OnWizardUpdate;
    }

    void filterAndSelectObjects()
    {
        if (checkValues(true))
        {
            List<GameObject> objectsToSearch;
            if (Selection.gameObjects.Length > 0)
            {
                objectsToSearch = new List<GameObject>(Selection.gameObjects);
                if (searchChildrens)
                    objectsToSearch = AddChildrensOf(objectsToSearch);
            }
            else
            {
                objectsToSearch = new List<GameObject>(UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects());
                objectsToSearch = AddChildrensOf(objectsToSearch);
            }

            Selection.objects = getCorrespondingGameObjectsIn(objectsToSearch.ToArray());
        }
        else
        {
            Debug.LogError("SelectObjectByComponent : Invalid value found, selection unchanged...");
        }
    }

    GameObject[] getCorrespondingGameObjectsIn(GameObject[] gameObjectsTable)
    {
        List<GameObject> toSelect = new List<GameObject> { };

        foreach (GameObject gameObject in gameObjectsTable)
        {
            bool isValid;

            if (componentToSearch != "")
            {
                isValid = gameObject.GetComponent(componentToSearch) != null;

                if (debugOn)
                {
                    Debug.Log("Searched \"" + componentToSearch + "\" in \"" + gameObject.name + "\" and found :" + isValid);
                }
            }
            else
            {
                isValid = true;
            }

            if (componentToAvoid != "" && isValid)
            {
                isValid = gameObject.GetComponent(componentToAvoid) == null;

                if (debugOn)
                {
                    Debug.Log("Searched \"" + componentToAvoid + "\" in \"" + gameObject.name + "\" and found :" + !isValid);
                }
            }

            if (isValid)
            {
                toSelect.Add(gameObject);
            }
        }

        return toSelect.ToArray();
    }

    List<GameObject> AddChildrensOf(List<GameObject> gameObjectsList)
    {
        HashSet<GameObject> gameObjectsSet = new HashSet<GameObject>(gameObjectsList);
        int initialSize = gameObjectsList.Count;
        for (int i = 0; i < initialSize; i++)
        {
            GameObject gameObject = gameObjectsList[i];
            foreach (Transform transform in gameObject.GetComponentsInChildren<Transform>(true))
            {
                gameObjectsSet.Add(transform.gameObject);
            }
        }
        return gameObjectsSet.Distinct().ToList();
    }

    bool checkValues(bool isLaunching)
    {
        if (isLaunching)
        {
            if (componentToSearch == "" && componentToAvoid == "")
            {
                errorString = "No filter detected, please make sure to enter usefull values!";
                return false;
            }
        }


        helpString = "This tool will filter the currently selected objects with the specified values. " +
            "If you select zero object the filter will be applied to the whole active scene.\n";

        if (Selection.gameObjects.Length == 0)
        {
            helpString += "No object selected the whole active scene will be searched.";
            createButtonName = "Select and Close";
            otherButtonName = "Select in Active Scene";
        }
        else
        {
            helpString += Selection.gameObjects.Length.ToString() + " object" + (Selection.gameObjects.Length > 1 ? "s" : "") +
                " selected as the base of the search.";
            createButtonName = "Reselect and Close";
            otherButtonName = "Reselect on current selection";
        }



        return true;
    }


    void OnWizardCreate()
    {
        filterAndSelectObjects();
    }

    void OnWizardOtherButton()
    {
        filterAndSelectObjects();
    }

    void OnWizardUpdate()
    {
        if (checkValues(false))
        {
            errorString = "";
        }
    }

}
#endif