using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterController/State/Walking")]
public class CC_State_Walking : CC_State
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Tooltip("The maximum speed the character can have while walking")]
    public float    m_maxWalkSpeed      = 5f;
    [Tooltip("The acceleration of the character")]
    public float    m_acceleration      = 5f;
    [Tooltip("The acceleration when the character change direction")]
    public float    m_turnOverAcc       = 10f;
    [Tooltip("The friction when the character is on the Ground")]
    public float    m_groundDrag        = 0.001f;

    public float    m_decceleration     = 5f;

    /// <summary> the gravity on the ground </summary>
    public float    m_groundedGravity   = -.05f;

    private float   m_currentSpeed;
    private float   m_currentMaxSpeed;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        if ( CheckChangeState() )
            return;
        HandleGravity();
        HandleSpeed();
        
        Vector3 forward = Vector3.Cross( instanceCC.transform.right, instanceCC.m_normalGround );
        instanceCC.m_currentMovement = forward * Mathf.Abs( instanceCC.m_currentSpeed ) + Vector3.up * instanceCC.m_current2DMovement.y;

        instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        if ( instanceCC.m_brainState.brain_CC_State_Waiting.canUse && instanceCC.m_isJumpPressed && ( instanceCC.SameDirection( instanceCC.m_Object_InFront ) ) )
        {
            instanceCC.ChangeState( instanceCC.m_brainState.brain_CC_State_Waiting );
            return true;
        }
        if ( !instanceCC.m_isGrounded || instanceCC.m_isJumping )
        {
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Jumping );
            return true;
        }
        if ( instanceCC.m_brainState.brain_CC_State_ClimbingWall.canUse && instanceCC.SameDirection( instanceCC.m_wallClimbable_InFront ) && ( instanceCC.m_isLookingRight ? instanceCC.m_currentMovementInput > 0 : instanceCC.m_currentMovementInput < 0 ) )
        {
            instanceCC.ChangeState( instanceCC.m_brainState.brain_CC_State_ClimbingWall );
            return true;
        }
        if ( instanceCC.m_isInteractPressed && instanceCC.m_ObjectPushable && instanceCC.LookAt( instanceCC.m_ObjectPushable.gameObject ) )
        {
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Pushing );
            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnExitState()
    {
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState()
    {
        m_currentSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_currentMaxSpeed = m_maxWalkSpeed;
        m_currentMaxSpeed = instanceCC.m_currentMaxSpeed;
    }

    //--------------------------------------------------------------------------------------\\
    void HandleSpeed()
    {
        m_currentMaxSpeed = m_maxWalkSpeed * Mathf.Abs( instanceCC.m_currentMovementInput );
        if ( Mathf.Abs( m_currentSpeed ) <= m_currentMaxSpeed)
        {
            float acc = ( ( Mathf.Sign(m_currentSpeed) == Mathf.Sign(instanceCC.m_currentMovementInput) ) ) ? m_acceleration : m_turnOverAcc;
            float addedSpeed = (acc * Time.fixedDeltaTime * instanceCC.m_currentMovementInput);
            m_currentSpeed = Mathf.Clamp(m_currentSpeed + addedSpeed, -m_currentMaxSpeed, m_currentMaxSpeed );
        }
        else
        {
            m_currentSpeed *= Mathf.Pow(m_groundDrag, Time.fixedDeltaTime);
            if (m_currentSpeed < 0.01f && m_currentSpeed > -0.01f)
                m_currentSpeed = 0f;
        }

        instanceCC.m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_currentSpeed = m_currentSpeed;
    }

    //--------------------------------------------------------------------------------------\\
    void HandleGravity()
    {
        instanceCC.m_current2DMovement.y = m_groundedGravity;
    }

}
