using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterController/State/OnEdge")]
public class CC_State_OnEdge : CC_State_Deactivatable
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [HideInInspector]
    private         Vector3 m_targetPos;
    private const   float   m_minSpeedToTarget = 2f;
    private         bool    m_inTargetPos;
    private         float   m_previousSpeed;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        instanceCC.m_currentSpeed = 0;
        instanceCC.m_currentMaxSpeed = 0;
        instanceCC.m_currentMovement = Vector3.zero;
        instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;

        if ( !m_inTargetPos )
        {
            float step = Mathf.Max(m_minSpeedToTarget, m_previousSpeed) * Time.deltaTime;
            instanceCC.transform.position = Vector3.MoveTowards( instanceCC.transform.position, m_targetPos, step );
            if ( Vector3.Distance( instanceCC.transform.position, m_targetPos ) < 0.001f )
            {
                m_inTargetPos = true;
            }
        }
        CheckChangeState();
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        if ( instanceCC.m_isJumpPressed )
        {
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.m_isOnEdge = false;
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_JumpingFromWall );
            return true;
        }

        bool inputToForward = (instanceCC.m_isLookingRight ? instanceCC.m_currentMovementInput > 0 : instanceCC.m_currentMovementInput < 0);
        if ( instanceCC.m_brainState.brain_CC_State_Waiting.canUse && ( ( instanceCC.m_currentVerticalMovementInput > 0 && instanceCC.m_currentMovementInput == 0 ) || inputToForward ) )
        {
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Waiting );
            return true;
        }
        if ( instanceCC.m_currentVerticalMovementInput < 0 )
        {
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.m_isOnEdge = false;
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Jumping );
            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnExitState()
    {
        m_targetPos = Vector3.zero;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState()
    {
        m_targetPos = instanceCC.m_edge_InFront.GetComponent<OnTrigger_InfosToPlayer>().GetTargetPos();
        m_inTargetPos = m_targetPos == Vector3.zero;

        m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_isJumpPressed = false;
        instanceCC.m_isOnEdge = true;
        instanceCC.m_isInteractPressed = false;
    }
}
