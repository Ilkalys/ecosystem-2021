using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterController/State/ClimbingWall")]
public class CC_State_ClimbingWall : CC_State_Deactivatable
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Tooltip("The maximum speed the character can have while walking")]
    public          float   m_maxClimbSpeed = 5f;
    [Tooltip("The acceleration of the character")]
    public          float   m_acceleration = 5f;
    [Tooltip("The acceleration when the character change direction")]
    public          float   m_turnOverAcc = 10f;
    [Tooltip("The friction when the character is on the Ground")]
    public          float   m_climbDrag = 0.001f;

    public          float   m_decceleration = 5f;

    private         float   m_currentSpeed;
    private         float   m_currentMaxSpeed;

    private         Vector3 m_targetPos;
    private         bool    m_inTargetPos;
    private const   float   m_minSpeedToTarget = 2f;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        if ( CheckChangeState() )
            return;

        if ( !m_inTargetPos )
        {
            float step = Mathf.Max(m_minSpeedToTarget, m_currentSpeed) * Time.deltaTime;
            instanceCC.transform.position = Vector3.MoveTowards( instanceCC.transform.position, m_targetPos, step );
            if ( Vector3.Distance( instanceCC.transform.position, m_targetPos ) < 0.001f )
            {
                m_inTargetPos = true;
            }
        }

        HandleSpeed();
        instanceCC.m_currentMovement = Vector3.up * instanceCC.m_current2DMovement.y;
        instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        if ( instanceCC.m_isJumping )
        {
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.ChangeStateWithUpdate( SS_Character_Controller.instance.m_brainState.brain_CC_State_JumpingFromWall );
            return true;
        }
        if ( instanceCC.m_isGrounded && ( ( instanceCC.m_isLookingRight ? instanceCC.m_currentMovementInput < 0 : instanceCC.m_currentMovementInput > 0 ) || instanceCC.m_currentVerticalMovementInput < 0 ) )
        {
            instanceCC.m_previousSpeed = instanceCC.m_currentSpeed;
            instanceCC.m_currentSpeed = ( instanceCC.m_isLookingRight ? -1 : 1 );
            instanceCC.m_currentMovement = new Vector3( instanceCC.transform.forward.x, 0, instanceCC.transform.forward.z ) * Mathf.Abs( instanceCC.m_currentSpeed );
            instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Walking );
            return true;
        }
        if ( instanceCC.m_brainState.brain_CC_State_OnEdge.canUse && instanceCC.m_edge_InFront )
        {
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_OnEdge );
            return true;
        }
        if ( !instanceCC.m_wallClimbable_InFront )
        {
            instanceCC.StartCoroutine( "DesactivateState", this );
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Jumping );
            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnExitState()
    {
        m_targetPos = Vector3.zero;
        instanceCC.m_isOnWall = false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState()
    {
        m_targetPos = instanceCC.m_wallClimbable_InFront.GetComponent<OnTrigger_InfosToPlayer>().GetTargetPos();
        m_inTargetPos = m_targetPos == Vector3.zero;

        instanceCC.m_isOnWall = true;
        m_currentSpeed = instanceCC.m_currentSpeed;
        m_currentMaxSpeed = instanceCC.m_currentMaxSpeed;
    }

    //--------------------------------------------------------------------------------------\\
    void HandleSpeed()
    {
        if (instanceCC.m_currentVerticalMovementInput != 0)
        {
            if (m_currentMaxSpeed > m_maxClimbSpeed)
                m_currentMaxSpeed -= Time.deltaTime * m_decceleration;
            else
            {
                m_currentMaxSpeed = m_maxClimbSpeed;
            }
            float absCurrentMaxSpeed = m_currentMaxSpeed * Mathf.Abs(instanceCC.m_currentVerticalMovementInput);
            float acc = (Mathf.Sign(m_currentSpeed) == Mathf.Sign(instanceCC.m_currentVerticalMovementInput)) ? m_acceleration : m_turnOverAcc;
            float addedSpeed = (acc * Time.fixedDeltaTime * instanceCC.m_currentVerticalMovementInput);
            m_currentSpeed = Mathf.Clamp(m_currentSpeed + addedSpeed, -absCurrentMaxSpeed, absCurrentMaxSpeed);
        }
        else
        {
            if (m_currentMaxSpeed > m_maxClimbSpeed)
                m_currentMaxSpeed *= Mathf.Pow(m_climbDrag, Time.fixedDeltaTime);
            else
            {
                m_currentMaxSpeed = m_maxClimbSpeed;
            }
            m_currentSpeed *= Mathf.Pow(m_climbDrag, Time.fixedDeltaTime);
            if (m_currentSpeed < 0.02f && m_currentSpeed > -0.02f)
                m_currentSpeed = 0f;
        }

        instanceCC.m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_currentMaxSpeed = m_currentMaxSpeed;
        instanceCC.m_currentSpeed = m_currentSpeed;
        instanceCC.m_current2DMovement.y = m_currentSpeed;
    }
}
