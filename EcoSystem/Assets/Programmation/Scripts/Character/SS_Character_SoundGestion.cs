using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SS_Character_SoundGestion : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  string[]        events;
    public  string[]        switchs;
    public  string[]        states;

    public  int             indexOfConcrete;
    public  int             indexOfDirt;
    public  int             indexOfGrass;
    public  int             indexOfGravel;
    public  int             indexOfMetal;
    public  int             indexOfMud;
    public  int             indexOfSand;
    public  int             indexOfWater;
    public  int             indexOfTerrain;

    public  bool            onWater;

    private SoundGestion    m_cameraSoundGestion;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void PlayEvent( int index )
    {
        UpdateCurrentFootstepMaterial();
        AkSoundEngine.PostEvent( events[index], this.gameObject );
    }

    //--------------------------------------------------------------------------------------\\
    public void PlaySwitch( int index )
    {
        string[] indexs = switchs[index].Split( ' ' );
        AkSoundEngine.SetSwitch( AkSoundEngine.GetIDFromString( indexs[0] ), AkSoundEngine.GetIDFromString( indexs[1] ), this.gameObject );
    }

    //--------------------------------------------------------------------------------------\\
    public void PlayState( int index )
    {
        string[] indexs = states[index].Split( ' ' );
        AkSoundEngine.SetState( AkSoundEngine.GetIDFromString( indexs[0] ), AkSoundEngine.GetIDFromString( indexs[1] ) );
    }

    //--------------------------------------------------------------------------------------\\
    public void UpdateCurrentFootstepMaterial()
    {
        if ( onWater )
        {
            PlaySwitch( indexOfWater );
            return;
        }
        if ( SS_Character_Controller.instance.m_groundTag != null )
        {
            switch ( ( SS_Character_Controller.instance.m_groundTag ) )
            {
                case "Concrete":
                    {
                        PlaySwitch( indexOfConcrete );
                        break;
                    }
                case "Dirt":
                    {
                        PlaySwitch( indexOfDirt );
                        break;
                    }
                case "Grass":
                    {
                        PlaySwitch( indexOfGrass );
                        break;
                    }
                case "Gravel":
                    {
                        PlaySwitch( indexOfGravel );
                        break;
                    }
                case "Metal":
                    {
                        PlaySwitch( indexOfMetal );
                        break;
                    }
                case "Mud":
                    {
                        PlaySwitch( indexOfMud );
                        break;
                    }
                case "Sand":
                    {
                        PlaySwitch( indexOfSand );
                        break;
                    }
                default:
                    {
                        PlaySwitch( indexOfTerrain );
                        break;
                    }
            }
        }
    }

    //--------------------------------------------------------------------------------------\\
    public void FootSide(float isRight)
    {
        AkSoundEngine.SetRTPCValue("Panning", isRight);
    }
}
