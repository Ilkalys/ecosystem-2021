using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class StopWaitingStateAndTeleport : StateMachineBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [FormerlySerializedAs("offset")]
    public Vector2 m_localForwardTeleportation;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool( "High_Obstacle", false );
        animator.SetBool( "Medium_Obstacle", false );
        animator.SetBool( "Climb", false );
        SS_Character_Controller.instance.m_isWaiting = false;
        SS_Character_Controller.instance.transform.position += m_localForwardTeleportation.x * SS_Character_Controller.instance.transform.forward + m_localForwardTeleportation.y * Vector3.up;
    }
}
