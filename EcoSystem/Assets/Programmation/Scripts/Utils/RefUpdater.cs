using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A class that holds a reference to an object and invokes events when the reference is get or set.
/// </summary>
/// <typeparam name="T">The type of the object to hold</typeparam>
public class RefUpdater<T>
{
    private T _reference;
    public T Reference
    {
        get 
        {
            OnReferenceGet?.Invoke(_reference);
            return _reference; 
        }
        set
        {
            T prevRef = _reference;
            _reference = value;
            OnReferenceSet?.Invoke(value);
            if((value is null && prevRef != null) || (value != null && !value.Equals(prevRef)))
                OnReferenceChanged?.Invoke(value);
            if (value is null && !(prevRef is null))
                OnReferenceDropped?.Invoke(value);
        }
    }

    public RefUpdater(T value)
    {
        Reference = value;
    }

    //Events //TODO change them to USharpEvent if possible after implementation
    public delegate void ReferenceAccessedHandler(T newValue);
    public event ReferenceAccessedHandler OnReferenceGet;
    public event ReferenceAccessedHandler OnReferenceChanged;
    public event ReferenceAccessedHandler OnReferenceSet;
    public event ReferenceAccessedHandler OnReferenceDropped;

    static public implicit operator T(RefUpdater<T> value)
    {
        return value.Reference;
    }

    static public explicit operator RefUpdater<T>(T value)
    {
        return new RefUpdater<T>(value);
    }

    public override bool Equals(object obj)
    {
        return obj is RefUpdater<T> refUpdater &&
               EqualityComparer<T>.Default.Equals(_reference, refUpdater._reference);
    }

    public override int GetHashCode()
    {
        return -417141133 + EqualityComparer<T>.Default.GetHashCode(_reference);
    }
}
