using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AdditionnalColorTintTargets : MonoBehaviour
{
    public List<Graphic> AdditionnnalGraphicTargets;
    private Selectable selectable;

    private bool initialized = false;

    private void Start()
    {
        if (TryGetComponent<Selectable>(out selectable))
        {
            if (selectable.transition != Selectable.Transition.ColorTint)
            {
                Debug.LogError("The selectable of this object (" + gameObject.name + ") is not setup to work with Color Tint transitions!");
            }
            else if (selectable.targetGraphic is null)
            {
                Debug.LogError("The selectable of this (" + gameObject.name + ") has not any Target Graphic set!\n" +
                    "One is needed as it will be used to detect the color the additionnal targets should be set to.");
            }
            else
            {
                initialized = true;
            }
        }
        else
        {
            Debug.LogError("Couldn't find a selectable in this object : " + gameObject.name);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (initialized)
        {
            Color detectedColor = selectable.targetGraphic.canvasRenderer.GetColor();
            foreach (Graphic target in AdditionnnalGraphicTargets)
            {
                target.CrossFadeColor(detectedColor, 0, true, true);
            }
        }
    }
}