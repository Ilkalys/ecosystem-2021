using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SoundGestion : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  string[]    events;
    public  string[]    switchs;
    public  string[]    states;

    public  GameObject  target;

    public  UnityEvent  onTriggerEnter;
    public  UnityEvent  onTriggerExit;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void PlayEvent(int index)
    {
        AkSoundEngine.PostEvent( events[index], target ? target : this.gameObject );
    }

    //--------------------------------------------------------------------------------------\\
    public void PlaySwitch( int index )
    {
        string[] indexs = switchs[index].Split( ' ' );
        AkSoundEngine.SetSwitch( AkSoundEngine.GetIDFromString( indexs[0] ) , AkSoundEngine.GetIDFromString( indexs[1] ), target ? target : this.gameObject );
    }

    //--------------------------------------------------------------------------------------\\
    public void PlayState( int index )
    {
        string[] indexs = states[index].Split( ' ' );
        AkSoundEngine.SetState( AkSoundEngine.GetIDFromString( indexs[0] ), AkSoundEngine.GetIDFromString( indexs[1] ) );
    }

    //--------------------------------------------------------------------------------------\\
    private void OnTriggerEnter( Collider other )
    {
        if ( other.gameObject == SS_Character_Controller.instance.gameObject )
        {
            onTriggerEnter.Invoke();
        }
    }

    //--------------------------------------------------------------------------------------\\
    private void OnTriggerExit( Collider other )
    {
        if ( other.gameObject == SS_Character_Controller.instance.gameObject )
        {
            onTriggerExit.Invoke();
        }
    }
}
