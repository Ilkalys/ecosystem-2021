using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SetAlphaUiPushableObject : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  Image   m_buttonLeft;
    public  Image   m_buttonRight;
    public  Image   m_addImage;

    public  Vector2 RangeDistanceForFading;
    private float   m_counter_beforeFadeIn;
    private float   m_alphaValue;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    private void LateUpdate()
    {
        float dist = Vector3.Distance( SS_Character_Controller.instance.transform.position, this.transform.position );
        if ( dist > RangeDistanceForFading.y )
        {
            m_alphaValue = 0;
        }
        else if ( dist < RangeDistanceForFading.x )
        {
            m_alphaValue = 1;
        }
        else
        {
            m_alphaValue = 1 - ( dist - RangeDistanceForFading.x ) / ( RangeDistanceForFading.y - RangeDistanceForFading.x );
        }

        if ( SS_Character_Controller.instance.m_ObjectPushable && SS_Character_Controller.instance.m_isInteractLeftPressed && SS_Character_Controller.instance.m_isInteractRightPressed )
        {
            m_counter_beforeFadeIn = 5;
            SetAlpha( m_buttonLeft, ( true ), 0 );
            SetAlpha( m_buttonRight, ( true ), 0 );
            SetAlpha( m_addImage, ( true ), 0 );
        }
        else if ( m_counter_beforeFadeIn >= 0 )
        {
            m_counter_beforeFadeIn -= Time.deltaTime;
        }
        else
        {
            SetAlpha( m_buttonLeft, ( SS_Character_Controller.instance.m_ObjectPushable && SS_Character_Controller.instance.m_isInteractLeftPressed ), m_alphaValue );
            SetAlpha( m_buttonRight, ( SS_Character_Controller.instance.m_ObjectPushable && SS_Character_Controller.instance.m_isInteractRightPressed ), m_alphaValue );
            SetAlpha( m_addImage, ( SS_Character_Controller.instance.m_ObjectPushable && SS_Character_Controller.instance.m_isInteractLeftPressed && SS_Character_Controller.instance.m_isInteractRightPressed ), m_alphaValue );
        }
    }

    //--------------------------------------------------------------------------------------\\
    public void SetAlpha( Image image, bool goToAlphaZero, float initAlpha )
    {
        Color color = image.color;

        if ( !goToAlphaZero )
        {
            if ( color.a < initAlpha )
            {
                color.a += Time.deltaTime * 2f;
            }
            else
            {
                color.a = initAlpha;
            }
        }
        else
        {
            if ( color.a > 0 )
            {
                color.a -= Time.deltaTime * 2f;
            }
            else
            {
                color.a = 0;
            }
        }
        image.color = color;
    }

    //--------------------------------------------------------------------------------------\\
    public void ResetAll()
    {
        m_alphaValue = 0;
        ResetImage( m_buttonLeft );
        ResetImage( m_buttonRight );
        ResetImage( m_addImage );
    }

    //--------------------------------------------------------------------------------------\\
    private void ResetImage( Image image )
    {
        Color color = image.color;
        color.a = m_alphaValue;
        image.color = color;
    }
}
