using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// A list of level and their dedicated scenes to load as well as their priorised one : 
/// the one that will set the spawn and camera position at startup. 
/// </summary>
[CreateAssetMenu(menuName = "Scene Management/Levels Scenes Distribution")]
public class LevelsScenesDistribution : ScriptableObject
{
    public string globalScene;
    public List<LevelScenesDetails> levelsDetails;


    /// <summary>
    /// Search the levels for one with their scene set as "Primary" corresponding with the string given.
    /// </summary>
    /// <param name="sceneToSearch">The name of the scene to search</param>
    /// <returns>the index of the first level that corresponds, or -1 if not found.</returns>
    public int SearchLevelWithPrimarySceneName(string sceneToSearch)
    {
        if (sceneToSearch == null)
        {
            return -1;
        }

        for (int i = 0; i < levelsDetails.Count; i++)
        {
            if (sceneToSearch.Equals(levelsDetails[i].GetPrimaryScene))
                return i;
        }

        return -1;
    }
}

[System.Serializable]
public class LevelScenesDetails
{
    public List<string> scenesToLoad; //Using Serializable Scene instead of a string would be cool!

    public int indexOfPrimaryScene;

    public string GetPrimaryScene => 
        (indexOfPrimaryScene >= 0 && indexOfPrimaryScene < scenesToLoad.Count) ? scenesToLoad[indexOfPrimaryScene] : null;
}

#if UNITY_EDITOR

[CanEditMultipleObjects]
[CustomEditor(typeof(LevelsScenesDistribution))]
public class LevelsScenesDetailsEditor : Editor
{
    SerializedProperty levelsDetailsProperty;
    SerializedProperty globalSceneProperty;

    private void OnEnable()
    {
        globalSceneProperty = serializedObject.FindProperty("globalScene");   
        levelsDetailsProperty = serializedObject.FindProperty("levelsDetails");   
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        
        EditorGUILayout.PropertyField(globalSceneProperty);
        
        if (serializedObject.isEditingMultipleObjects)
        {
            bool GlobalSceneExist = false;
            foreach (LevelsScenesDistribution targetObject in serializedObject.targetObjects)
                GlobalSceneExist |= EditorUtils.SceneExists(targetObject.globalScene);
            if (GlobalSceneExist)
            {
                EditorGUILayout.HelpBox("At Least one of the selected objects has a non referenced GloablScene name. " +
                    "Select them independantly for a more precise message on the concerned one.", MessageType.Error);
            }
        }
        else
        {
            if (!EditorUtils.SceneExists(globalSceneProperty.stringValue))
            {
                EditorGUILayout.HelpBox("Couldn't find scene named " + globalSceneProperty.stringValue + 
                    " in the build settings! Please check the name or add the scene in the Build Settings", MessageType.Error);
            }
        }


        EditorGUILayout.PropertyField(levelsDetailsProperty);

        if (serializedObject.isEditingMultipleObjects)
        {
            bool hasSceneRepetition = false;
            foreach (LevelsScenesDistribution targetObject in serializedObject.targetObjects)
                hasSceneRepetition |= HasPrimarySceneRepetition(targetObject, out string _, out string _, out string _);
            if (hasSceneRepetition)
            {
                EditorGUILayout.HelpBox("At Least one of the selected objects is having different levels " +
                    "referencing the same scene as their primary one! Select them independantly for a " +
                    "more precise message on the concerned one.", MessageType.Error);
            }
        }
        else
        {
            if (HasPrimarySceneRepetition(serializedObject.targetObject as LevelsScenesDistribution, out string detectedSimilar1, out string detectedSimilar2, out string sceneName))
            {
                EditorGUILayout.HelpBox("Detected different levels referencing the same scene as their primary one!\n" +
                    detectedSimilar1 + " = " + detectedSimilar2 + " = \"" + sceneName + "\"", MessageType.Error);
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    bool HasPrimarySceneRepetition(LevelsScenesDistribution targetObject, out string detectedSimilar1, out string detectedSimilar2, out string sceneName)
    {
        detectedSimilar1 = null;
        detectedSimilar2 = null;
        sceneName = null;

        for (int i = 0; i < targetObject.levelsDetails.Count; i++)
        {
            for (int j = i+1; j < targetObject.levelsDetails.Count; j++)
            {
                if (string.Equals(targetObject.levelsDetails[i].GetPrimaryScene, targetObject.levelsDetails[j].GetPrimaryScene))
                {
                    detectedSimilar1 = "Level " + i + " Scene " + targetObject.levelsDetails[i].indexOfPrimaryScene;
                    detectedSimilar2 = "Level " + j + " Scene " + targetObject.levelsDetails[j].indexOfPrimaryScene;
                    sceneName = targetObject.levelsDetails[i].GetPrimaryScene;
                    return true;
                }
            }
        }

        return false;
    }
}

[CustomPropertyDrawer(typeof(LevelScenesDetails))]
public class LevelScenesDetailPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        label.text = label.text.Replace("Element", "Level");
        EditorGUI.BeginProperty(position, label, property);

        if (!TryGetMessage(property, out string message, out MessageType messageType))
        {
            Rect helpBoxPos = new Rect(position);
            helpBoxPos.height = EditorGUIUtility.singleLineHeight * 2;
            helpBoxPos.y += position.height - helpBoxPos.height;
            EditorGUI.HelpBox(helpBoxPos, message, messageType);
        }

        EditorGUI.PropertyField(position, property, label, true);

        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true) + (TryGetMessage(property, out _, out _) ? 0 : EditorGUIUtility.singleLineHeight * 2);
    }

    bool TryGetMessage(SerializedProperty property, out string message, out MessageType messageType)
    {
        message = "";
        messageType = MessageType.None;

        string GlobalSceneName = "GlobalScene"; //TODO search for this instead of hardcodding it!

        LevelScenesDetails levelDetailsObject = EditorUtils.GetActualObjectForSerializedProperty<LevelScenesDetails>(fieldInfo, property);
        if (property.hasMultipleDifferentValues)
        {
            message = "Cannot assert Scene names when multi editing different objects";
            messageType = MessageType.Warning;
        }
        else if (!ChecksScenes(levelDetailsObject.scenesToLoad, out string ErrorMessage, GlobalSceneName))
        {
            message = ErrorMessage;
            messageType = MessageType.Error;
        }
        else if (levelDetailsObject.indexOfPrimaryScene < 0 || levelDetailsObject.indexOfPrimaryScene >= levelDetailsObject.scenesToLoad.Count)
        {
            message = "Index Of First Scene is out of bounds, please set it between 0 and the number of scenes to load ( " + levelDetailsObject.scenesToLoad.Count + " )";
            messageType = MessageType.Error;
        }

        return message == "";
    }

    public bool ChecksScenes(List<string> scenesToCheck, out string errorMessage, string differentThan = null)
    {
        bool allGood = true;
        errorMessage = "";

        if (scenesToCheck.Count == 0)
        {
            allGood = false;
            errorMessage = "There is no referenced scene to load, at least one is required!";
        }

        List<string> scenesFullyChecked = new List<string>(scenesToCheck.Count);
        foreach (string sceneName in scenesToCheck)
        {
            if (!EditorUtils.SceneExists(sceneName))
            {
                allGood = false;
                errorMessage = "Couldn't find scene named \"" + sceneName + "\" in the build settings! Please check the name or add the scene in the Build Settings";
            }
            else if (sceneName.Equals(differentThan))
            {
                allGood = false;
                errorMessage = "Scene named \"" + sceneName + "\" is referenced twice !";
            }
            else
            {
                foreach (string sceneChecked in scenesFullyChecked)
                {
                    if (sceneName.Equals(sceneChecked))
                    {
                        allGood = false;
                        errorMessage = "Scene named \"" + sceneName + "\" is referenced twice !";
                        break;
                    }
                }
            }
            
            if (!allGood)
                break;

            scenesFullyChecked.Add(sceneName);
        }
        return allGood;
    }
}

#endif

