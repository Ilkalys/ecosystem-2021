using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SS_StickToTrack))]
public class SSStickToTrackEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SS_StickToTrack stickToTrack = (SS_StickToTrack)target;

        if (stickToTrack.m_path)
        {
            if (GUILayout.Button("Stick"))
            {
                stickToTrack.StickToPath();
            }
        }
        else
        {
            GUILayout.Label("Can't Stick without a Path");
        }
    }
}
