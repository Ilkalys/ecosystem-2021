using CustomsAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// A component that ensure that only one object of the specified ID is in the currently loaded scenes.
/// This rely on a UniqueObjectLabelList to list the different kind of objects and how they should be specificaly managed.
/// </summary>
[DisallowMultipleComponent]
public class UniqueObject : MonoBehaviour, ISerializationCallbackReceiver
{
    static public List<RefUpdater<GameObject>> uniqueObjectsRefs = new List<RefUpdater<GameObject>>();
    static public UniqueObjectsLabelList uniqueLogicRef; // TODO put uniquelogic here! it is supposed tobe the same for all objects of the same id!

    [SerializeField]
    private UniqueObjectsLabelList serializedLabelList;

    public int id;
    public bool privilege = false;
    public bool dontDestroyOnLoad = false;

    private bool isFollowingActiveScenes = false;
    private void Awake()
    {
        if (uniqueObjectsRefs.Count <= uniqueLogicRef.uniqueLogic.Count)
        {
            for (int i = uniqueObjectsRefs.Count; i < uniqueLogicRef.uniqueLogic.Count; i++)
            {
                uniqueObjectsRefs.Add(new RefUpdater<GameObject>(null));
            }
        }

        if (uniqueObjectsRefs[id].Reference is null)
        {
            uniqueObjectsRefs[id].Reference = gameObject;
        }
        else
        {
            switch(uniqueLogicRef.uniqueLogic[id].singletonLogic)
            {
                case UniqueObjectLogic.SingletonLogic.keepFirstDestroyNewer:
                    Destroy(gameObject);
                    break;

                case UniqueObjectLogic.SingletonLogic.destroyFirstKeepNewer:
                    Destroy(uniqueObjectsRefs[id].Reference);
                    uniqueObjectsRefs[id].Reference = gameObject;
                    break;

                case UniqueObjectLogic.SingletonLogic.keepFirstDeactivateNewer:
                    //TODO add privilege management!
                    gameObject.SetActive(false);
                    break;

                case UniqueObjectLogic.SingletonLogic.deactivateFirstKeepNewer:
                    //TODO add privilege management!
                    uniqueObjectsRefs[id].Reference.SetActive(false);
                    uniqueObjectsRefs[id].Reference = gameObject;
                    break;

                default:
                    Debug.LogError("Unknown value, not implemented!");
                    break;
            }
        }

        if (uniqueLogicRef.uniqueLogic[id].dontDestroyOnLoad == UniqueObjectLogic.DontDestroyOnLoad.True || 
           (
                uniqueLogicRef.uniqueLogic[id].dontDestroyOnLoad == UniqueObjectLogic.DontDestroyOnLoad.ObjectSpecific && 
                dontDestroyOnLoad)
           )
        {
            if (uniqueLogicRef.dontDestroyOnLoadComportement == UniqueObjectsLabelList.DontDestroyOnLoadComportement.DontDestroyOnLoad)
                DontDestroyOnLoad(gameObject);
            else if (uniqueLogicRef.dontDestroyOnLoadComportement == UniqueObjectsLabelList.DontDestroyOnLoadComportement.SwitchToActiveScene)
            {
                SceneManager.MoveGameObjectToScene(gameObject, SceneManager.GetActiveScene());
                if (uniqueLogicRef.followActiveScene)
                {
                    SceneManager.activeSceneChanged += switchToActiveScene;
                    isFollowingActiveScenes = true;
                }
            }
            else if (uniqueLogicRef.dontDestroyOnLoadComportement == UniqueObjectsLabelList.DontDestroyOnLoadComportement.SwitchToNamedScene)
            {
                Scene switchScene = SceneManager.GetSceneByName(uniqueLogicRef.sceneName);
                if (switchScene.IsValid())
                    SceneManager.MoveGameObjectToScene(gameObject, switchScene);
                else
                {
                    DontDestroyOnLoad(gameObject);
                    Debug.LogWarning("the specified scene \"" + uniqueLogicRef.sceneName + "\" was not found... Object was set as DontDestroyOnLoad instead. \n" +
                        "Make sure that the global scene as at least started loading when this component awakes.");
                }
            }
        }
    }

    private void OnDestroy()
    {
        if (uniqueObjectsRefs[id].Reference == gameObject)
            uniqueObjectsRefs[id].Reference = null;

        if (isFollowingActiveScenes)
            SceneManager.activeSceneChanged -= switchToActiveScene;
    }

    void switchToActiveScene(Scene current, Scene next)
    {
        SceneManager.MoveGameObjectToScene(gameObject, next);
    }

    /// <summary>
    /// return the refUpdater to the object based on the first label name in the unique object Label 
    /// list scriptable object currently binded that correspond.
    /// </summary>
    /// <param name="labelName">The label to search</param>
    /// <returns>The specified refUpdater<GameObject> or null if the label doesn't exist.</returns>
    static public RefUpdater<GameObject> GetByLabel(string labelName)
    {
        int index = uniqueLogicRef.uniqueLogic.FindIndex(uniqueObjectLogic => uniqueObjectLogic.label.Equals(labelName));
        if (index == -1)
        {
            Debug.LogError("couldn't find " + labelName + " in " + uniqueLogicRef.name + "!");
            return null;
        }
        else
        {
            return uniqueObjectsRefs[index];
        }
    }

    // this kind of serialization is far from perfect but better than nothing...
    // At least it work fine with only one LabelListAsset
    // TODO find a better way to serialize static data
    // example issue : a new label list is created, set as the new static one on a uniqueObject,
    // then Unity is closed and another UniqueObject (thus not yet set with the newer labelList)
    // is selected and set the old label list as the static one for the class. 
    public void OnBeforeSerialize()
    {
        serializedLabelList = uniqueLogicRef;
    }

    public void OnAfterDeserialize()
    {
        uniqueLogicRef = serializedLabelList;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(UniqueObject))]
[CanEditMultipleObjects]
public class UniqueObjectEditor: Editor
{
    bool pickingLabelsList = false;
    SerializedProperty idProperty;
    SerializedProperty privilegeProperty;
    SerializedProperty serializedLabelListProperty;
    SerializedProperty dontDestroyOnLoadProperty;

    private void OnEnable()
    {
        idProperty = serializedObject.FindProperty("id");
        privilegeProperty = serializedObject.FindProperty("privilege");
        serializedLabelListProperty = serializedObject.FindProperty("serializedLabelList");
        dontDestroyOnLoadProperty = serializedObject.FindProperty("dontDestroyOnLoad");

        if (UniqueObject.uniqueLogicRef is null)
            UniqueObject.uniqueLogicRef = serializedLabelListProperty.objectReferenceValue as UniqueObjectsLabelList;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        UniqueObject targetObject = null;
        if (!serializedObject.isEditingMultipleObjects)
            targetObject = serializedObject.targetObject as UniqueObject;

        if (GUILayout.Button("Set ID labels"))
        {
            pickingLabelsList = true;
            EditorGUIUtility.ShowObjectPicker<UniqueObjectsLabelList>(UniqueObject.uniqueLogicRef, false, "", EditorGUIUtility.GetControlID(FocusType.Passive));
        }
        

        Event e = Event.current;
        if (e.type == EventType.ExecuteCommand)
        {
            if (e.commandName == "ObjectSelectorUpdated")
            {
                UniqueObjectsLabelList selectedLabelList = EditorGUIUtility.GetObjectPickerObject() as UniqueObjectsLabelList;
                UniqueObject.uniqueLogicRef = selectedLabelList;
                serializedLabelListProperty.objectReferenceValue = selectedLabelList;
            }
            else if (e.commandName == "ObjectSelectorClosed")
            {
                pickingLabelsList = false;
            }
        }

        if (UniqueObject.uniqueLogicRef != null)
        {
            if (GUILayout.Button("Edit ID labels"))
            {
                Selection.objects = new Object[] { UniqueObject.uniqueLogicRef };
            }

            EditorGUILayout.PropertyField(idProperty);
            if (!pickingLabelsList) // we don't clamp the value until the label list is definitely chosen (picking object closed)
            {
                EditorUtils.clampIntValue(idProperty, 0, UniqueObject.uniqueLogicRef.uniqueLogic.Count - 1);
            }
            string idlabel = idProperty.hasMultipleDifferentValues ?
                "mixed" :
                (
                    idProperty.intValue >= 0 && idProperty.intValue < UniqueObject.uniqueLogicRef.uniqueLogic.Count ?
                    UniqueObject.uniqueLogicRef.uniqueLogic[idProperty.intValue].label :
                    "Out of current label list range"
                );
            EditorGUILayout.LabelField("Label :", idlabel);

            if (!idProperty.hasMultipleDifferentValues)
            {
                //TODO uncomment once privilege logic is implemented
                //if (UniqueObject.uniqueLogicRef.uniqueLogic[idProperty.intValue].privilegeLogic == UniqueObjectLogic.Privilege.privilegeMarkedObject)
                //{
                //    EditorGUILayout.PropertyField(privilegeProperty);
                //}
                if (UniqueObject.uniqueLogicRef.uniqueLogic[idProperty.intValue].dontDestroyOnLoad == UniqueObjectLogic.DontDestroyOnLoad.ObjectSpecific)
                {
                    EditorGUILayout.PropertyField(dontDestroyOnLoadProperty);
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Some specific UniqueObject parameters might be hidden when editing multiple objects with different ID.", MessageType.Info);
            }
        }
        else
        {
            EditorGUILayout.HelpBox("Please set a label list first!", MessageType.Error);
        }



        serializedObject.ApplyModifiedProperties();
    }
}
#endif