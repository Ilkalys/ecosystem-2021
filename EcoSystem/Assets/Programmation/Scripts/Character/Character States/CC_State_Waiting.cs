using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterController/State/Waiting" )]
public class CC_State_Waiting : CC_State_Deactivatable
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public          Vector3 m_targetPos;
    private const   float   m_minSpeedToTarget = 2f;
    private         float   m_previousSpeed;
    private         bool    m_inTargetPos;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        instanceCC.m_currentSpeed = 0;
        instanceCC.m_currentMaxSpeed = 5;
        instanceCC.m_currentMovement = Vector3.zero;
        instanceCC.m_rigidBody.velocity = Vector3.zero;
        if ( !m_inTargetPos )
        {
            float step = Mathf.Max(m_minSpeedToTarget, m_previousSpeed) * Time.deltaTime;
            instanceCC.transform.position = Vector3.MoveTowards( instanceCC.transform.position, m_targetPos, step );
            if ( Vector3.Distance( instanceCC.transform.position, m_targetPos ) < 0.001f )
            {
                m_inTargetPos = true;
            }
        }
        if(canUse)
            instanceCC.m_Object_InFront = null;

        if ( !instanceCC.m_isWaiting)
        {
            instanceCC.ChangeState( instanceCC.m_brainState.brain_CC_State_Walking);
        }
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        return false;
    }


    //--------------------------------------------------------------------------------------\\
    public override void OnExitState()
    {
        if ( instanceCC.m_nbCough > 0 )
        {
            instanceCC.m_OnEndCough.Invoke();

        }
        instanceCC.m_nbCough = 0;
        instanceCC.m_coughIntensity = 0;
        instanceCC.m_isHardLanding = false;
        instanceCC.m_isOnEdge = false;
        instanceCC.m_edge_InFront = null;
        instanceCC.m_wallClimbable_InFront = null;
        instanceCC.m_Object_InFront = null;
        instanceCC.m_animationPos = Vector3.zero;
        m_targetPos = Vector3.zero;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState()
    {
        if ( instanceCC.m_edge_InFront )
        {
            m_targetPos = instanceCC.m_edge_InFront.GetComponent<OnTrigger_InfosToPlayer>().GetTargetPos();
        }
        else if ( instanceCC.m_wallClimbable_InFront )
        {
            m_targetPos = instanceCC.m_wallClimbable_InFront.GetComponent<OnTrigger_InfosToPlayer>().GetTargetPos();
        }
        else if ( instanceCC.m_Object_InFront )
        {
            m_targetPos = instanceCC.m_Object_InFront.GetComponent<OnTrigger_InfosToPlayer>().GetTargetPos();
        }
        else if ( instanceCC.m_animationPos != Vector3.zero )
        {
            m_targetPos = instanceCC.m_animationPos;
        }
        else
        {
            m_targetPos = Vector3.zero;
        }

        m_inTargetPos = m_targetPos == Vector3.zero;
        m_previousSpeed = instanceCC.m_currentSpeed;

        instanceCC.m_isWaiting = true;
        instanceCC.StartCoroutine("DesactivateState", this);
        instanceCC.m_rigidBody.velocity = Vector3.zero;
    }

}
