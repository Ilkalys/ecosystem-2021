using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UniSense;
public class Controller_SetPadColor : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public LedsColorSetter             m_LCS;
    public Image                       m_image;
    public SkinnedMeshRenderer         m_mesh;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        if ( m_image )
        {
            m_LCS.lightBarColor = m_image.color;
        }
        if ( m_mesh )
        {
            m_LCS.lightBarColor = m_mesh.material.color;
        }
    }
}
