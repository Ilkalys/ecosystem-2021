using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class UI_InputGestion : MonoBehaviour
{
    public string actionToRebind;
    public void RemapButton()
    {
        var rebindOperation = SS_Character_Controller.instance.m_playerActions.asset[actionToRebind]
            .PerformInteractiveRebinding().Start();
        rebindOperation.Dispose();
    }

    public void RemapVector()
    {
        var rebindOperation = SS_Character_Controller.instance.m_playerActions.asset[actionToRebind]
            .PerformInteractiveRebinding().Start();
        rebindOperation.Dispose();
    }
}
