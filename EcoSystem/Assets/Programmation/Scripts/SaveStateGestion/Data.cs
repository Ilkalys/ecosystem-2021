using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "DataStorage/Data")]
public class Data : ScriptableObject
{
    public List<Vector3> positionToSave;
    public List<Quaternion> rotationToSave;
    public List<Vector3> scaleToSave;

}
