using UnityEngine;
using UnityEngine.Events;

public class OnTrigger_InfosToPlayer : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  UnityEvent      OnPushableEnter;
    public  UnityEvent      OnPushableExit;

    private Vector3         m_positionChild_Left;
    private Vector3         m_positionChild_Right;
    private Transform       m_parent;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    private void Start()
    {
        m_parent = this.transform.parent;
    }

    //--------------------------------------------------------------------------------------\\
    private void OnTriggerEnter( Collider other )
    {
        if ( other == SS_Character_Controller.instance.m_character_ClimbDetection )
        {
            switch ( LayerMask.LayerToName(this.gameObject.layer) )
            {
                case "Climbable":
                    {
                        SS_Character_Controller.instance.m_wallClimbable_InFront = this.gameObject;
                        break;
                    }
                case "MediumObject":
                    {
                        SS_Character_Controller.instance.m_Object_InFront = this.gameObject;
                        break;
                    }
                case "HighObject":
                    {
                        SS_Character_Controller.instance.m_Object_InFront = this.gameObject;
                        break;
                    }
                case "JumpOverObject":
                    {
                        SS_Character_Controller.instance.m_Object_InFront = this.gameObject;
                        break;
                    }
                case "PushableObject":
                    {
                        SS_Character_Controller.instance.m_ObjectPushable = this.gameObject;
                        OnPushableEnter.Invoke();
                        break;
                    }
            }
        }
        else if ( other == SS_Character_Controller.instance.m_character_EdgeDetection )
        {
            if ( this.gameObject.layer == LayerMask.NameToLayer( "Edge" ) )
            {
                SS_Character_Controller.instance.m_edge_InFront = this.gameObject;
            }
        }
    }

    //--------------------------------------------------------------------------------------\\
    private void OnTriggerExit( Collider other )
    {
        if ( other == SS_Character_Controller.instance.m_character_ClimbDetection )
        {
            switch ( LayerMask.LayerToName( this.gameObject.layer ) )
            {
                case "Climbable":
                    {
                        if ( SS_Character_Controller.instance.m_wallClimbable_InFront == this.gameObject )
                        {
                            SS_Character_Controller.instance.m_wallClimbable_InFront = null;
                        }
                        break;
                    }
                case "MediumObject":
                    {
                        if ( SS_Character_Controller.instance.m_Object_InFront == this.gameObject )
                        {
                            SS_Character_Controller.instance.m_Object_InFront = null;
                        }
                        break;
                    }
                case "HighObject":
                    {
                        if ( SS_Character_Controller.instance.m_Object_InFront == this.gameObject )
                        {
                            SS_Character_Controller.instance.m_Object_InFront = null;
                        }
                        break;
                    }
                case "JumpOverObject":
                    {
                        if ( SS_Character_Controller.instance.m_Object_InFront == this.gameObject )
                        {
                            SS_Character_Controller.instance.m_Object_InFront = null;
                        }
                        break;
                    }
                case "PushableObject":
                    {
                        if ( SS_Character_Controller.instance.m_ObjectPushable == this.gameObject )
                        {
                            SS_Character_Controller.instance.m_ObjectPushable = null;
                            this.transform.parent = m_parent;
                            OnPushableExit.Invoke();
                        }
                        break;
                    }
            }
        }
        else if ( other == SS_Character_Controller.instance.m_character_EdgeDetection )
        {
            if ( this.gameObject.layer == LayerMask.NameToLayer( "Edge" ) && SS_Character_Controller.instance.m_edge_InFront == this.gameObject )
            {
                SS_Character_Controller.instance.m_edge_InFront = null;
            }
        }
    }

    //--------------------------------------------------------------------------------------\\
    public Vector3 GetTargetPos()
    {
        m_positionChild_Left = Vector3.zero;
        m_positionChild_Right = Vector3.zero;

        if ( this.gameObject.layer != LayerMask.NameToLayer( "PushableObject" ) )
        {
            foreach ( Transform child in this.GetComponentsInChildren<Transform>() )
            {
                if ( child.name == "TargetPos" )
                {
                    m_positionChild_Left = child.position;
                }
            }
            if ( this.gameObject.layer != LayerMask.NameToLayer( "Climbable" ) )
            {
                return m_positionChild_Left;
            }
            else
            {
                 return new Vector3( m_positionChild_Left.x, SS_Character_Controller.instance.transform.position.y, m_positionChild_Left.z );
            }
        }
        else
        {
            foreach ( Transform child in this.GetComponentsInChildren<Transform>() )
            {
                if ( child.name == "TargetPos_Left" )
                {
                    m_positionChild_Left = child.position;

                }
                else if ( child.name == "TargetPos_Right" )
                {
                    m_positionChild_Right = child.position;
                }
            }
            return SS_Character_Controller.instance.SameDirection( this.gameObject ) ? m_positionChild_Left : m_positionChild_Right;
        }
    }
}
