using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SS_StickToTrack : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Tooltip("The path to follow")]
    public          Cinemachine.SSTrack             m_path;

    [Tooltip("Wich Axis of the Track the position of the Character will follow (0 = not followed)")]
    public          Vector3                         m_followedPositionTrackAxis     = new Vector3(1, 0, 1);
    [Tooltip("Wich Axis of the Track the rotation of the Character will follow (0 = not followed)")]
    public          Vector3                         m_followedRotationTrackAxis     = new Vector3(0, 1, 0);

    //The event's current position on the path, in distance units.
    public          float                           m_positionOnThePath;
    public          bool                            m_enableBothDirection;
    public          int                             m_precisionStick                = 10;

    [Tooltip("How to interpret the Path Position.  If set to Path Units, values are as follows: 0 represents the first waypoint on the path, 1 is the second, and so on.  Values in-between are points on the path in between the waypoints.  If set to Distance, then Path Position represents distance along the path.")]
    private const   Cinemachine.SSTrack.PositionUnits m_positionUnits               = Cinemachine.SSTrack.PositionUnits.PathUnits;

    private Vector3 oldPos;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Start()
    {
        m_positionOnThePath = m_path.FindClosestPointWithAxes( this.transform.position, ( int )m_positionOnThePath, 2, m_precisionStick, m_followedPositionTrackAxis );
        StickToPath();
    }

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        if ( this.transform.position != oldPos )
        {
            StickToPath();
        }
    }

    //--------------------------------------------------------------------------------------\\
    public void StickToPath()
    {
        m_positionOnThePath = m_path.FindClosestPointWithAxes( this.transform.position, ( int )m_positionOnThePath, 2, m_precisionStick, m_followedPositionTrackAxis );
        SetEventPosition( m_positionOnThePath );
        oldPos = this.transform.position;
    }

    //--------------------------------------------------------------------------------------\\
    public void SetEventPosition( float distanceAlongPath )
    {
        if ( m_path != null )
        {
            Vector3 positionOnPath = m_path.EvaluatePositionAtUnit(m_positionOnThePath, m_positionUnits);
            positionOnPath.x = m_followedPositionTrackAxis.x == 0 ? transform.position.x : positionOnPath.x;
            positionOnPath.y = m_followedPositionTrackAxis.y == 0 ? transform.position.y : positionOnPath.y;
            positionOnPath.z = m_followedPositionTrackAxis.z == 0 ? transform.position.z : positionOnPath.z;

            transform.position = positionOnPath;

            Vector3 orientationOnPath = m_path.EvaluateOrientationAtUnit(m_positionOnThePath, m_positionUnits).eulerAngles;
            Vector3 rotationEuler = transform.rotation.eulerAngles;
            orientationOnPath.x = m_followedRotationTrackAxis.x == 0 ? rotationEuler.x : orientationOnPath.x;
            orientationOnPath.y = m_followedRotationTrackAxis.y == 0 ? rotationEuler.y : orientationOnPath.y;
            orientationOnPath.z = m_followedRotationTrackAxis.z == 0 ? rotationEuler.z : orientationOnPath.z;

            Vector3 forward = transform.forward;
            transform.rotation = Quaternion.Euler( orientationOnPath );
            Vector3 newForward = transform.forward;

            if ( m_enableBothDirection )
            {
                if ( Vector3.Dot( newForward, forward ) < 0 )
                {
                    orientationOnPath.y += 180;
                }

                transform.rotation = Quaternion.Euler( orientationOnPath );
            }
        }
    }
}
