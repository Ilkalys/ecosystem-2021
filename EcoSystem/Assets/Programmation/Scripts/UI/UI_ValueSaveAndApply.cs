using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//TODO : 
// - ideally the component searching part of the Start() method should be done in the inspector to display direct error message if needed.

/// <summary>
/// A small component to be associated with UI Slider, buttons or toggles to apply their effect 
/// </summary>
public class UI_ValueSaveAndApply : MonoBehaviour
{
    Selectable selectable = null;
    [Tooltip("The player pref dictionnary key to save and reload the last value set, leave empty to not save it")]
    public string playerPrefKey;

    // Start is called before the first frame update
    void Start()
    {
        if (TryGetComponent<Selectable>(out selectable))
        {
            if (selectable.GetType() == typeof(Slider))
            {
                Slider slider = (Slider)selectable;

                if (playerPrefKey != null && playerPrefKey != "" && PlayerPrefs.HasKey(playerPrefKey))
                {
                    slider.value = PlayerPrefs.GetFloat(playerPrefKey);
                }
                else
                {
                    slider.onValueChanged.Invoke(slider.value);
                }
            }
            else if (selectable.GetType() == typeof(Toggle))
            {
                Toggle toggle = (Toggle)selectable;

                if (playerPrefKey != null && playerPrefKey != "" && PlayerPrefs.HasKey(playerPrefKey))
                {
                    toggle.isOn = (PlayerPrefs.GetInt(playerPrefKey) == 1);
                }
                else
                {
                    toggle.onValueChanged.Invoke(toggle.isOn);
                }
            }
            else if (selectable.GetType() == typeof(Button))
            {
                Button button = (Button)selectable;

                button.onClick.Invoke();
            }
        }
    }

    private void OnDestroy()
    {
        if (selectable != null)
        {
            if (selectable.GetType() == typeof(Slider))
            {
                Slider slider = (Slider)selectable;

                if (playerPrefKey != null && playerPrefKey != "")
                {
                    PlayerPrefs.SetFloat(playerPrefKey, slider.value);
                }
            }
            else if (selectable.GetType() == typeof(Toggle))
            {
                Toggle toggle = (Toggle)selectable;

                if (playerPrefKey != null && playerPrefKey != "")
                {
                    PlayerPrefs.SetInt(playerPrefKey, toggle.isOn ? 1 : 0);
                }
            }
        }
    }
}
