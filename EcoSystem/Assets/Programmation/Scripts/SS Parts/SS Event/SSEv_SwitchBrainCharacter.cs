using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SSEv_SwitchBrainCharacter : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public CC_BrainState    m_newBrain;
    public float            delayBeforeSwitch;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void SwitchBrain()
    {
        if ( m_newBrain && SS_Character_Controller.instance )
        {
            SS_Character_Controller.instance.m_brainState = m_newBrain;
            SS_Character_Controller.instance.SwitchBrain();
        }
    }
}
