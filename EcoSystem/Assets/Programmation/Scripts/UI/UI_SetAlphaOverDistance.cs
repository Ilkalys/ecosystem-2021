using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_SetAlphaOverDistance : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public  Image           image;
    public  SpriteRenderer  sprite;
    public  Vector2         RangeDistanceForFading;
    private float           m_alphaValue;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Update()
    {
        float dist = Vector3.Distance( SS_Character_Controller.instance.transform.position, this.transform.position );
        if ( dist > RangeDistanceForFading.y )
        {
            m_alphaValue = 0;
        }
        else if ( dist < RangeDistanceForFading.x )
        {
            m_alphaValue = 1;
        }
        else
        {
            m_alphaValue = 1 - ( dist - RangeDistanceForFading.x ) / ( RangeDistanceForFading.y - RangeDistanceForFading.x );
        }

        if ( image )
        {
            Color color = image.color;
            color.a = m_alphaValue;
            image.color = color;
        }
        if ( sprite )
        {
            Color color = sprite.color;
            color.a = m_alphaValue;
            sprite.color = color;
        }
    }
}
