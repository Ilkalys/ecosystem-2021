using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_SetRatio : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    public Vector2 m_ratio;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    void Awake()
    {
        SetRatio( m_ratio.x, m_ratio.y );
    }

    //--------------------------------------------------------------------------------------\\
    void SetRatio( float w, float h )
    {
        if ( ( ( ( float )Screen.width ) / ( ( float )Screen.height ) ) > w / h )
        {
            Screen.SetResolution( ( int )( ( ( float )Screen.height ) * ( w / h ) ), Screen.height, true );
        }
        else
        {
            Screen.SetResolution( Screen.width, ( int )( ( ( float )Screen.width ) * ( h / w ) ), true );
        }
    }
}
