using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Serialization;
/// <summary>
/// A simple component to link with a black background image to make a fade in or fade out
/// </summary>
public class Fader : TimerComponent
{
    public enum FadeType // As they were firstly for a fading of black panel in front of the scene the name and function are inverted : fade in of the scene == fade out of the black panel.
    {
        FadeIn, 
        FadeOut
    }

    private bool launched = false;

    public FadeType fadeType;
    public Image blackground;
    public SpriteRenderer spriteRenderer;
    public TextMeshProUGUI TMP;

    public new void Start()
    {
        base.Start();

        timer.onTimerEnd.AddListener(setBackgroundfinal);
    }


    public void startFader()
    {
        if (!launched)
        {
            TimerStart();
            launched = true;
        }
    }

    public void Reset()
    {
        launched = false;
    }

    public new void Update()
    {
        base.Update();
        if (timer.IsRunning)
        {
            Color newer = spriteRenderer? spriteRenderer.color : (blackground) ? blackground.color :TMP? TMP.color: Color.white ;

            if (fadeType == FadeType.FadeIn)
            {
                newer.a = (timer.TimeLeft / timer.WaitTime);
            }
            else
            {
                newer.a = (timer.TimePassed / timer.WaitTime);
            }
            if ( spriteRenderer )
            {
                spriteRenderer.color = newer;
            }
            if ( blackground ) {
                blackground.color = newer;
            }
            if ( TMP )
            {
                TMP.color = newer;
            }
        }
    }

    public void setBackgroundfinal()
    {
        Color newer =  spriteRenderer? spriteRenderer.color : (blackground) ? blackground.color :TMP? TMP.color: Color.white ;

        if (fadeType == FadeType.FadeIn)
        {
            newer.a = 0;
        }
        else
        {
            newer.a = 1;
        }
        if ( spriteRenderer )
        {
            spriteRenderer.color = newer;
        }
        if ( blackground )
        {
            blackground.color = newer;
        }
        if ( TMP )
        {
            TMP.color = newer;
        }
    }
}
