using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// A script to hold some of the UI responses to inputs : 
/// -> UI button to link to the cancel input, 
/// -> detect UI global events
/// </summary>
public class UI_InputInteraction : MonoBehaviour
{
    InputMaster inputMaster;
    public Button backButton;

    private Selectable currentSelectable;
    private bool initialized = false;

    public UnityEvent<Selectable, Selectable> onInteractableChanged;
    public UnityEvent<Selectable> onBackButtonClicked;
    public UnityEvent<Selectable> onOtherButtonClicked;

    [Tooltip("Tabs in all the differents scenes : this will click on the next or previous interactable one when gamepad shoulders are pressed")]
    public List<Button> tabsList;
    int actualTabIndex =  0;

    void Start()
    {
        StartCoroutine(startRoutine());
    }

    IEnumerator startRoutine()
    {
        yield return new WaitUntil(() => SceneManager.GetActiveScene().isLoaded);

        if (SS_Character_Controller.instance != null && SS_Character_Controller.instance.gameObject != null)
        {
            inputMaster = SS_Character_Controller.instance.m_playerActions;
        }
        else
        {
            inputMaster = new InputMaster();
            inputMaster.Enable();
        }

        foreach (Button actualTab in tabsList)
        {
            actualTab.onClick.AddListener(() =>
            {
                actualTabIndex = tabsList.FindIndex((Button tab) => actualTab == tab);
            });
        }

        inputMaster.UI.Cancel.performed += OnCancelInput;
        inputMaster.UI.Pause.performed += OnPauseInput;

        inputMaster.UI.Submit.performed += OnSubmitInput;

        inputMaster.UI.PrevTab.performed += OnPrevTab;
        inputMaster.UI.NextTab.performed += OnNextTab;

        initialized = true;
    }

    private void OnDestroy()
    {
        inputMaster.UI.Cancel.performed -= OnCancelInput;
        inputMaster.UI.Pause.performed -= OnPauseInput;

        inputMaster.UI.Submit.performed -= OnSubmitInput;

        inputMaster.UI.PrevTab.performed -= OnPrevTab;
        inputMaster.UI.NextTab.performed -= OnNextTab;
    }

    public void Update()
    {
        if (!initialized)
            return;


        Selectable newSelectable = EventSystem.current?.currentSelectedGameObject?.GetComponent<Selectable>();
        if (currentSelectable != newSelectable)
        {
            if (newSelectable != null)
            {
                onInteractableChanged.Invoke(currentSelectable, newSelectable);
            }
            currentSelectable = newSelectable;
        }
    }

    public void SetCancelButton(Button backButton)
    {
        this.backButton = backButton;
    }

    void OnCancelInput(InputAction.CallbackContext context)
    {
        if (backButton != null && backButton.IsInteractable())
        {
            if (backButton.gameObject == EventSystem.current.currentSelectedGameObject)
            {
                backButton.onClick.Invoke();
                onBackButtonClicked.Invoke(backButton);
            }
            else
            {
                backButton.Select();
            }
        }

    }

    void OnSubmitInput(InputAction.CallbackContext context)
    {
        if (backButton != null && backButton.IsInteractable() && 
            backButton.gameObject == EventSystem.current.currentSelectedGameObject)
        {
            onBackButtonClicked.Invoke(backButton);
        }
        else
        {
            onOtherButtonClicked.Invoke(currentSelectable);
        }
    }


    void OnPauseInput(InputAction.CallbackContext context)
    {
        UI_InputEnabler inputEnabler = GetComponentInChildren<UI_InputEnabler>();
        if (inputEnabler != null && !(inputEnabler.paused))
        {
            inputEnabler.OnPauseInput(context);
        }
        else
        {
            OnCancelInput(context);
        }
    }

    void OnPrevTab(InputAction.CallbackContext context)
    {
        int newIndex = actualTabIndex == 0 ? -1 : tabsList.FindLastIndex(actualTabIndex -1, actualTabIndex, (Button tab) => (tab.IsInteractable() && tab.gameObject.activeInHierarchy));

        if (newIndex >= 0)
        {
            actualTabIndex = newIndex;
            tabsList[newIndex].onClick.Invoke();
        }
    }
    void OnNextTab(InputAction.CallbackContext context)
    {
        int newIndex = tabsList.FindIndex(actualTabIndex + 1, tabsList.Count - (actualTabIndex + 1), (Button tab) => (tab.IsInteractable() && tab.gameObject.activeInHierarchy));

        if (newIndex >= 0)
        {
            actualTabIndex = newIndex;
            tabsList[newIndex].onClick.Invoke();
        }
    }

    public void SetActualTab(Button actualTab)
    {
        int index = tabsList.FindIndex((Button tab) => actualTab == tab);
        if (index >= 0)
        {
            actualTabIndex = index;
            tabsList[index].onClick.Invoke();
        }
        else
        {
            Debug.LogError("Could not find tab \"" + actualTab.name + "\"");
        }
    }
}
