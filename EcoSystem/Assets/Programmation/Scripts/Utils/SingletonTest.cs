using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonTest : MonoBehaviour
{
    static SingletonTest singleton = null;
    private void Awake()
    {
        Debug.Log(gameObject.name + " has awaken");
        TryGetSingle("Awake()");
    }

    private void OnEnable()
    {
        Debug.Log(gameObject.name + " has been enabled");
        TryGetSingle("OnEnable()");
    }

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(gameObject.name + " has started");
        TryGetSingle("Start()");
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(gameObject.name + " has been updated");
        TryGetSingle("Update()");
    }

    public void TryGetSingle(string origin)
    {
        if (singleton != null)
        {
            Debug.Log(gameObject.name + " Found an already existing singleton while in method " + origin);
            //Destroy(gameObject);
        }
        else
        {
            Debug.Log(gameObject.name + " chose itself as the new singleton while in method " + origin);
            singleton = this; 
        }
    }
}
