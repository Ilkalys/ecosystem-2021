using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CharacterController/State/Jumping")]
public class CC_State_Jumping : CC_State_Deactivatable
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Header("               Jump Parameters ")]
    [Space]
    [Tooltip("The maximum duration of a Jump")]
    public  float m_maxJumpTime;
    [Tooltip("The maximum height of a Jump")]
    public  float m_maxJumpHeight;

    [Tooltip("The minimum horizontal force of a Jump")]
    public  float m_jumpHorizontalForce;
    [Tooltip("The duration where the horizontal force is the only applied")]
    public  float m_jumpHorizontalForceDuration;
    private float m_counter_HorizontalJumpTime;

    [Tooltip("The duration of falling needed to consider it like a hard falling")]
    public  float m_timeBeforeHardFalling       = 5f;
    private float m_counter_Falling;

    [Header("               Air Control Parameters ")]
    [Space]

    [Range(0, 1)]
    [Tooltip("0 : The player can't control the character on the air 1 : The Player control the character like in the ground")]
    public  float m_airControl                  = 0f;
    [Tooltip("The acceleration when the character change direction in the air")]
    public  float m_airTurnOverAcc              = 0f;
    [Tooltip("The friction when the character is in the Air")]
    public  float m_airDrag                     = 0.001f;
    [Tooltip("The acceleration of the character in the Air")]
    public  float m_airMaxSpeed                 = 5f;
    [Tooltip("The acceleration of the character in the Air")]
    public  float m_airAcceleration             = 5f;
    [Tooltip("The decceleration of the maxspeed of character in the Air")]
    public  float m_airMaxSpeedDecceleration    = 5f;


    [HideInInspector]
    /// <summary> The gravity while the character is in the air </summary>
    public float m_onAirGravity;
    [HideInInspector]
    /// <summary> The velocity of the jump at the beginning of the jump </summary>
    public float m_initialJumpVelocity;

    private float m_current2DMovementY;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public override void UpdateState()
    {
        if ( CheckChangeState() )
            return;

        if ( m_current2DMovementY < 0 )
        {
            m_counter_Falling += Time.deltaTime;
        }
        else
        {
            m_counter_Falling = 0;
        }

        if ( m_counter_HorizontalJumpTime > 0 )
        {
            m_counter_HorizontalJumpTime -= Time.deltaTime;
        }
        else
        {
            HandleSpeed();
        }
        HandleGravity();

        instanceCC.m_current2DMovement.y = m_current2DMovementY;
        instanceCC.m_currentMovement = new Vector3(instanceCC.transform.forward.x, 0, instanceCC.transform.forward.z) * Mathf.Abs(instanceCC.m_currentSpeed) + Vector3.up * instanceCC.m_current2DMovement.y;
        instanceCC.m_rigidBody.velocity = instanceCC.m_currentMovement;
    }

    //--------------------------------------------------------------------------------------\\
    protected override bool CheckChangeState()
    {
        if ( instanceCC.m_isFalling && instanceCC.m_edge_InFront && instanceCC.m_brainState.brain_CC_State_OnEdge.canUse )
        {
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_OnEdge );
            return true;
        }
        if ( instanceCC.m_isGrounded && !instanceCC.m_isJumping )
        {
            if ( m_counter_Falling > m_timeBeforeHardFalling )
            {
                instanceCC.ChangeState( instanceCC.m_brainState.brain_CC_State_Waiting );
                instanceCC.m_isHardLanding = true;
                return true;
            }
            instanceCC.ChangeStateWithUpdate( instanceCC.m_brainState.brain_CC_State_Walking );
            return true;
        }
        if ( instanceCC.SameDirection( instanceCC.m_wallClimbable_InFront ) && instanceCC.m_brainState.brain_CC_State_ClimbingWall.canUse )
        {
            instanceCC.ChangeState( instanceCC.m_brainState.brain_CC_State_ClimbingWall );
            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnExitState()
    {
        if(canUse)
            instanceCC.StartCoroutine( "DesactivateState", this );
    }

    //--------------------------------------------------------------------------------------\\
    public override void OnEnterState()
    {
        SSCC_setupJumpVariable();
        if ( canUse && instanceCC.m_isJumping && instanceCC.m_isGrounded )
        {
            m_current2DMovementY = m_initialJumpVelocity;
            if ( instanceCC.m_currentMovementInput != 0 && Mathf.Abs( instanceCC.m_currentSpeed) < m_jumpHorizontalForce )
            {
                instanceCC.m_currentSpeed = m_jumpHorizontalForce * Mathf.Sign(instanceCC.m_currentMovementInput);
            }
            m_counter_HorizontalJumpTime = m_jumpHorizontalForceDuration;
        }
        else
        {
            m_current2DMovementY = instanceCC.m_current2DMovement.y;
        }
        m_counter_Falling = 0;
    }

    //--------------------------------------------------------------------------------------\\
    private void SSCC_setupJumpVariable()
    {
        float timeToApex = m_maxJumpTime / 2;
        m_onAirGravity = (-2 * m_maxJumpHeight) / Mathf.Pow(timeToApex, 2);
        m_initialJumpVelocity = (2 * m_maxJumpHeight) / timeToApex;

    }

    //--------------------------------------------------------------------------------------\\
    void HandleSpeed()
    {
        float currentSpeed = instanceCC.m_currentSpeed;

        if (Mathf.Abs(instanceCC.m_currentMovementInput) > 0.9f)
        {
            if ( instanceCC.m_currentMaxSpeed > m_airMaxSpeed )
                instanceCC.m_currentMaxSpeed -= Time.deltaTime * m_airMaxSpeedDecceleration;
            else
            {
                instanceCC.m_currentMaxSpeed = m_airMaxSpeed;
            }
            float absCurrentMaxSpeed = instanceCC.m_currentMaxSpeed * Mathf.Abs(instanceCC.m_currentMovementInput);
            float acc = (Mathf.Sign(currentSpeed) == Mathf.Sign(instanceCC.m_currentMovementInput)) ? m_airAcceleration : m_airTurnOverAcc;
            float addedSpeed = (acc * Time.fixedDeltaTime * instanceCC.m_currentMovementInput);
            currentSpeed = Mathf.Clamp(currentSpeed + addedSpeed * m_airControl, -absCurrentMaxSpeed, absCurrentMaxSpeed);
        }
        else
        {
            currentSpeed *= Mathf.Pow(m_airDrag, Time.fixedDeltaTime);
            if (currentSpeed < 0.02f && currentSpeed > -0.02f)
                currentSpeed = 0f;
        }

        instanceCC.m_previousSpeed = instanceCC.m_currentSpeed;
        instanceCC.m_currentSpeed = currentSpeed;
    }

    //--------------------------------------------------------------------------------------\\
    void HandleGravity()
    {
        float fallMultiplier = 2.0f;

        float previousYVelocity = m_current2DMovementY;
        float newYVelocity = m_current2DMovementY + m_onAirGravity * (instanceCC.m_isFalling ? fallMultiplier : 1) * Time.deltaTime;
        float nextYVelocity = Mathf.Max((previousYVelocity + newYVelocity) * 0.5f, -20.0f);
        m_current2DMovementY = newYVelocity;
    }

}
