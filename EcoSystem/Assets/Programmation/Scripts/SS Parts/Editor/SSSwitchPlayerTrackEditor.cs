using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SSEv_SwitchPlayerTrack))]
public class SSSwitchPlayerTrackEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Adapt the Segment of the New Track to the SSEvent Position");
        SSEv_SwitchPlayerTrack switchPlayerTrack = (SSEv_SwitchPlayerTrack)target;
        SSEventOnTrack eventOnTrack = switchPlayerTrack.gameObject.GetComponent<SSEventOnTrack>();
        if (eventOnTrack)
        {
            if (GUILayout.Button("Adapt"))
            {
                SerializedObject SO = new UnityEditor.SerializedObject(switchPlayerTrack);
                SerializedProperty segNewTrack = SO.FindProperty("m_segmentOfNewTrack");

                segNewTrack.intValue = (int)eventOnTrack.m_pathParams.positionOnThePath;
                SO.ApplyModifiedProperties();

                switchPlayerTrack.m_segmentOfNewTrack = (int)eventOnTrack.m_pathParams.positionOnThePath;
            }
        }
        else
        {
            GUILayout.Label("Can't Adapt without Event");
        }
        GUILayout.EndHorizontal();
    }
}
