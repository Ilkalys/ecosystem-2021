#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditorUtils
{
    public static void clampIntValue(SerializedProperty intProperty, int? min, int? max)
    {
        if (min.HasValue && intProperty.intValue < min.Value)
        {
            intProperty.intValue = min.Value;
        }
        if (max.HasValue && intProperty.intValue > max.Value)
        {
            intProperty.intValue = max.Value;
        }
    }

    /// <summary>
    /// Is this property the object at the root of the inspector (and not a property of a larger one)
    /// </summary>
    public static bool IsInspectorRootProperty(SerializedProperty property)
    {
        return property.objectReferenceValue.GetInstanceID() == property.serializedObject.targetObject.GetInstanceID();
    }

    // Copied from https://github.com/arimger/Unity-Editor-Toolbox/ in SceneNameAttributeDrawer class
    /// <summary>
    /// Checks in the scene registered if the given name is already registered
    /// </summary>
    /// <returns>true if the name is found, false otherwise</returns>
    public static bool SceneExists(string sceneName, bool ignoreCase = false)
    {
        if (string.IsNullOrEmpty(sceneName))
        {
            return false;
        }

        for (var i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            var scenePath = SceneUtility.GetScenePathByBuildIndex(i);
            var lastSlash = scenePath.LastIndexOf("/");
            var name = scenePath.Substring(lastSlash + 1, scenePath.LastIndexOf(".") - lastSlash - 1);

            if (string.Compare(name, sceneName, ignoreCase) == 0)
            {
                return true;
            }
        }

        return false;
    }

    // copied from http://sketchyventures.com/2015/08/07/unity-tip-getting-the-actual-object-from-a-custom-property-drawer/ and added List<> support
    /// <summary>
    /// A black magic method able to find non Unity.Object underlying object of a complex SerializedProperty (as an Array or a List).
    /// Very usefull in the case of a PropertyDrawer
    /// </summary>
    /// <typeparam name="T">The type of the object to find</typeparam>
    /// <param name="fieldInfo">The fieldInfo value of the PropertyDrawer</param>
    /// <param name="property">the serializedProperty of the object to find</param>
    /// <returns>The underlying object referenced by the SerializedProperty if found, null otherwise.</returns>
    public static T GetActualObjectForSerializedProperty<T>(System.Reflection.FieldInfo fieldInfo, SerializedProperty property) where T : class
    {
        var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
        if (obj == null) { return null; }

        T actualObject = null;
        if (obj.GetType().IsArray || obj.GetType() == typeof(List<T>))
        {
            var index = System.Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));

            if (obj.GetType().IsArray)
            {
                actualObject = ((T[])obj)[index];
            }
            else
            {
                actualObject = ((List<T>)obj)[index];
            }
        }
        else
        {
            actualObject = obj as T;
        }
        return actualObject;
    }
}
#endif
