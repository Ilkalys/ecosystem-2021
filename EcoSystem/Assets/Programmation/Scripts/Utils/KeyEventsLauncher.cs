using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyEventsLauncher : MonoBehaviour
{
    public USharpEvent startEvent;
    public USharpEvent updateEvent;
    public USharpEvent destroyEvent;

    public USharpEvent playerTriggerEnterEvent;
    public USharpEvent playerTriggerExitEvent;

    void Start() =>
        startEvent.Invoke();

    void Update() =>
        updateEvent.Invoke();

    void OnDestroy() =>
        destroyEvent.Invoke();

    void OnTriggerEnter( Collider other )
    {
        if ( other.gameObject == SS_Character_Controller.instance.gameObject )
            playerTriggerEnterEvent.Invoke();
    }

    void OnTriggerExit( Collider other ) 
    {
        if( other.gameObject == SS_Character_Controller.instance.gameObject ) 
            playerTriggerExitEvent.Invoke();
    }
}
