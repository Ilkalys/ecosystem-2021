using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class SSEv_SwitchPlayerTrack : MonoBehaviour
{
    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          ATTRIBUTES                                                      \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    [Tooltip("The new Track, for a better switch the old and the new track should probably be overlaped")]
    public Cinemachine.SSTrack  m_newTrack;

    [Tooltip("The segment of the new track where we will search the closest Point on this track, a segment is a section of path between 2 waypoints. The shorter is this segment, the better will be the position")]
    public int                  m_segmentOfNewTrack;

    //--------------------------------------------------------------------------------------------------------------------------\\
    //                                                          METHODS                                                         \\
    //--------------------------------------------------------------------------------------------------------------------------\\

    //--------------------------------------------------------------------------------------\\
    public void Switch()
    {
        if ( SS_Character_Controller.instance != null)
        {
            SS_StickToTrack playerToSwitch  = SS_Character_Controller.instance.gameObject.GetComponent<SS_StickToTrack>();
            if ( playerToSwitch )
            {
                if ( playerToSwitch.m_path != m_newTrack )
                {
                    playerToSwitch.m_path = m_newTrack;
                    playerToSwitch.m_positionOnThePath = m_newTrack.FromPathNativeUnits( m_newTrack.FindClosestPoint( playerToSwitch.transform.position, m_segmentOfNewTrack, 1, 10 ), Cinemachine.SSTrack.PositionUnits.PathUnits );
                }
            }
        }
    }
}
