using CustomsAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

/// <summary>
/// Timer class, to create code based on time. Don't forget to call <see cref="Update(float)"/> 
/// often and regularly, the timer granularity and functionning depends on it.
/// </summary>
[Serializable]
public class Timer
{
    // Events
    public USharpEvent onTimerStart;
    [FormerlySerializedAs("OnTimeout")]
    public USharpEvent onTimerEnd;

    // Fields
    private bool _paused = false;
    private bool _stopped = true;
    private bool _needReset = false;
    [SerializeField, FormerlySerializedAs("waitTime"), HideInInspector]
    private float _waitTime = 1f;
    [SerializeField, HideInInspector]
    private float _timeLeft = 1f;

    // Parameters
    public float WaitTime => _waitTime;
    public float TimeLeft => _timeLeft;
    public float TimePassed => _waitTime - _timeLeft;
    public bool IsRunning => !(_paused || _stopped);
    public bool IsPaused => _paused;
    public bool IsStopped => _stopped;


    // Constructor
    public Timer(float waitTime = 1f)
    {
        this._waitTime = waitTime;
        this._timeLeft = waitTime;
    }


    // Methods

    /// <summary>
    /// The method used to update the Timer, call it often and regularly, the timer granularity 
    /// and functionning depends on it. In a MonoBehaviour component this 
    /// should be called in the update method for example.
    /// </summary>
    /// <param name="timePassed"> The time passed since last call (typically <see cref="Time.deltaTime"/> 
    /// in the case of the MonoBehaviour Update() method)</param>
    /// <example>
    /// In a simple Monobehaviour this method should be called in the Update() Method 
    /// <code>
    /// Timer timer;
    /// private void Update()
    /// {
    ///     timer.Update(Time.deltaTime);
    /// }
    /// </code>
    /// </example>
    public void Update(float timePassed)
    {
        if (!_stopped && !_paused)
        {
            _timeLeft -= timePassed;
            if (_timeLeft <= 0)
            {
                End();
            }
        }
    }


    /// <summary>
    /// Start the countdown
    /// </summary>
    public void Start()
    {
        if (!_needReset)
        {
            _stopped = false;
            onTimerStart.Invoke();
        }
    }

    /// <summary>
    /// Stop the countdown but keep the current <see cref="Timer._timeLeft"/> value for the countdown to be potentially resumed afterward. 
    /// <see cref="Resume"/>
    /// </summary>
     public void Pause()
    {
        _paused = true;
    }

    /// <summary>
    /// Resume the timer's countdown to its current value of <see cref="Timer._timeLeft"/>. 
    /// Is equivalent to <see cref="StartTimer"/> if the timer has not been paused previously;
    /// </summary>
    public void Resume()
    {
        _paused = false;
    }

    /// <summary>
    /// Stops the timer without thE ability to resume it (use <see cref="Reset(float)"/> or <see cref="Restart(float)"/> to reuse it)
    /// </summary>
    public void Stop()
    {
        _stopped = true;
        _timeLeft = 0;
        _needReset = true;
    }

    /// <summary>
    /// Ends the timer immediatly and calls onTimerEnd events
    /// </summary>
    public void End()
    {
        Stop();
        onTimerEnd?.Invoke();
    }

    /// <summary>
    /// Stops and Reset the timer to its <see cref="Timer.waitTime"/>
    /// </summary>
    public void Reset(float newWaitTime = -1)
    {
        Stop();
        if (newWaitTime >= 0)
        {
            _waitTime = newWaitTime;
        }
        _paused = false;
        _timeLeft = _waitTime;
        _needReset = false;
    }

    /// <summary>
    /// Stops, Resets the Timer and Starts it again
    /// </summary>
    public void Restart(float newWaitTime = -1)
    {
        Reset(newWaitTime);
        Start();
    }
}


#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(Timer))]
public class TimerProperty : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.PropertyField(position, property, true);

        if (property.isExpanded)
        {
            Timer targetObject = EditorUtils.GetActualObjectForSerializedProperty<Timer>(fieldInfo, property);

            position.y += position.height - (EditorGUIUtility.standardVerticalSpacing * 2 + EditorGUIUtility.singleLineHeight * 3) ;
            position.height = EditorGUIUtility.singleLineHeight;
            EditorGUI.BeginChangeCheck();
            float newWaitTime = EditorGUI.FloatField(position, new GUIContent("Wait Time"), targetObject.WaitTime);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(property.serializedObject.targetObject, "Changed Wait Time value");
                targetObject.Reset(newWaitTime);
            }
            else if (!Application.isPlaying && newWaitTime != targetObject.TimeLeft)
            {
                targetObject.Reset(newWaitTime);
                EditorUtility.SetDirty(property.serializedObject.targetObject);
            }

            position.y += EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight;
            EditorGUI.LabelField(position, new GUIContent("Time Left"), new GUIContent(targetObject.TimeLeft.ToString()));

            position.y += EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight;
            position.width = EditorGUIUtility.currentViewWidth / 2;
            if (GUI.Button(position, new GUIContent("Stop")))
            {
                targetObject.Stop();
            }

            position.x += position.width; //TODO this isn't perfectly fitting the inspector 
            if (GUI.Button(position, new GUIContent("Restart")))
            {
                targetObject.Restart();
            }
        }
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true) + 
            (property.isExpanded ? (EditorGUIUtility.standardVerticalSpacing * 2 + EditorGUIUtility.singleLineHeight * 3) : 0);
    }
}
#endif
//*/