using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SaveAndLoadManager : MonoBehaviour
{
    public List<GameObject> TransformToSave;
    public Data dataStorage;

    public void Save()
    {
        Debug.Log("Save");
        dataStorage.positionToSave.Clear();
        dataStorage.rotationToSave.Clear();
        dataStorage.scaleToSave.Clear();

        for (int i = 0; i < TransformToSave.Count; i++)
        {
            dataStorage.positionToSave.Add(TransformToSave[i].transform.position);
            dataStorage.rotationToSave.Add(TransformToSave[i].transform.rotation);
            dataStorage.scaleToSave.Add(TransformToSave[i].transform.localScale);
        }
    }

    public void Load()
    {
        Debug.Log("Load");
        if (dataStorage.positionToSave.Count > 0)
        {
            for (int i = 0; i < dataStorage.positionToSave.Count && i < TransformToSave.Count; i++)
            {
                TransformToSave[i].transform.position = dataStorage.positionToSave[i];
                TransformToSave[i].transform.rotation = dataStorage.rotationToSave[i];
                TransformToSave[i].transform.localScale = dataStorage.scaleToSave[i];
            }
        }
    }
}
