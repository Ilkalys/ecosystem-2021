using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// A script to reload the main scene of a level already loaded. This is used as a way to make checkpoints.
/// </summary>
public class MainLevelSceneReloader : MonoBehaviour
{
    public LevelsScenesDistribution levelsScenesDistribution;

    //UnityEvent onReloadedScene = new UnityEvent();

    public void ReloadCurrentLevelPrimaryScene()
    {
        //int currentLevel = levelsScenesDistribution.SearchLevelWithPrimarySceneName(SceneManager.GetActiveScene().name);
        int currentLevel = levelsScenesDistribution.SearchLevelWithPrimarySceneName(gameObject.scene.name);

        GameObject player = UniqueObject.GetByLabel("Player").Reference;
        GameObject camera = UniqueObject.GetByLabel("Camera").Reference;
        string sceneToReload = levelsScenesDistribution.levelsDetails[currentLevel].GetPrimaryScene;

        Debug.Assert(!(player is null), "Couldn't find Player UniqueObject. Reloading scene aborted.");
        Debug.Assert(!(camera is null), "Couldn't find Camera UniqueObject. Reloading scene aborted.");
        Debug.Assert(!(sceneToReload is null), "Couldn't find level " + currentLevel + " PrimaryScene. Reloading scene aborted.");

        if (sceneToReload is null || player is null || camera is null)
        {
            return;
        }


        Destroy(player);
        Destroy(camera);

        if (SceneManager.sceneCount > 1)
        {
            SceneManager.UnloadSceneAsync(sceneToReload).completed += (AsyncOperation _) =>
                SceneManager.LoadSceneAsync(sceneToReload, LoadSceneMode.Additive).completed += (AsyncOperation loadOperation) =>
                    OnReloadedScene(loadOperation, sceneToReload);
        }
        else
        {
            Scene scene = SceneManager.GetActiveScene(); 
            SceneManager.LoadScene(scene.name);
        }
    }

    void OnReloadedScene(AsyncOperation sceneLoadOperation, string sceneReloaded)
    {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneReloaded));
        //onReloadedScene.Invoke()
    }
}
